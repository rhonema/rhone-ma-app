import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { ThemeService } from '../../_services/theme.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { Role } from '../../_models/role';
import { Employee } from '../../_models/employee';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnDestroy {

  @Input() sidebarVisible = true;
  @Input() navTab = 'menu';
  @Input() currentActiveMenu;
  @Input() currentActiveSubMenu;
  @Output() changeNavTabEvent = new EventEmitter();
  @Output() activeInactiveMenuEvent = new EventEmitter();

  public themeClass = 'theme-cyan';
  public darkClass = '';
  private ngUnsubscribe = new Subject();

  currentUser: Employee;
  greeting: any;

  constructor(
    private themeService: ThemeService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
      this.themeService.themeClassChange
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(themeClass => {
          themeClass = themeClass;
        });

      this.themeService.darkClassChange.pipe(takeUntil(this.ngUnsubscribe)).subscribe(darkClass => {
          this.darkClass = darkClass;
      });

      this.currentUser = this.authenticationService.currentUserValue;

      const today = new Date();
      const curHr = today.getHours();

      if (curHr < 12) {
        this.greeting = 'Good Morning';
      } else if (curHr < 18) {
        this.greeting = 'Good Afternoon';
      } else {
        this.greeting = 'Good Evening';
      }

      console.log(this.currentUser);
    }

  ngOnDestroy() {
      this.ngUnsubscribe.next();
      this.ngUnsubscribe.complete();
  }

  changeNavTab(tab: string) {
    this.navTab = tab;
  }

  activeInactiveMenu(menuItem: string) {
    this.activeInactiveMenuEvent.emit({ 'item': menuItem });
  }

  changeTheme(theme: string) {
    this.themeService.themeChange(theme);
  }

  changeDarkMode(darkClass: string) {
      this.themeService.changeDarkMode(darkClass);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.roles[0] === Role.Admin;
  }

  get isFinance() {
    return this.currentUser && this.currentUser.roles[0] === Role.Finance;
  }

  get isGvr() {
    return this.currentUser && this.currentUser.roles[0] === Role.Gvr;
  }

  get isHod() {
    return this.currentUser && this.currentUser.roles[0] === Role.Hod;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/authentication/login']);
  }
}
