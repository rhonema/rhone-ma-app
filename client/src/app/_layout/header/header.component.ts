import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../_services/authentication.service';
import { ThemeService } from '../../_services/theme.service';
import { Employee } from '../../_models/employee';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
providers: [NgbDropdownConfig]
})
export class HeaderComponent implements OnInit {

  currentUser: Employee;

  // Properties
  @Input() showNotifMenu = false;
  @Input() showToggleMenu = false;
  @Input() darkClass = '';
  @Output() toggleSettingDropMenuEvent = new EventEmitter();
  @Output() toggleNotificationDropMenuEvent = new EventEmitter();

  constructor(
    private config: NgbDropdownConfig,
    private themeService: ThemeService,
    private router: Router,
    private authenticationService: AuthenticationService
    ) {
    config.placement = 'bottom-right';
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
  }

  toggleSettingDropMenu() {
    this.toggleSettingDropMenuEvent.emit();
  }


  toggleSideMenu() {
    this.themeService.showHideMenu();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/authentication/login']);
  }

}
