import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UploadService {
    constructor(
        private http: HttpClient
    ) {}

    imageUpload(imageForm: FormData) {
        return this.http.post(`${environment.apiUrl}/upload`, imageForm);
    }

    fileUpload(fileForm: FormData) {
        console.log(fileForm);
        return this.http.post(`${environment.apiUrl}/upload/file`, fileForm);
    }
}
