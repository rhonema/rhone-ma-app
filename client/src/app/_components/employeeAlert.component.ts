import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { EmployeeAlertService } from '../_services/employeeAlert.service';

@Component({
  selector: 'app-employeeAlert',
  templateUrl: 'employeeAlert.component.html'
})
export class EmployeeAlertComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any;

  constructor(
    private employeeAlertService: EmployeeAlertService
  ) {}

  ngOnInit() {
    this.subscription = this.employeeAlertService.getAlert()
      .subscribe(message => {
        switch (message && message.type) {
          case 'success':
            message.cssClass = 'block-header alert alert-success';
            break;
          case 'error':
            message.cssClass = 'block-header alert alert-danger';
            break;
        }

        this.message = message;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
