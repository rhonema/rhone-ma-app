import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { GvrAlertService } from '../_services/gvrAlert.service';

@Component({
  selector: 'app-gvrAlert',
  templateUrl: 'gvrAlert.component.html'
})
export class GvrAlertComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any;

  constructor(
    private gvrAlertService: GvrAlertService
  ) {}

  ngOnInit() {
    this.subscription = this.gvrAlertService.getAlert()
      .subscribe(message => {
        switch (message && message.type) {
          case 'success':
            message.cssClass = 'block-header alert alert-success';
            break;
          case 'error':
            message.cssClass = 'block-header alert alert-danger';
            break;
        }

        this.message = message;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
