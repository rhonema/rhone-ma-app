import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import { GvrSafePipe } from '../_helpers/gvr.safe.pipe';

import { routing } from './gvr.routing';
import { LayoutModule } from '../_layout/layout.module';
import { PagesModule } from '../pages/pages.module';
import { GvrComponent } from './gvr/gvr.component';
import { IndexComponent } from './gvr/index/index.component';
import { GvrAlertComponent } from '../_components/gvrAlert.component';

import { CustomersComponent } from './gvr/customers/customers.component';
import { CustomerAddComponent } from './gvr/customers/customer-add/customer-add.component';
import { CustomerEditComponent } from './gvr/customers/customer-edit/customer-edit.component';

import { ContactPersonAddComponent } from './gvr/customers/contactPerson/contactPerson-add/contactPerson-add.component';
import { ContactPersonEditComponent } from './gvr/customers/contactPerson/contactPerson-edit/contactPerson-edit.component';

import { CustomerPositionsComponent } from './gvr/customers/customerPositions/customerPositions.component';
import { CustomerPositionAddComponent } from './gvr/customers/customerPositions/customerPosition-add/customerPosition-add.component';
import { CustomerPositionEditComponent } from './gvr/customers/customerPositions/customerPosition-edit/customerPositions-edit.component';

import { GvrCategoriesComponent } from './gvr/gvrCategories/gvrCategories.component';
import { GvrCategoryAddComponent } from './gvr/gvrCategories/gvrCategory-add/gvrCategory-add.component';
import { GvrCategoryEditComponent } from './gvr/gvrCategories/gvrCategory-edit/gvrCategory-edit.component';

import { GvrSectionsComponent } from './gvr/gvrSections/gvrSections.component';
import { GvrSectionAddComponent } from './gvr/gvrSections/gvrSection-add/gvrSection-add.component';
import { GvrSectionEditComponent } from './gvr/gvrSections/gvrSection-edit/gvrSection-edit.component';

import { GvrSettingsComponent } from './gvr/gvrSettings/gvrSettings.component';

import { ActivitiesComponent } from './gvr/activities/activities.component';
import { ActivityListComponent } from './gvr/activities/activity-list/activity-list.component';
import { ActivityAddComponent } from './gvr/activities/activity-add/activity-add.component';
import { ActivityEditComponent } from './gvr/activities/activity-edit/activity-edit.component';

import { ActivityDetailAddComponent } from './gvr/activities/activityDetail/activityDetail-add/activityDetail-add.component';
import { ActivityDetailEditComponent } from './gvr/activities/activityDetail/activityDetail-edit/activityDetail-edit.component';

import { ActivityReportComponent } from './gvr/activities/activity-report/activity-report.component';
import { ActivitySubmitComponent } from './gvr/activities/activity-submit/activity-submit.component';

import { AnalysisActivitiesComponent } from './gvr/analysis/activities/analysis-activities.component';
import { AnalysisCrmComponent } from './gvr/analysis/crm/analysis-crm.component';

import { ClaimsComponent } from './gvr/claims/claims.component';




@NgModule({
  imports: [
    CommonModule,
    routing,
    LayoutModule,
    RouterModule,
    PagesModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DataTablesModule,
    Ng2SearchPipeModule,
    AngularMultiSelectModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    GvrSafePipe,
    GvrComponent,
    IndexComponent,
    GvrAlertComponent,
    CustomersComponent,
    CustomerAddComponent,
    CustomerEditComponent,
    ContactPersonAddComponent,
    ContactPersonEditComponent,
    CustomerPositionsComponent,
    CustomerPositionAddComponent,
    CustomerPositionEditComponent,
    GvrSectionsComponent,
    GvrSectionAddComponent,
    GvrSectionEditComponent,
    GvrCategoriesComponent,
    GvrCategoryAddComponent,
    GvrCategoryEditComponent,
    GvrSettingsComponent,
    ActivitiesComponent,
    ActivityListComponent,
    ActivityReportComponent,
    ActivityAddComponent,
    ActivityEditComponent,
    ActivityDetailAddComponent,
    ActivityDetailEditComponent,
    ActivitySubmitComponent,
    AnalysisActivitiesComponent,
    AnalysisCrmComponent,
    ClaimsComponent
  ]
})

export class GvrModule { }
