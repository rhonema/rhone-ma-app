import { Routes, RouterModule } from '@angular/router';
import { GvrComponent } from './gvr/gvr.component';
import { IndexComponent } from './gvr/index/index.component';

import { AuthGuard } from '../_helpers/auth.guard';
import { CustomersComponent } from './gvr/customers/customers.component';
import { CustomerAddComponent } from './gvr/customers/customer-add/customer-add.component';
import { CustomerEditComponent } from './gvr/customers/customer-edit/customer-edit.component';

import { ContactPersonAddComponent } from './gvr/customers/contactPerson/contactPerson-add/contactPerson-add.component';
import { ContactPersonEditComponent } from './gvr/customers/contactPerson/contactPerson-edit/contactPerson-edit.component';

import { CustomerPositionsComponent } from './gvr/customers/customerPositions/customerPositions.component';
import { CustomerPositionAddComponent } from './gvr/customers/customerPositions/customerPosition-add/customerPosition-add.component';
import { CustomerPositionEditComponent } from './gvr/customers/customerPositions/customerPosition-edit/customerPositions-edit.component';

import { GvrSectionsComponent } from './gvr/gvrSections/gvrSections.component';
import { GvrSectionAddComponent } from './gvr/gvrSections/gvrSection-add/gvrSection-add.component';
import { GvrSectionEditComponent } from './gvr/gvrSections/gvrSection-edit/gvrSection-edit.component';

import { GvrCategoriesComponent } from './gvr/gvrCategories/gvrCategories.component';
import { GvrCategoryAddComponent } from './gvr/gvrCategories/gvrCategory-add/gvrCategory-add.component';
import { GvrCategoryEditComponent } from './gvr/gvrCategories/gvrCategory-edit/gvrCategory-edit.component';

import { GvrSettingsComponent } from './gvr/gvrSettings/gvrSettings.component';

import { ActivitiesComponent } from './gvr/activities/activities.component';
import { ActivityListComponent } from './gvr/activities/activity-list/activity-list.component';
import { ActivityAddComponent } from './gvr/activities/activity-add/activity-add.component';
import { ActivityEditComponent } from './gvr/activities/activity-edit/activity-edit.component';

import { ActivityDetailAddComponent } from './gvr/activities/activityDetail/activityDetail-add/activityDetail-add.component';
import { ActivityDetailEditComponent } from './gvr/activities/activityDetail/activityDetail-edit/activityDetail-edit.component';

import { ActivityReportComponent } from './gvr/activities/activity-report/activity-report.component';

import { ActivitySubmitComponent } from './gvr/activities/activity-submit/activity-submit.component';

import { AnalysisActivitiesComponent } from './gvr/analysis/activities/analysis-activities.component';
import { AnalysisCrmComponent } from './gvr/analysis/crm/analysis-crm.component';

import { ClaimsComponent } from './gvr/claims/claims.component';





/***************************
* SETUP
***************************/

const routes: Routes = [
    {
        path: '',
        component: GvrComponent,
        children: [
            { path: '', redirectTo: 'dashboard'},
            {
              path: 'dashboard',
              children: [
                  {
                    path: '',
                    redirectTo: 'index',
                    pathMatch: 'full'
                  },
                  {
                    path: 'index',
                    component: IndexComponent,
                    canActivate: [AuthGuard],
                    data: {
                      title: 'GVR :: Dashboard'
                    }
                  }
              ]
            },
            {
              path: 'customers',
              component: CustomersComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Customers'
              }
            },
            {
              path: 'customers/add',
              component: CustomerAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Add Customer'
              }
            },
            {
              path: 'customers/edit/:id',
              component: CustomerEditComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Edit Customer'
              }
            },
            {
              path: 'customers/customerPositions',
              component: CustomerPositionsComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Customer Position'
              }
            },
            {
              path: 'customers/customerPositions/add',
              component: CustomerPositionAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Add Customer Position'
              }
            },
            {
              path: 'customers/customerPositions/edit/:id',
              component: CustomerPositionEditComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Edit Customer Position'
              }
            },
            {
              path: 'customers/contactPersons/add/:id',
              component: ContactPersonAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Add Contact Person'
              }
            },
            {
              path: 'customers/contactPersons/edit/:id/customers/:cid',
              component: ContactPersonEditComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Edit Contact Person'
              }
            },
            {
              path: 'gvrSections',
              component: GvrSectionsComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: GVR Sections'
              }
            },
            {
              path: 'gvrSections/add',
              component: GvrSectionAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Add GVR Section'
              }
            },
            {
              path: 'gvrSections/edit/:id',
              component: GvrSectionEditComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Edit GVR Section'
              }
            },
            {
              path: 'gvrCategories',
              component: GvrCategoriesComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: GVR Categories'
              }
            },
            {
              path: 'gvrCategories/add',
              component: GvrCategoryAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Add GVR Categories'
              }
            },
            {
              path: 'gvrCategories/edit/:id',
              component: GvrCategoryEditComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Edit GVR Categories'
              }
            },
            {
              path: 'gvrSettings',
              component: GvrSettingsComponent,
              data: {
                title: 'GVR :: Settings'
              }
            },
            {
              path: 'activities',
              component: ActivitiesComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Activities'
              }
            },
            {
              path: 'activities/list/:id/year/:year',
              component: ActivityListComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: List Activities'
              }
            },
            {
              path: 'activities/add/:id/year/:year',
              component: ActivityAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Add Activity'
              }
            },
            {
              path: 'activities/edit/:id',
              component: ActivityEditComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Edit Activity'
              }
            },
            {
              path: 'activities/activityDetail/add/:id/from/:type',
              component: ActivityDetailAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Add Activity Detail'
              }
            },
            {
              path: 'activities/activityDetail/edit/:id/activities/:aid/from/:type',
              component: ActivityDetailEditComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'GVR :: Edit Activity Detail'
              }
            },
            {
              path: 'activities/report/:id/year/:year/uid/:uid',
              component: ActivityReportComponent,
              data: {
                title: 'GVR :: Report'
              }
            },
            {
              path: 'activities/submit/:id/year/:year',
              component: ActivitySubmitComponent,
              data: {
                title: 'GVR :: Report'
              }
            },
            {
              path: 'analysis/activities',
              component: AnalysisActivitiesComponent,
              data: {
                title: 'GVR :: Analysis'
              }
            },
            {
              path: 'analysis/crm',
              component: AnalysisCrmComponent,
              data: {
                title: 'GVR :: Analysis'
              }
            },
            {
              path: 'claims',
              component: ClaimsComponent,
              data: {
                title: 'GVR :: Claims'
              }
            }

        ]
    },
];

export const routing = RouterModule.forChild(routes);
