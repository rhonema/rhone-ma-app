import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { SidebarService } from '../../../_services/sidebar.service';
import { GvrAlertService } from '../../../_services/gvrAlert.service';
import { AuthenticationService } from '../../../_services/authentication.service';
import { UsersService } from '../../../admin/admin/employee/users/users.service';
import { ActivitiesService } from '../activities/activities.service';
import { ThirdPartyDraggable } from '@fullcalendar/interaction';





@Component({
    selector: 'app-claims',
    templateUrl: './claims.component.html',
    styleUrls: ['./claims.component.css']
})
export class ClaimsComponent implements OnInit {
    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //variables
    currentUser: any;
    year: any;
    weekTable: any;
    user: any;
    submit: any;
    dateSubmit: any;
    message: any;

    //dropdown
    public usersList: Array<any>;
    public selectedUser: Array<any>;
    public dropdownUsersSettings: any;

    public yearsList: Array<any>;
    public selectedYear: Array<any>;
    public dropdownYearsSettings: any;

    constructor(
        private router: Router,
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private authenticationService: AuthenticationService,
        private usersService: UsersService,
        private activitiesService: ActivitiesService
    ) {
        this.dropdownUsersSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'employeeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownYearsSettings = {
            singleSelection: true,
            idField: 'year',
            textField: 'year',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };
    }


    ngOnInit () {
        this.user = this.authenticationService.currentUserValue;

        this.getUsers();

        this.getYears();
    }


    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    getUsers () {
        this.usersService.getGvrUsers()
        .subscribe((res: any) => {
          console.log(res);
            if (res) {
                this.usersList = res;
            }
        },
        error => {
          this.message = error;
        });
    }

    getYears() {
        this.activitiesService.getYears()
        .subscribe((res: any) => {
            this.yearsList = res;
        },
        error => {
            this.message = error;
        });
    }

    getWeeks(year, uid) {
        this.currentUser = uid;
        this.year = year.toString();
        const startDate = moment(moment().startOf('year').format(this.year + '-MM-DD'));
        const endDate = moment(moment().endOf('year').format(this.year + '-MM-DD'));
        const totalWeeks = endDate.diff(startDate, 'week');

        const weekData = [];
        for (let i = 1; i <= totalWeeks; i++) {
            const start = moment(this.year).day('Sunday').week(i).format('YYYY/MM/DD');
            const end = moment(this.year).day('Sunday').week(i).add(6, 'd').format('YYYY/MM/DD');
            const report = this.getActivity(i, this.year, uid);
            const data = {
                i,
                start,
                end,
                report
            };

            weekData.push(data);

        }
        this.weekTable = weekData;
    }

    getActivity(week, year, uid) {
        const result = [];
        this.activitiesService.checkSubmit(week, year, uid)
        .subscribe((res: any) => {
            result.push(res);
        },
        error => {
            this.message = error;
        });

        return result;
    }

    submitClaim(week, year, uid) {
        this.activitiesService.submitClaim(week, year, uid)
        .subscribe((res: any) => {
            this.gvrAlertService.success('Claim Submited.', true);
            this.getWeeks(year, uid);
        },
        error => {
            this.message = error;
        });
    }

    onSubmit(isValid: Boolean, form: NgForm) {
        this.getWeeks(form.value.year[0], form.value.receipients[0].id);
    }

}
