import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { GvrSection } from '../../../_models/gvrSection';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GvrSectionsService {

  constructor(
    private http: HttpClient
  ) { }

  getGvrSections(page: any): Observable<GvrSection[]> {
    return this.http.get<GvrSection[]>(`${environment.apiUrl}/gvr/gvrSections/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetched Gvr Sections')),
      catchError(this.handleError('getGvrSections', []))
    );
  }

  getAllGvrSections(): Observable<GvrSection[]> {
    return this.http.get<GvrSection[]>(`${environment.apiUrl}/gvr/gvrSections/all`)
    .pipe(
      tap(_ => this.log('Fetch Gvr Sections')),
      catchError(this.handleError('getAllGvrSections', []))
    );
  }

  getGvrSection(id: any): Observable<GvrSection> {
    return this.http.get<GvrSection>(`${environment.apiUrl}/gvr/gvrSections/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch Gvr Section id: ${id}`))
    );
  }

  addGvrSection(gvrSection: GvrSection) {
    return this.http.post(`${environment.apiUrl}/gvr/gvrSections/create`, gvrSection);
  }

  updateGvrSection(id: any, gvrSection: GvrSection): Observable<any> {
    return this.http.put(`${environment.apiUrl}/gvr/gvrSections/${id}`, gvrSection)
    .pipe(
      tap(_ => this.log(`Updated Gvr Section id: ${id}`))
    );
  }

  deleteGvrSection(id: any): Observable<GvrSection> {
    return this.http.delete<GvrSection>(`${environment.apiUrl}/gvr/gvrSections/${id}`)
    .pipe(
      tap(_ => this.log('Gvr Section deleted'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
