import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../_services/sidebar.service';
import { AlertService } from '../../../../_services/alert.service';
import { GvrSectionsService } from '../gvrSections.service';
import { GvrCategoriesService } from '../../gvrCategories/gvrCategories.service';

import { GvrSection } from '../../../../_models/gvrSection';
import { GvrCategory } from '../../../../_models/gvrCategory';

@Component({
  selector: 'app-Section-edit',
  templateUrl: './gvrSection-edit.component.html',
  styleUrls: ['./gvrSection-edit.component.css']
})
export class GvrSectionEditComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dropdown
  public dropdownGvrCategoryList: Array<any>;
	public selectedGvrCategoryItems: Array<any>;
  public dropdownGvrCategorySettings: any;

  //variable
  id = '';
  message: any;
  gvrSection: GvrSection;
  gvrCategories: GvrCategory[] = [];
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private gvrSectionsService: GvrSectionsService,
    private gvrCategoriesService: GvrCategoriesService,
    private router: Router,
    private route: ActivatedRoute
    ) {
      this.dropdownGvrCategorySettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'gvrCategoryName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };
    }

    ngOnInit() {
      this.gvrSection = new GvrSection();
      this.getGvrSection(this.route.snapshot.params.id);
      this.loadAllGvrCategories();
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getGvrSection(id: any) {
      this.gvrSectionsService.getGvrSection(id).subscribe((data: any) => {
        this.gvrSection = data;
        this.selectedGvrCategoryItems = data.gvrCategories;
      }, error => console.log(error));
    }

    loadAllGvrCategories() {
      this.gvrCategoriesService.getAllGvrCategories()
      .subscribe((res: any) => {
        this.dropdownGvrCategoryList = res;
      },
      error => {
        this.message = error;
      });
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();

        this.gvrSectionsService.updateGvrSection(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Gvr Section Updated.', true);
            this.router.navigate(['/gvr/gvrSections']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
