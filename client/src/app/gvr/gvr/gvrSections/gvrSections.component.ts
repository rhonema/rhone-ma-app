import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';

import { SidebarService } from '../../../_services/sidebar.service';
import { GvrSectionsService } from './gvrSections.service';
import { ModalService } from '../../../_services/modal.service';
import { AlertService } from '../../../_services/alert.service';


@Component({
  selector: 'app-gvrSections',
  templateUrl: './gvrSections.component.html',
  styleUrls: ['./gvrSections.component.css']
})
export class GvrSectionsComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private gvrSectionsService: GvrSectionsService,
    private modalService: ModalService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllGvrSections(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.gvrSectionsService.deleteGvrSection(id)
            .pipe(first())
            .subscribe(() => this.loadAllGvrSections(1)
            );
            this.alertService.success('Gvr Section Deleted.', true);
        }
      });
  }


  private loadAllGvrSections(page) {
    this.gvrSectionsService.getGvrSections(page)
    .subscribe((res: any) => {
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
