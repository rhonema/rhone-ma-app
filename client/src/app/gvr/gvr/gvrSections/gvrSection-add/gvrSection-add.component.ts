import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { AlertService } from '../../../../_services/alert.service';
import { GvrSectionsService } from '../gvrSections.service';
import { GvrCategoriesService } from '../../gvrCategories/gvrCategories.service';

import { GvrSection } from '../../../../_models/gvrSection';
import { GvrCategory } from '../../../../_models/gvrCategory';


@Component({
  selector: 'app-gvrSection-add',
  templateUrl: './gvrSection-add.component.html',
  styleUrls: ['./gvrSection-add.component.css']
})
export class GvrSectionAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dropdown
  public dropdownGvrCategoryList: Array<any>;
	public selectedGvrCategoryItems: Array<any>;
  public dropdownGvrCategorySettings: any;

  //variable
  gvrSection: GvrSection;
  gvrCategories: GvrCategory[] = [];
  message: any;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private gvrSectionsService: GvrSectionsService,
    private gvrCategoriesService: GvrCategoriesService,
    private router: Router
  ) {
    this.selectedGvrCategoryItems = [];

    this.dropdownGvrCategorySettings = {
      singleSelection: false,
      idField: '_id',
      textField: 'gvrCategoryName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
  }

  ngOnInit() {
    this.gvrSection = new GvrSection();
    this.loadAllGvrCategories();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  loadAllGvrCategories() {
    this.gvrCategoriesService.getAllGvrCategories()
    .subscribe((res: any) => {
      this.dropdownGvrCategoryList = res;
    },
    error => {
      this.message = error;
    });
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.gvrSectionsService.addGvrSection(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Gvr Section Added.', true);
          this.router.navigate(['/gvr/gvrSections']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
