import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { GvrCategory } from '../../../_models/gvrCategory';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GvrCategoriesService {

  constructor(
    private http: HttpClient
  ) { }

  getGvrCategories(page: any): Observable<GvrCategory[]> {
    return this.http.get<GvrCategory[]>(`${environment.apiUrl}/gvr/gvrCategories/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetched Gvr categories')),
      catchError(this.handleError('getGvrCategories', []))
    );
  }

  getAllGvrCategories(): Observable<GvrCategory[]> {
    return this.http.get<GvrCategory[]>(`${environment.apiUrl}/gvr/gvrCategories/all`)
    .pipe(
      tap(_ => this.log('Fetch Gvr categories')),
      catchError(this.handleError('getAllGvrCategories', []))
    );
  }

  getGvrCategory(id: any): Observable<GvrCategory> {
    return this.http.get<GvrCategory>(`${environment.apiUrl}/gvr/gvrCategories/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch Gvr category id: ${id}`))
    );
  }

  addGvrCategory(gvrCategory: GvrCategory) {
    return this.http.post(`${environment.apiUrl}/gvr/gvrCategories/create`, gvrCategory);
  }

  updateGvrCategory(id: any, gvrCategory: GvrCategory): Observable<any> {
    return this.http.put(`${environment.apiUrl}/gvr/gvrCategories/${id}`, gvrCategory)
    .pipe(
      tap(_ => this.log(`Updated Gvr category id: ${id}`))
    );
  }

  deleteGvrCategory(id: any): Observable<GvrCategory> {
    return this.http.delete<GvrCategory>(`${environment.apiUrl}/gvr/gvrCategories/${id}`)
    .pipe(
      tap(_ => this.log('Gvr Category deleted'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
