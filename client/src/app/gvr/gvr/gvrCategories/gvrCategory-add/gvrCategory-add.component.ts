import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { AlertService } from '../../../../_services/alert.service';
import { GvrCategoriesService } from '../gvrCategories.service';
import { GvrCategory } from '../../../../_models/gvrCategory';

@Component({
  selector: 'app-gvrCategory-add',
  templateUrl: './gvrCategory-add.component.html',
  styleUrls: ['./gvrCategory-add.component.css']
})
export class GvrCategoryAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  gvrCategory: GvrCategory;
  message: any;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private gvrCategoriesService: GvrCategoriesService,
    private router: Router
  ) {}

  ngOnInit() {
    this.gvrCategory = new GvrCategory();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.gvrCategoriesService.addGvrCategory(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Gvr Category Added.', true);
          this.router.navigate(['/gvr/gvrCategories']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
