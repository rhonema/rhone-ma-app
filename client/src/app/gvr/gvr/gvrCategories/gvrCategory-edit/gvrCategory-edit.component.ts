import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../_services/sidebar.service';
import { AlertService } from '../../../../_services/alert.service';
import { GvrCategoriesService } from '../gvrCategories.service';
import { GvrCategory } from '../../../../_models/gvrCategory';

@Component({
  selector: 'app-category-edit',
  templateUrl: './gvrCategory-edit.component.html',
  styleUrls: ['./gvrCategory-edit.component.css']
})
export class GvrCategoryEditComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  gvrCategory: GvrCategory;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private gvrCategoriesService: GvrCategoriesService,
    private router: Router,
    private route: ActivatedRoute
    ) {}

    ngOnInit() {
      this.gvrCategory = new GvrCategory();
      this.getGvrCategory(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getGvrCategory(id: any) {
      this.gvrCategoriesService.getGvrCategory(id).subscribe((data: any) => {
        this.gvrCategory = data;
      }, error => console.log(error));
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();

        this.gvrCategoriesService.updateGvrCategory(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Gvr Category Updated.', true);
            this.router.navigate(['/gvr/gvrCategories']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
