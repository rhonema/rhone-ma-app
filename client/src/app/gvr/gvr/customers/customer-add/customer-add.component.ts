import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { CustomersService } from '../customers.service';
import { CompaniesService } from '../../../../admin/admin/setup/companies/companies.service';
import { Customer } from '../../../../_models/customer';
import { Company } from '../../../../_models/company';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.css']
})
export class CustomerAddComponent implements OnInit {

    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    ///dropdown
    public companyList: Array<any>;
    public selectedCompany: Array<any>;
    public dropdownCompanySettings: any;

    //variable
    id: any;
    customer: Customer;
    company: Company[] = [];
    message: any;
    submitted = false;

    constructor(
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private customersService: CustomersService,
        private companiesService: CompaniesService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.dropdownCompanySettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

    }

    ngOnInit() {
        this.customer = new Customer();
        this.loadAllCompanies();
    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    loadAllCompanies() {
        this.companiesService.getAllCompanies()
        .subscribe((res: any) => {
          this.companyList = res;
        },
        error => {
          this.message = error;
        });
      }

    onSubmit(isValid: Boolean, form: NgForm) {
        if (isValid) {
        this.submitted = true;

        this.gvrAlertService.clear();

        this.customersService.addCustomer(form.value)
        .pipe(first())
        .subscribe(
            data => {
            this.gvrAlertService.success('Customer Added.', true);
            this.router.navigate(['/gvr/customers']);
            },
            error => {
            this.gvrAlertService.error(error);
            }
        );

        } else {
            return;
        }
    }

}
