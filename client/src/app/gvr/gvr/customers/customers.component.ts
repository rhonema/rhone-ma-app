import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';
import { Subject } from 'rxjs';

import { SidebarService } from '../../../_services/sidebar.service';
import { ModalService } from '../../../_services/modal.service';
import { GvrAlertService } from '../../../_services/gvrAlert.service';
import { Customer } from '../../../_models/customer';
import { CustomersService } from './customers.service';


@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit, OnDestroy {
    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //dialog modal
    public confirmedResult: boolean;

    //pagination
    customers: Customer[] = [];
    dtOptions: DataTables.Settings = {};
    dtTrigger = new Subject();

    //variable
    message: any;

    constructor(
        private route: ActivatedRoute,
        private modalService: ModalService,
        private gvrAlertService: GvrAlertService,
        private sidebarService: SidebarService,
        private customersService: CustomersService
      ) { }

      ngOnInit() {
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10
        };

        this.loadAllCustomers();
      }

      ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
      }

      openConfirm(id: any) {
        this.modalService.confirm(
          'Are you sure want to delete?'
        ).pipe(
          take(1) // take() manages unsubscription for us
        ).subscribe(result => {
            if (result) {
                this.customersService.deleteCustomer(id)
                .pipe(first())
                .subscribe(() => this.loadAllCustomers()
                );
                this.gvrAlertService.success('Customer Deleted.', true);
            }
          });
      }

      toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
      }

      private loadAllCustomers() {
        this.customersService.getAllCustomers()
        .subscribe((res: any) => {
          this.customers = res;
          this.dtTrigger.next();
        },
        error => {
          this.message = error;
        });
      }

}
