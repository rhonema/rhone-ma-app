import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../../_services/gvrAlert.service';
import { ContactPersonsService } from '../contactPersons.service';
import { CustomerPositionsService } from '../../customerPositions/customerPositions.service';
import { Customer } from '../../../../../_models/customer';
import { CustomerPosition } from './../../../../../_models/customerPosition';
import { ContactPerson } from '../../../../../_models/contactPerson';



@Component({
  selector: 'app-contactPerson-add',
  templateUrl: './contactPerson-add.component.html',
  styleUrls: ['./contactPerson-add.component.css']
})
export class ContactPersonAddComponent implements OnInit {

    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //dropdown
    public contactPersonTitleList: Array<any>;
    public selectedContactPersonTitle: Array<any>;
    public dropdownContactPersonTitleSettings: any;
    
    public dropdownList: Array<any>;
    public selectedItems: Array<any>;
    public dropdownSettings: any;

    //variable
    id: any;
    cid: any;
    customer: Customer;
    contactPerson: ContactPerson;
    customerPosition: CustomerPosition;
    message: any;
    submitted = false;

    constructor(
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private contactPersonsService: ContactPersonsService,
        private customerPositionsService: CustomerPositionsService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.contactPersonTitleList = [
            {'contactPersonTitle': 'MR.'},
            {'contactPersonTitle': 'MRS.'},
            {'contactPersonTitle': 'MS.'},
            {'contactPersonTitle': 'DR.'}
        ];

        this.dropdownContactPersonTitleSettings = {
            singleSelection: true,
            idField: 'contactPersonTitle',
            textField: 'contactPersonTitle',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownSettings = {
            singleSelection: false,
            idField: '_id',
            textField: 'customerPositionName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };
    }

    ngOnInit() {
        this.customer = new Customer();
        this.contactPerson = new ContactPerson();
        this.id = this.route.snapshot.paramMap.get('id');
        this.cid = this.route.snapshot.paramMap.get('cid');
        this.loadAllCustomerPositions();
    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    loadAllCustomerPositions() {
        this.customerPositionsService.getAllPositions()
        .subscribe((res: any) => {
          this.dropdownList = res;
        },
        error => {
          this.message = error;
        });
    }

    onSubmit(isValid: Boolean, form: NgForm) {
        if (isValid) {
        this.submitted = true;

        this.gvrAlertService.clear();

        this.contactPersonsService.addContactPerson(this.id, form.value)
        .pipe(first())
        .subscribe(
            data => {
            this.gvrAlertService.success('Contact Person Added.', true);
            this.router.navigate(['/gvr/customers/edit/' + data]);
            },
            error => {
            this.gvrAlertService.error(error);
            }
        );

        } else {
            return;
        }
    }

}
