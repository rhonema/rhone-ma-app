import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../../_services/gvrAlert.service';
import { ContactPersonsService } from '../contactPersons.service';
import { CustomerPositionsService } from '../../customerPositions/customerPositions.service';
import { Customer } from '../../../../../_models/customer';
import { CustomerPosition } from '../../../../../_models/customerPosition';
import { ContactPerson } from '../../../../../_models/contactPerson';

@Component({
  selector: 'app-contactPerson-edit',
  templateUrl: './contactPerson-edit.component.html',
  styleUrls: ['./contactPerson-edit.component.css']
})
export class ContactPersonEditComponent implements OnInit {

    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //dropdown
    public contactPersonTitleList: Array<any>;
    public selectedContactPersonTitle: Array<any>;
    public dropdownContactPersonTitleSettings: any;

    public dropdownList: Array<any>;
    public selectedItems: Array<any>;
    public dropdownSettings: any;

    //variable
    id = '';
    cid = '';
    customer: Customer;
    contactPerson: ContactPerson;
    customerPosition: CustomerPosition[] = [];
    message: any;
    submitted = false;

    constructor(
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private contactPersonsService: ContactPersonsService,
        private customerPositionsService: CustomerPositionsService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.contactPersonTitleList = [
            {'contactPersonTitle': 'MR.'},
            {'contactPersonTitle': 'MRS.'},
            {'contactPersonTitle': 'MS.'},
            {'contactPersonTitle': 'DR.'}
        ];
  
        this.dropdownContactPersonTitleSettings = {
            singleSelection: true,
            idField: 'contactPersonTitle',
            textField: 'contactPersonTitle',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };
        
        this.dropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'customerPositionName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };
    }

    ngOnInit() {
        this.customer = new Customer();
        this.contactPerson = new ContactPerson();
        this.id = this.route.snapshot.paramMap.get('id');
        this.cid = this.route.snapshot.paramMap.get('cid');
        this.getContactPerson(this.route.snapshot.params.id, this.route.snapshot.params.cid);
        this.loadAllCustomerPositions();
    }

    toggleFullWidth() {
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    getContactPerson(id: any, cid: any) {
        this.contactPersonsService.getContactPerson(id, cid).subscribe((data: any) => {
          this.contactPerson = data;
          this.selectedItems = data.contactPersonPosition;
          this.selectedContactPersonTitle = data.contactPersonTitle;
        }, error => console.log(error));
      }

    loadAllCustomerPositions() {
        this.customerPositionsService.getAllPositions()
        .subscribe((res: any) => {
          this.dropdownList = res;
        },
        error => {
          this.message = error;
        });
    }


    onSubmit(isValid: Boolean, form: NgForm) {
        if (isValid) {

            this.submitted = true;

            this.gvrAlertService.clear();

            this.contactPersonsService.updateContactPerson(this.cid, this.id, form.value)
            .pipe(first())
            .subscribe(
            data => {
                this.gvrAlertService.success('Contact Person Updated.', true);
                this.router.navigate(['/gvr/customers/edit/' + this.cid]);
            },
            error => {
                this.gvrAlertService.error(error);
            }
            );

        } else {
            return;
        }
    }


}
