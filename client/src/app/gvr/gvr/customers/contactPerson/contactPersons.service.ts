import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Customer } from '../../../../_models/customer';


@Injectable({
  providedIn: 'root'
})
export class ContactPersonsService {

  constructor(
    private http: HttpClient
  ) { }

    getContactPersons(page: any): Observable<Customer[]> {
        return this.http.get<Customer[]>(`${environment.apiUrl}/gvr/customers/contactPersons/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Contact Persons')),
            catchError(this.handleError('getContactPersons', []))
        );
    }

    getAllContactPersons(): Observable<Customer[]> {
        return this.http.get<Customer[]>(`${environment.apiUrl}/gvr/customers/contactPersons/all`)
        .pipe(
            tap(_ => this.log('Fetched Contact Persons')),
            catchError(this.handleError('getAllContactPersons', []))
        );
    }

    getContactPerson(id: any, cid: any): Observable<Customer> {
        return this.http.get<Customer>(`${environment.apiUrl}/gvr/customers/contactPersons/${id}/customers/${cid}`)
        .pipe(
            tap(_ => this.log(`Fetched Contact Person id: ${id}`))
        );
    }

    addContactPerson(id: any, customer: Customer) {
        const contactPersonData = {
            id: id,
            customer: customer
        }
        return this.http.post(`${environment.apiUrl}/gvr/customers/contactPersons/create`, contactPersonData);
    }

    updateContactPerson(cid: any, id: any, customer: Customer) {
        const contactPersonData = {
            id: id,
            cid: cid,
            customer: customer
        }
        return this.http.put(`${environment.apiUrl}/gvr/customers/contactPersons/update`, contactPersonData)
        .pipe(
            tap(_ => this.log(`Updated Contact Person id=${id}`))
        );
    }

    deleteContactPerson(id: any, cid: any): Observable<Customer> {
        return this.http.delete<Customer>(`${environment.apiUrl}/gvr/customers/contactPersons/${id}/customers/${cid}`)
        .pipe(
            tap(_ => this.log(`Deleted Contact Person id=${id}`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}