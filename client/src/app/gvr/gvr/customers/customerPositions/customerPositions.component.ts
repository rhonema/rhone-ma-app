import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';

import { SidebarService } from '../../../../_services/sidebar.service';
import { CustomerPositionsService } from './customerPositions.service';
import { ModalService } from '../../../../_services/modal.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { CustomerPosition } from '../../../../_models/customerPosition';


@Component({
  selector: 'app-customerPositions',
  templateUrl: './customerPositions.component.html',
  styleUrls: ['./customerPositions.component.css']
})
export class CustomerPositionsComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  message: any;
  customerPosition: CustomerPosition[] = [];

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private customerPositionsService: CustomerPositionsService,
    private modalService: ModalService,
    private gvrAlertService: GvrAlertService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllPositions(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.customerPositionsService.deletePosition(id)
            .pipe(first())
            .subscribe(() => this.loadAllPositions(1)
            );
            this.gvrAlertService.success('Customer Position Deleted.', true);
        }
      });
  }


  private loadAllPositions(page) {
    this.customerPositionsService.getPositions(page)
    .subscribe((res: any) => {
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
