import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../../_services/gvrAlert.service';
import { CustomerPositionsService } from '../customerPositions.service';
import { CustomerPosition } from '../../../../../_models/customerPosition';

@Component({
  selector: 'app-customerPosition-edit',
  templateUrl: './customerPosition-edit.component.html',
  styleUrls: ['./customerPosition-edit.component.css']
})
export class CustomerPositionEditComponent implements OnInit {
  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  customerPosition: CustomerPosition;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private gvrAlertService: GvrAlertService,
    private customerPositionsService: CustomerPositionsService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

    ngOnInit() {
      this.customerPosition = new CustomerPosition();
      this.getPosition(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getPosition(id: any) {
      this.customerPositionsService.getPosition(id).subscribe((data: any) => {
        this.customerPosition = data;
      }, error => console.log(error));
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.gvrAlertService.clear();

        this.customerPositionsService.updatePosition(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.gvrAlertService.success('Customer Position Updated.', true);
            this.router.navigate(['/gvr/customers/customerPositions']);
          },
          error => {
            this.gvrAlertService.error(error);
          }
        );
      } else {
        return;
      }
    }

}
