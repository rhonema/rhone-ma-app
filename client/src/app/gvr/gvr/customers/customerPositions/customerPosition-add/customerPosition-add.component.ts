import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../../_services/gvrAlert.service';
import { CustomerPositionsService } from '../customerPositions.service';
import { CustomerPosition } from '../../../../../_models/customerPosition';

@Component({
  selector: 'app-customerPosition-add',
  templateUrl: './customerPosition-add.component.html',
  styleUrls: ['./customerPosition-add.component.css']
})
export class CustomerPositionAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  customerPosition: CustomerPosition;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private gvrAlertService: GvrAlertService,
    private customerPositionsService: CustomerPositionsService,
    private router: Router
    ) { }

    ngOnInit() {
      this.customerPosition = new CustomerPosition();
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.gvrAlertService.clear();

        this.customerPositionsService.addPosition(form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.gvrAlertService.success('New Customer Position Added.', true);
            this.router.navigate(['/gvr/customers/customerPositions']);
          },
          error => {
            this.gvrAlertService.error(error);
          }
        );
      } else {
        return;
      }
    }


}
