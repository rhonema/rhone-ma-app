import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { CustomerPosition } from '../../../../_models/customerPosition';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerPositionsService {

  constructor(
    private http: HttpClient
  ) { }

  getPositions(page: any): Observable<CustomerPosition[]> {
    return this.http.get<CustomerPosition[]>(`${environment.apiUrl}/gvr/customers/customerPositions/list?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetched Customer Position')),
      catchError(this.handleError('getPositions', []))
    );
  }

  getAllPositions(): Observable<CustomerPosition[]> {
    return this.http.get<CustomerPosition[]>(`${environment.apiUrl}/gvr/customers/customerPositions/all`)
    .pipe(
      tap(_ => this.log('Fetch Customer Position')),
      catchError(this.handleError('getAllPositions', []))
    );
  }

  getPosition(id: any): Observable<CustomerPosition> {
    return this.http.get<CustomerPosition>(`${environment.apiUrl}/gvr/customers/customerPositions/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch Customer Position id: ${id}`))
    );
  }

  addPosition(customerPosition: CustomerPosition) {
    return this.http.post(`${environment.apiUrl}/gvr/customers/customerPositions/create`, customerPosition);
  }

  updatePosition(id: any, customerPosition: CustomerPosition): Observable<any> {
    return this.http.put(`${environment.apiUrl}/gvr/customers/customerPositions/${id}`, customerPosition)
    .pipe(
      tap(_ => this.log(`Updated Customer Position id: ${id}`))
    );
  }

  deletePosition(id: any): Observable<CustomerPosition> {
    return this.http.delete<CustomerPosition>(`${environment.apiUrl}/gvr/customers/customerPositions/${id}`)
    .pipe(
      tap(_ => this.log('Customer Position deleted'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
