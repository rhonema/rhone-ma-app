import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';

import { SidebarService } from '../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { ModalService } from '../../../../_services/modal.service';
import { CustomersService } from '../customers.service';
import { CompaniesService } from '../../../../admin/admin/setup/companies/companies.service';
import { ContactPersonsService } from '../contactPerson/contactPersons.service';
import { Customer } from '../../../../_models/customer';
import { Company } from '../../../../_models/company';


@Component({
    selector: 'app-customer-edit',
    templateUrl: './customer-edit.component.html',
    styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {
    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //dropdown
    public companyList: Array<any>;
    public selectedCompany: Array<any>;
    public dropdownCompanySettings: any;

    //variable
    message: any;
    id = '';
    customer: Customer;
    company: Company[] = [];
    submitted = false;

    constructor(
      private sidebarService: SidebarService,
      private gvrAlertService: GvrAlertService,
      private modalService: ModalService,
      private customersService: CustomersService,
      private companiesService: CompaniesService,
      private contactPersonsService: ContactPersonsService,
      private router: Router,
      private route: ActivatedRoute
      ) {
        //Company
        this.dropdownCompanySettings = {
          singleSelection: true,
          idField: '_id',
          textField: 'companyName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

       }

      ngOnInit() {
        this.customer = new Customer();
        this.getCustomer(this.route.snapshot.params.id);
        this.loadAllCompanies();
      }

      toggleFullWidth() {
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
      }

      getCustomer(id: any) {
        this.customersService.getCustomer(id).subscribe((data: any) => {
          this.customer = data;
          this.selectedCompany = data.company;
        }, error => console.log(error));
      }

      loadAllCompanies() {
        this.companiesService.getAllCompanies()
        .subscribe((res: any) => {
          this.companyList = res;
        },
        error => {
          this.message = error;
        });
      }


      openConfirm(id: any) {
        this.modalService.confirm(
          'Are you sure want to delete?'
        ).pipe(
          take(1) // take() manages unsubscription for us
        ).subscribe(result => {
            if (result) {
                this.contactPersonsService.deleteContactPerson(id, this.route.snapshot.params.id)
                .pipe(first())
                .subscribe(() => this.getCustomer(this.route.snapshot.params.id)
                );
                this.gvrAlertService.success('Contact Person Deleted.', true);
            }
          });
      }

      onSubmit(isValid: boolean, form: NgForm) {
        if (isValid) {
          this.submitted = true;

          //reset alerts on submit
          this.gvrAlertService.clear();

          this.customersService.updateCustomer(form.value.id, form.value)
          .pipe(first())
          .subscribe(
            data => {
              this.gvrAlertService.success('Customer Updated.', true);
              this.router.navigate(['/gvr/customers/edit/' + data]);
            },
            error => {
              this.gvrAlertService.error(error);
            }
          );
        } else {
          return;
        }
      }

  }
