import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { Customer } from '../../../_models/customer';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(
    private http: HttpClient
  ) { }

    getCustomers(page: any): Observable<Customer[]> {
        return this.http.get<Customer[]>(`${environment.apiUrl}/gvr/customers/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Customers')),
            catchError(this.handleError('getCustomers', []))
        );
    }

    getAllCustomers(): Observable<Customer[]> {
        return this.http.get<Customer[]>(`${environment.apiUrl}/gvr/customers/all`)
        .pipe(
            tap(_ => this.log('Fetched Customers')),
            catchError(this.handleError('getAllCustomers', []))
        );
    }

    getCustomerByCompany(id: any): Observable<Customer[]> {
        return this.http.get<Customer[]>(`${environment.apiUrl}/gvr/customers/company/${id}`)
        .pipe(
            tap(_ => this.log('Fetched Customers By Company')),
            catchError(this.handleError('getCustomerByCompany', []))
        );
    }

    getCustomer(id: any): Observable<Customer> {
        return this.http.get<Customer>(`${environment.apiUrl}/gvr/customers/${id}`)
        .pipe(
            tap(_ => this.log(`Fetched Customer id: ${id}`))
        );
    }

    addCustomer(customer: Customer) {
        return this.http.post(`${environment.apiUrl}/gvr/customers/create`, customer);
    }

    updateCustomer(id: any, customer: Customer) {
        return this.http.put(`${environment.apiUrl}/gvr/customers/${id}`, customer)
        .pipe(
            tap(_ => this.log(`Updated customer id=${id}`))
        );
    }

    deleteCustomer(id: any): Observable<Customer> {
        return this.http.delete<Customer>(`${environment.apiUrl}/gvr/customers/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted customer id=${id}`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}