import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { SidebarService } from '../../../_services/sidebar.service';
import { GvrAlertService } from '../../../_services/gvrAlert.service';
import { AuthenticationService } from '../../../_services/authentication.service';
import { ActivitiesService } from './activities.service';

@Component({
    selector: 'app-activities',
    templateUrl: './activities.component.html',
    styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {
    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //variables
    currentYear: any;
    year: any;
    weekTable: any;
    user: any;
    submit: any;
    dateSubmit: any;
    message: any;

    //dropdown
    public yearsList: Array<any>;
    public selectedYear: Array<any>;
    public dropdownYearsSettings: any;

    constructor(
        private router: Router,
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private authenticationService: AuthenticationService,
        private activitiesService: ActivitiesService
    ) {
        this.dropdownYearsSettings = {
            singleSelection: true,
            idField: 'year',
            textField: 'year',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };
    }


    ngOnInit () {
        this.user = this.authenticationService.currentUserValue;

        this.getYears();

        this.getWeeks(moment().format('YYYY'));

        this.currentYear = moment().format('YYYY');
        
    }


    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    getYears() {
        this.activitiesService.getYears()
        .subscribe((res: any) => {
            this.yearsList = res;
        },
        error => {
            this.message = error;
        });
    }

    onYearSelect(event) {
        this.getWeeks(event);
    }

    getWeeks(year) {
        this.currentYear = year.toString();
        this.year = year.toString();
        const startDate = moment(moment().startOf('year').format(this.year + '-MM-DD'));
        const endDate = moment(moment().endOf('year').format(this.year + '-MM-DD'));
        const totalWeeks = endDate.diff(startDate, 'week');

        const weekData = [];
        for (let i = 1; i <= totalWeeks; i++) {
            const start = moment(this.year).day('Sunday').week(i).format('YYYY/MM/DD');
            const end = moment(this.year).day('Sunday').week(i).add(6, 'd').format('YYYY/MM/DD');
            const report = this.getActivity(i, this.year, this.user._id);
            const data = {
                i,
                start,
                end,
                report
            };

            weekData.push(data);

        }
        this.weekTable = weekData;
    }

    getActivity(week, year, uid) {
        const result = [];
        this.activitiesService.checkSubmit(week, year, uid)
        .subscribe((res: any) => {
            result.push(res);
        },
        error => {
            this.message = error;
        });

        return result;
    }

}
