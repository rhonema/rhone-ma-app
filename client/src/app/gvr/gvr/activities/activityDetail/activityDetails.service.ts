import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {
    NgbModal,
    NgbModalOptions,
    ModalDismissReasons,
    NgbModalRef
  } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../../../../../environments/environment';
import { Activity } from '../../../../_models/activity';


@Injectable({
  providedIn: 'root'
})
export class ActivityDetailsService {

    activityDetails: any;

  constructor(
    private http: HttpClient,
    private modalService: NgbModal
  ) { }

  getActivityDetails(page: any): Observable<Activity[]> {
        return this.http.get<Activity[]>(`${environment.apiUrl}/gvr/activities/activityDetails/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Activity Details')),
            catchError(this.handleError('getContactPersons', []))
        );
    }

    getAllActivityDetails(id: any): Observable<Activity[]> {
        return this.http.get<Activity[]>(`${environment.apiUrl}/gvr/activities/activityDetails/all/${id}`)
        .pipe(
            tap(_ => this.log('Fetched Activity Details')),
            catchError(this.handleError('getAllActivityDetails', []))
        );
    }

    getActivityDetail(id: any, aid: any): Observable<Activity> {
        return this.http.get<Activity>(`${environment.apiUrl}/gvr/activities/activityDetails/${id}/activities/${aid}`)
        .pipe(
            tap(_ => this.log(`Fetched Activity Detail id: ${id}`))
        );
    }

    addActivityDetail(id: any, activity: Activity) {
        const activityDetailData = {
            id: id,
            activity: activity
        }
        return this.http.post(`${environment.apiUrl}/gvr/activities/activityDetails/create`, activityDetailData);
    }

    updateActivityDetail(aid: any, id: any, activity: Activity) {
        const activityDetailData = {
            id: id,
            aid: aid,
            activity: activity
        }
        return this.http.put(`${environment.apiUrl}/gvr/activities/activityDetails/update`, activityDetailData)
        .pipe(
            tap(_ => this.log(`Updated Activity Detail id=${id}`))
        );
    }

    deleteActivityDetail(id: any, aid: any): Observable<Activity> {
        return this.http.delete<Activity>(`${environment.apiUrl}/gvr/activities/activityDetails/${id}/activities/${aid}`)
        .pipe(
            tap(_ => this.log(`Deleted Activity Detail id=${id}`))
        );
    }

    getAnalysisActivity(): Observable<Activity> {
        return this.http.get<Activity>(`${environment.apiUrl}/gvr/activities/activityDetails/analysis`)
        .pipe(
            tap(_ => this.log(`Fetched Activity Detail Analysis`))
        );
    }

    getAnalysisCrm(): Observable<Activity> {
        return this.http.get<Activity>(`${environment.apiUrl}/gvr/activities/activityDetails/crm`)
        .pipe(
            tap(_ => this.log(`Fetched Activity Detail CRM`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}