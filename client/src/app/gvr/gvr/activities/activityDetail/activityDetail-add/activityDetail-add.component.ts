import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { take , first} from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../../_services/gvrAlert.service';
import { ModalService } from '../../../../../_services/modal.service';
import { ActivitiesService } from '../../activities.service';
import { ActivityDetailsService } from '../activityDetails.service';
import { GvrSectionsService } from '../../../../gvr/gvrSections/gvrSections.service';
import { ProductsService } from '../../../../../admin/admin/product/products/products.service';
import { UsersService } from '../../../../../admin/admin/employee/users/users.service';

import { Activity } from '../../../../../_models/activity';
import { ActivityDetail } from '../../../../../_models/activityDetail';
import { STATUS, TIMELINE } from '../../../../../_constant/constant';

@Component({
  selector: 'app-activityDetail-add',
  templateUrl: './activityDetail-add.component.html',
  styleUrls: ['./activityDetail-add.component.css']
})
export class ActivityDetailAddComponent implements OnInit {

    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //dropdown
    public dropdownSectionList: Array<any>;
    public selectedSection: Array<any>;
    public dropdownSectionSettings: any;

    public dropdownGvrCategoryList: Array<any>;
    public selectedGvrCategory: Array<any>;
    public dropdownGvrCategorySettings: any;

    public dropdownProductList: Array<any>;
    public selectedProduct: Array<any>;
    public dropdownProductSettings: any;

    public dropdownUserList: Array<any>;
    public selectedUser: Array<any>;
    public dropdownUserSettings: any;

    public dropdownTimelineList: Array<any>;
    public selectedTimeline: Array<any>;
    public dropdownTimelineSettings: any;

    public dropdownStatusList: Array<any>;
    public selectedStatus: Array<any>;
    public dropdownStatusSettings: any;

    //variable
    id: any;
    type: any;
    cid: any;
    aid: any;
    activity: Activity;
    activityDetail: ActivityDetail;
    week: any;
    message: any;
    showOtherServices = false;
    showButton = false;
    submitted = false;

    constructor(
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private modalService: ModalService,
        private activitiesService: ActivitiesService,
        private activityDetailsService: ActivityDetailsService,
        private gvrSectionsService: GvrSectionsService,
        private productsService: ProductsService,
        private usersService: UsersService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.dropdownSectionSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'gvrSectionName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownGvrCategorySettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'gvrCategoryName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownProductSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'productName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownUserSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'employeeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

          this.dropdownTimelineSettings = {
            singleSelection: true,
            idField: 'timeline',
            textField: 'timeline',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };

          this.dropdownStatusSettings = {
            singleSelection: true,
            idField: 'status',
            textField: 'status',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };
    }

    ngOnInit() {
        this.showButton = false;
        this.activity = new Activity();
        this.activityDetail = new ActivityDetail();
        this.id = this.route.snapshot.paramMap.get('id');
        this.type = this.route.snapshot.paramMap.get('type');
        this.dropdownTimelineList = TIMELINE;
        this.dropdownStatusList = STATUS;
        this.loadAllSections();
        this.loadAllProducts();
        this.loadAllUsers();
        this.getActivity(this.id);

    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    loadAllSections() {
        this.gvrSectionsService.getAllGvrSections()
        .subscribe((res: any) => {
          this.dropdownSectionList = res;
        },
        error => {
          this.message = error;
        });
    }

    loadAllProducts() {
        this.productsService.getAllProducts()
        .subscribe((res: any) => {
          this.dropdownProductList = res;
        },
        error => {
          this.message = error;
        });
    }

    loadAllUsers() {
        this.usersService.getAllUsers()
        .subscribe((res: any) => {
          this.dropdownUserList = res;
        },
        error => {
          this.message = error;
        });
    }

    onSectionSelect(id: any) {
      this.getGvrCategory(id);
    }

    toggleOthers(event: any) {
      if (event.target.checked) {
        this.showOtherServices = true;
      } else {
        this.showOtherServices = false;
      }
    }

    getGvrCategory(id: any) {
      this.cid = id._id;
      this.gvrSectionsService.getGvrSection(id._id)
      .subscribe((res: any) => {
        this.dropdownGvrCategoryList = res.gvrCategories;
      },
      error => {
        this.message = error;
      });
    }

    onGvrCategorySelect(id: any) {
      this.getProduct(id);
    }

    getProduct(id: any) {
      this.productsService.getProductBySubcategory(this.cid, id._id)
      .subscribe((res: any) => {
        this.dropdownProductList = res;
      },
      error => {
        this.message = error;
      });
    }

    getActivity(id: any) {
      this.activitiesService.getActivity(id)
      .subscribe((res: any) => {
        if (res.activityDetail[0] && this.type !== 'edit') {
          this.showButton = true;
        }
        this.week = res.weekNo;
        this.activity = res;
      },
      error => {
        this.message = error;
      });
    }

    openConfirm(id: any, aid: any) {
      this.modalService.confirm(
        'Are you sure want to delete?'
      ).pipe(
        take(1) // take() manages unsubscription for us
      ).subscribe(result => {
          if (result) {
              this.activityDetailsService.deleteActivityDetail(id, aid)
              .pipe(first())
              .subscribe(() => this.getActivity(aid)
              );
              this.gvrAlertService.success('Activity Detail Deleted.', true);
          }
        });
    }

    onSubmit(isValid: Boolean, form: NgForm) {

        if (isValid) {
        this.submitted = true;

        this.gvrAlertService.clear();

        this.activityDetailsService.addActivityDetail(this.id, form.value)
        .pipe(first())
        .subscribe(
            data => {
            form.resetForm();
            this.showButton = true;
            this.submitted = false;
            this.gvrAlertService.success('Activity Detail Added.', true);
            this.getActivity(this.id);
            if (this.type === 'edit') {
              this.router.navigate(['/gvr/activities/edit/' + this.id]);
            } else {
              this.router.navigate(['/gvr/activities/activityDetail/add/' + data]);
            }
            },
            error => {
            this.gvrAlertService.error(error);
            }
        );

        } else {
            return;
        }
    }

}
