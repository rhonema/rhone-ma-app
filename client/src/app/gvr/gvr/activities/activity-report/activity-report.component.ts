import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import { SidebarService } from '../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { ActivitiesService } from '../activities.service';
import { Activity } from '../../../../_models/activity';



@Component({
    selector: 'app-activity-report',
    templateUrl: './activity-report.component.html',
    styleUrls: ['./activity-report.component.css']
})
export class ActivityReportComponent implements OnInit {
    //template
    public sidebarVisible: boolean;
    public isResizing: boolean;

    //variable
    activities = [];
    createdBy: any;
    week: any;
    start: any;
    end: any;
    dateSubmitted: any;
    lateSubmitted: any;
    message: any;

    constructor(
        private router: Router,
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private route: ActivatedRoute,
        private activitiesService: ActivitiesService
    ) {
        this.isResizing = false;
        this.sidebarService.toggle();
    }

    ngOnInit () {
        this.getActivity(this.route.snapshot.params.id, this.route.snapshot.params.year, this.route.snapshot.params.uid);
    }

    getActivity(week, year, uid) {
        this.activitiesService.getWeekActivity(week, year, uid)
        .subscribe((res: any) => {
            console.log(res);
            if (res) {
                this.createdBy = res[0].createdBy[0].employeeName;
                this.start = moment(year).day('Sunday').week(week).format('YYYY/MM/DD');
                this.end = moment(year).day('Sunday').week(week).add(6, 'd').format('YYYY/MM/DD');
                this.week = res[0].weekNo;
                this.dateSubmitted = res[0].dateSubmitted;
                this.lateSubmitted = res[0].lateSubmitted;

                this.activities = res;
            }
        },
        error => {
          this.message = error;
        });
    }
}