import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';
import * as moment from 'moment';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { ActivitiesService } from '../activities.service';
import { AuthenticationService } from '../../../../_services/authentication.service';



@Component({
    selector: 'app-activity-list',
    templateUrl: './activity-list.component.html',
    styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {
    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //variable
    year: any;
    weekNo: any;
    activitiesList: any;
    user: any;
    message: any;

    constructor(
        private router: Router,
        private sidebarService: SidebarService,
        private modalService: ModalService,
        private gvrAlertService: GvrAlertService,
        private authenticationService: AuthenticationService,
        private activitiesService: ActivitiesService,
        private route: ActivatedRoute
    ) {}

    ngOnInit () {
        this.user = this.authenticationService.currentUserValue;
        this.year = this.route.snapshot.params.year;
        this.weekNo = this.route.snapshot.params.id;
        this.getActivities(this.route.snapshot.params.id, this.route.snapshot.params.year, this.user._id);
    }

    getActivities(id, year, uid) {
        this.activitiesService.getAllActivities(id, year, uid)
        .subscribe((res: any) => {
          this.activitiesList = res;
        },
        error => {
          this.message = error;
        });
    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    openConfirm(id: any) {
        this.modalService.confirm(
            'Are you sure want to delete?'
          ).pipe(
            take(1) // take() manages unsubscription for us
          ).subscribe(result => {
              if (result) {
                  this.activitiesService.deleteActivity(id)
                  .pipe(first())
                  .subscribe(() => this.getActivities(this.route.snapshot.params.id, this.route.snapshot.params.year, this.user._id)
                  );
                  this.gvrAlertService.success('Customer Deleted.', true);
              }
            });
    }


}