import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { Activity } from '../../../_models/activity';
import { ContactPerson } from '../../../_models/contactPerson';


@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  constructor(
    private http: HttpClient
  ) { }

    getYears(): Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/gvr/activities/years`)
        .pipe(
            tap(_ => this.log('Fetched All Years')),
            catchError(this.handleError('getYears', []))
        );
    }

    getActivities(page: any): Observable<Activity[]> {
        return this.http.get<Activity[]>(`${environment.apiUrl}/gvr/activities/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Activities')),
            catchError(this.handleError('getActivities', []))
        );
    }

    getAllActivities(id: any, year: any, uid: any): Observable<Activity[]> {
        return this.http.get<Activity[]>(`${environment.apiUrl}/gvr/activities/list/${id}/year/${year}/uid/${uid}`)
        .pipe(
            tap(_ => this.log('Fetched Activities')),
            catchError(this.handleError('getAllActivities', []))
        );
    }

    getWeekActivity(id: any, year: any, uid: any): Observable<Activity[]> {
        return this.http.get<Activity[]>(`${environment.apiUrl}/gvr/activities/report/${id}/year/${year}/uid/${uid}`)
        .pipe(
            tap(_ => this.log('Fetched Week Activities')),
            catchError(this.handleError('getWeekActivity', []))
        );
    }

    getActivity(id: any): Observable<Activity> {
        return this.http.get<Activity>(`${environment.apiUrl}/gvr/activities/${id}`)
        .pipe(
            tap(_ => this.log(`Fetched Activity id: ${id}`))
        );
    }

    addActivity(activity: Activity) {
        return this.http.post(`${environment.apiUrl}/gvr/activities/create`, activity);
    }

    updateActivity(id: any, activity: Activity) {
        return this.http.put(`${environment.apiUrl}/gvr/activities/${id}`, activity)
        .pipe(
            tap(_ => this.log(`Updated Activity id=${id}`))
        );
    }

    deleteActivity(id: any): Observable<Activity> {
        return this.http.delete<Activity>(`${environment.apiUrl}/gvr/activities/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted Activity id=${id}`))
        );
    }

    checkSubmit(week: any, year: any, uid: any) {
        const activity = {
            week: week,
            year: year,
            uid: uid
        }
        return this.http.post(`${environment.apiUrl}/gvr/activities/check`, activity);
    }

    submitActivity(week: any, year: any, uid: any, receipients: any) {
        const activity = {
            week: week,
            year: year,
            uid: uid,
            receipients: receipients
        }
        return this.http.post(`${environment.apiUrl}/gvr/activities/submit`, activity);
    }

    submitClaim(week: any, year: any, uid: any) {
        const activity = {
            week: week,
            year: year,
            uid: uid
        }
        return this.http.post(`${environment.apiUrl}/gvr/activities/submitClaim`, activity);
    }

    unlock(week: any, year: any, uid: any) {
        const activity = {
            week: week,
            year: year,
            uid: uid
        }
        return this.http.post(`${environment.apiUrl}/gvr/activities/unlock`, activity);
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}