import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';
import * as moment from 'moment';

import { SidebarService } from '../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { CustomersService } from '../../customers/customers.service';
import { CompaniesService } from '../../../../admin/admin/setup/companies/companies.service';
import { ActivitiesService } from '../activities.service';
import { Activity } from '../../../../_models/activity';
import { CPTITLE, OBJECTIVE, POPULATION, TITLE } from '../../../../_constant/constant';



@Component({
    selector: 'app-activity-edit',
    templateUrl: './activity-edit.component.html',
    styleUrls: ['./activity-edit.component.css']
})
export class ActivityEditComponent implements OnInit {
    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //dropdown
    public dateList: Array<any>;
    public selectedDate: Array<any>;
    public dropdownDateSettings: any;

    public companyList: Array<any>;
    public selectedCompany: Array<any>;
    public dropdownCompanySettings: any;

    public customerList: Array<any>;
    public selectedCustomer: Array<any>;
    public dropdownCustomerSettings: any;

    public contactPersonList: Array<any>;
    public selectedContactPerson: Array<any>;
    public dropdownContactPersonSettings: any;

    public objectiveList: Array<any>;
    public selectedObjective: Array<any>;
    public dropdownObjectiveSettings: any;

    public populationList: Array<any>;
    public selectedPopulation: Array<any>;
    public dropdownPopulationSettings: any;

    public titleList: Array<any>;
    public selectedTitle: Array<any>;
    public dropdownTitleSettings: any;

    //variable
    weekNo: any;
    year: any;
    week: any;
    activity: Activity;
    message: any;
    selectedCP = [];
    showOtherPurpose = false;
    showActivityDetail = false;
    showAll = true;
    submitted = false;

    constructor(
      private sidebarService: SidebarService,
      private gvrAlertService: GvrAlertService,
      private customersService: CustomersService,
      private companiesService: CompaniesService,
      private activitiesService: ActivitiesService,
      private router: Router,
      private route: ActivatedRoute
      ) {
        this.dropdownDateSettings = {
          singleSelection: true,
          idField: '_id',
          textField: 'date',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownCompanySettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownCustomerSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'customerName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownContactPersonSettings = {
          singleSelection: true,
          idField: '_id',
          textField: 'contactPersonName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownObjectiveSettings = {
          singleSelection: true,
          idField: 'purpose',
          textField: 'purpose',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownPopulationSettings = {
          singleSelection: true,
          idField: 'population',
          textField: 'population',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownTitleSettings = {
          singleSelection: true,
          idField: 'title',
          textField: 'title',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }

      ngOnInit() {
        this.activity = new Activity();
        this.getActivity(this.route.snapshot.params.id);
        this.loadAllCompanies();
        this.objectiveList = OBJECTIVE;
        this.populationList = POPULATION;
        this.titleList = TITLE;
      }

      toggleFullWidth() {
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
      }

      onCompanySelect(id: any) {
        this.getCustomer(id);
      }

      onCustomerSelect(id: any) {
        this.getContactPerson(id);
      }

      onObjectiveSelect(id: any) {
        if (
          id._id === 'OTHERS' ||
          id._id === 'ON LEAVE' ||
          id._id === 'STATION IN OFFICE' ||
          id._id === 'SEMINAR/EXHIBITION/TRAINING' ||
          id._id === 'SALES MEETING/MEETING/DISCUSSION'
        ) {
          this.showAll = false;
        } else {
          this.showAll = true;
        }

        if (id._id === 'OTHERS') {
          this.showOtherPurpose = true;
        } else {
          this.showOtherPurpose = false;
        }

        if (id._id === 'PROMOTE PRODUCT/SERVICE') {
          this.showActivityDetail = true;
        } else {
          this.showActivityDetail = false;
        }
      }

      getActivity(id: any) {
        this.activitiesService.getActivity(id).subscribe((data: any) => {
          console.log(data);


          this.activity = data;
          this.weekNo = data.weekNo;
          this.year = data.year;
          this.week = this.getDateRange(data.weekNo, data.year);
          if (
            data.purpose[0].purpose !== 'ON LEAVE' &&
            data.purpose[0].purpose !== 'STATION IN OFFICE' &&
            data.purpose[0].purpose !== 'SEMINAR/EXHIBITION/TRAINING' &&
            data.purpose[0].purpose !== 'SALES MEETING/MEETING/DISCUSSION'
          ) {
            this.getCustomer(data.company[0]);
            this.getContactPerson(data.customer[0]);
          }

          this.selectedDate = data.activityDate;
          this.selectedCompany = data.company;
          this.selectedCustomer = data.customer;
          this.selectedContactPerson = data.contactPerson;
          this.selectedObjective = data.purpose;
          this.selectedPopulation = data.population;
          this.selectedTitle = data.title;

          this.showAll = (
            data.purpose[0].purpose === 'ON LEAVE' ||
            data.purpose[0].purpose === 'STATION IN OFFICE' ||
            data.purpose[0].purpose === 'SEMINAR/EXHIBITION/TRAINING' ||
            data.purpose[0].purpose === 'SALES MEETING/MEETING/DISCUSSION'
          ) ? false : true;
          this.showOtherPurpose = (data.purpose[0].purpose === 'OTHERS') ? true : false;
          this.showActivityDetail = (data.purpose[0].purpose === 'PROMOTE PRODUCT/SERVICE') ? true : false;

        }, error => console.log(error));
      }

      loadAllCompanies() {
        this.companiesService.getAllCompanies()
        .subscribe((res: any) => {
          this.companyList = res;
        },
        error => {
          this.message = error;
        });
      }

      getDateRange(id: any, year: any) {
        const dateRange = [];
        for (let i = 0; i <= 6; i++) {
          const nextDate = moment(String(year)).day('Sunday').week(id).add(i, 'd').format('YYYY/MM/DD');
          dateRange.push(nextDate);
        }

        return dateRange;
      }

      getCustomer(company: any) {
        this.customersService.getCustomerByCompany(company._id)
        .subscribe((res: any) => {
          this.customerList = res;
        },
        error => {
          this.message = error;
        });
      }

      getContactPerson(person: any) {
        this.customersService.getCustomer(person._id)
        .subscribe((res: any) => {
          this.contactPersonList = res.contactPerson;
        },
        error => {
          this.message = error;
        });
      }

      onSubmit(isValid: Boolean, form: NgForm) {
        if (isValid) {
          this.submitted = true;

          this.gvrAlertService.clear();

          this.activitiesService.updateActivity(form.value.id, form.value)
          .pipe(first())
          .subscribe(
              data => {
              this.gvrAlertService.success('Activity Updated.', true);
              this.router.navigate(['/gvr/activities/list/' + this.weekNo + '/year/' + this.year]);
              },
              error => {
              this.gvrAlertService.error(error);
              }
          );

          } else {
              return;
          }
      }


  }
