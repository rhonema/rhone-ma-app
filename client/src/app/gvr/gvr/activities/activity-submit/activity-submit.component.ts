import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first, catchError } from 'rxjs/operators';
import * as moment from 'moment';

import { SidebarService } from '../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { AuthenticationService } from '../../../../_services/authentication.service';
import { ActivitiesService } from '../activities.service';
import { UsersService } from '../../../../admin/admin/employee/users/users.service';


@Component({
    selector: 'app-activity-submit',
    templateUrl: './activity-submit.component.html',
    styleUrls: ['./activity-submit.component.css']
})
export class ActivitySubmitComponent implements OnInit {
    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    //dropdown
    public usersList: Array<any>;
    public selectedUser: Array<any>;
    public dropdownUsersSettings: any;

    //variables
    week: any;
    year: any;
    user: any;
    receipients: any = [];
    message: any;
    submitted = false;


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        private authenticationService: AuthenticationService,
        private activitiesService: ActivitiesService,
        private usersService: UsersService
    ) {
        this.dropdownUsersSettings = {
            singleSelection: false,
            idField: '_id',
            textField: 'employeeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };
    }


    ngOnInit () {
        this.week = this.route.snapshot.params.id;
        this.year = this.route.snapshot.params.year;
        this.user = this.authenticationService.currentUserValue;
        this.getUsers();
    }


    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    getUsers () {
        this.usersService.getGvrUsers()
        .subscribe((res: any) => {
            if (res) {
                this.usersList = res;
            }
        },
        error => {
          this.message = error;
        });
    }

    default() {
        this.usersService.getUser(this.user._id)
        .subscribe((res: any) => {
            if (res) {
                let receipients = {'receipients': res.gvrNotification};
                
                this.activitiesService.submitActivity(this.week, this.year, this.user._id, receipients)
                .pipe(first())
                .subscribe(
                    data => {
                        this.gvrAlertService.success('Report Submitted', true);
                        this.router.navigate(['/gvr/activities']);
                    },
                    error => {
                    this.gvrAlertService.error(error);
                    }
                );
            }
        },
        error => {
          this.message = error;
        });
    }

    onSubmit(isValid: Boolean, form: NgForm) {
        if (isValid) {

            this.user = JSON.parse(localStorage.getItem('currentUser'));

            this.submitted = true;
            this.gvrAlertService.clear();

           
            this.activitiesService.submitActivity(this.week, this.year, this.user._id, form.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.gvrAlertService.success('Report Submitted', true);
                    this.router.navigate(['/gvr/activities']);
                },
                error => {
                this.gvrAlertService.error(error);
                }
            );
                
          } else {
              return;
          }
    }

    /*
    submit(week, year) {
        this.activitiesService.submitActivity(week, year, this.user._id)
        .subscribe((res: any) => {
            if (res === 1) {
                this.gvrAlertService.success('Report Submitted', true);
            } else {
                this.gvrAlertService.error('No activity found for the week!', true);
            }
        },
        error => {
          this.message = error;
        });
    }
    */

}
