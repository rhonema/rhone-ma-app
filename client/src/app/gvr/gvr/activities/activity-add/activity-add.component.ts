import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first, catchError } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import { SidebarService } from '../../../../_services/sidebar.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { CustomersService } from '../../customers/customers.service';
import { CompaniesService } from '../../../../admin/admin/setup/companies/companies.service';
import { ActivitiesService } from '../activities.service';
import { AuthenticationService } from '../../../../_services/authentication.service';
import { CustomerPositionsService } from '../../customers/customerPositions/customerPositions.service';
import { Customer } from '../../../../_models/customer';
import { Company } from '../../../../_models/company';
import { Activity } from '../../../../_models/activity';
import { ContactPerson } from '../../../../_models/contactPerson';
import { CPTITLE, OBJECTIVE, POPULATION, TITLE } from '../../../../_constant/constant';

@Component({
  selector: 'app-activity-add',
  templateUrl: './activity-add.component.html',
  styleUrls: ['./activity-add.component.css']
})
export class ActivityAddComponent implements OnInit {

    //template
    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    ///dropdown
    public dateList: Array<any>;
    public selectedDate: Array<any>;
    public dropdownDateSettings: any;

    public companyList: Array<any>;
    public selectedCompany: Array<any>;
    public dropdownCompanySettings: any;

    public customerList: Array<any>;
    public selectedCustomer: Array<any>;
    public dropdownCustomerSettings: any;

    public contactPersonList: Array<any>;
    public selectedContactPerson: Array<any>;
    public dropdownContactPersonSettings: any;

    public contactPersonTitleList: Array<any>;
    public selectedContactPersonTitle: Array<any>;
    public dropdownContactPersonTitleSettings: any;

    public objectiveList: Array<any>;
    public selectedObjective: Array<any>;
    public dropdownObjectiveSettings: any;

    public populationList: Array<any>;
    public selectedPopulation: Array<any>;
    public dropdownPopulationSettings: any;

    public titleList: Array<any>;
    public selectedTitle: Array<any>;
    public dropdownTitleSettings: any;

    public dropdownList: Array<any>;
    public selectedItems: Array<any>;
    public dropdownSettings: any;

    //variable
    id: any;
    user: any;
    year: any;
    routeWeek: any;
    week: any;
    customer: Customer;
    company: Company;
    activity: Activity;
    cp: ContactPerson;
    activityDetails: any;
    message: any;
    showOtherPurpose = false;
    showActivityDetail = false;
    showNewContactPerson = false;
    showAll = true;
    addClicked = false;
    nextClicked = false;
    submitted = false;

    constructor(
        private sidebarService: SidebarService,
        private gvrAlertService: GvrAlertService,
        public modalService: NgbModal,
        private customersService: CustomersService,
        private companiesService: CompaniesService,
        private activitiesService: ActivitiesService,
        private authenticationService: AuthenticationService,
        private customerPositionsService: CustomerPositionsService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.dropdownDateSettings = {
          singleSelection: true,
          idField: '_id',
          textField: 'date',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownCompanySettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownCustomerSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'customerName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
        };

        this.dropdownContactPersonSettings = {
          singleSelection: true,
          idField: '_id',
          textField: 'contactPersonName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownContactPersonTitleSettings = {
          singleSelection: true,
          idField: 'contactPersonTitle',
          textField: 'contactPersonTitle',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownObjectiveSettings = {
          singleSelection: true,
          idField: 'purpose',
          textField: 'purpose',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownPopulationSettings = {
          singleSelection: true,
          idField: 'population',
          textField: 'population',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownTitleSettings = {
          singleSelection: true,
          idField: 'title',
          textField: 'title',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

        this.dropdownSettings = {
          singleSelection: false,
          idField: '_id',
          textField: 'customerPositionName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
      };
    }

    ngOnInit() {
        this.customer = new Customer();
        this.activity = new Activity();
        this.cp = new ContactPerson();
        this.year = this.route.snapshot.params.year;
        this.routeWeek = this.route.snapshot.params.id;
        this.week = this.getDateRange(this.route.snapshot.params.id, this.route.snapshot.params.year);
        this.objectiveList = OBJECTIVE;
        this.contactPersonTitleList = CPTITLE;
        this.populationList = POPULATION;
        this.titleList = TITLE;
        this.loadAllCompanies();
        this.loadAllCustomerPositions();
    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    onCompanySelect(id: any) {
      this.getCustomer(id);
    }

    onCustomerSelect(id: any) {
      this.getContactPerson(id);
    }

    onObjectiveSelect(id: any) {
      if (
        id.purpose === 'OTHERS' ||
        id.purpose === 'ON LEAVE' ||
        id.purpose === 'STATION IN OFFICE' ||
        id.purpose === 'SEMINAR/EXHIBITION/TRAINING' ||
        id.purpose === 'SALES MEETING/MEETING/DISCUSSION'
      ) {
        this.showAll = false;
      } else {
        this.showAll = true;
      }

      if (id.purpose === 'OTHERS') {
        this.showOtherPurpose = true;
      } else {
        this.showOtherPurpose = false;
      }

      if (id.purpose === 'PROMOTE PRODUCT/SERVICE') {
        this.showActivityDetail = true;
      } else {
        this.showActivityDetail = false;
      }
    }

    loadAllCompanies() {
      this.companiesService.getAllCompanies()
      .subscribe((res: any) => {
        this.companyList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllCustomerPositions() {
      this.customerPositionsService.getAllPositions()
      .subscribe((res: any) => {
        this.dropdownList = res;
      },
      error => {
        this.message = error;
      });
  }

    getDateRange(id: any, year: any) {
      const dateRange = [];
      for (let i = 0; i <= 6; i++) {
        const nextDate = moment(year).day('Sunday').week(id).add(i, 'd').format('YYYY/MM/DD');
        dateRange.push(nextDate);
      }

      return dateRange;
    }

    getCustomer(company: any) {
      this.customersService.getCustomerByCompany(company._id)
      .subscribe((res: any) => {
        this.customerList = res;
      },
      error => {
        this.message = error;
      });
    }

    getContactPerson(person: any) {
      this.customersService.getCustomer(person._id)
      .subscribe((res: any) => {
        this.contactPersonList = res.contactPerson;
      },
      error => {
        this.message = error;
      });
    }

    public onAddClick(): void {
      this.addClicked = true;
    }

    public onDoneClick(): void {
      this.addClicked = false;
    }

    public onNextClick(): void {
      this.addClicked = false;
      this.nextClicked = true;
    }

    toggleNew(event: any) {
      if (event.target.checked) {
        this.showNewContactPerson = true;
      } else {
        this.showNewContactPerson = false;
      }
    }

    onSubmit(isValid: Boolean, form: NgForm) {

        if (isValid) {

          this.user = JSON.parse(localStorage.getItem('currentUser'));

          this.submitted = true;
          this.gvrAlertService.clear();
          form.value.weekNo = this.route.snapshot.params.id;
          form.value.year = (this.year) ? this.year : moment(form.value.date).format('YYYY');
          form.value.submitted = 0;
          form.value.createdBy = this.user._id;

          this.activitiesService.addActivity(form.value)
          .pipe(first())
          .subscribe(
              data => {
                form.resetForm();
                this.submitted = false;
                if (this.addClicked) {
                  console.log('Add');
                  this.gvrAlertService.success('Activity Added.', true);
                  this.router.navigate(['/gvr/activities/add/' + this.route.snapshot.params.id]);
                } else if (this.nextClicked) {
                  console.log('Next');
                  this.router.navigate(['/gvr/activities/activityDetail/add/' + data + '/from/add']);
                } else {
                  console.log('Done');
                  this.gvrAlertService.success('Activity Added.', true);
                  this.router.navigate(['/gvr/activities/list/' + this.route.snapshot.params.id + '/year/' + this.route.snapshot.params.year]);
                }
              },
              error => {
              this.gvrAlertService.error(error);
              }
          );

        } else {
            return;
        }
    }

}
