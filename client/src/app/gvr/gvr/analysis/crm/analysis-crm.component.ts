import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';
import { Subject } from 'rxjs';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { ActivityDetailsService } from '../../activities/activityDetail/activityDetails.service';

import { Activity } from '../../../../_models/activity';


@Component({
    selector: 'app-analysis-crm',
    templateUrl: './analysis-crm.component.html',
    styleUrls: ['./analysis-crm.component.css']
})
export class AnalysisCrmComponent implements OnInit {
    //template
    public sidebarVisible = true;
    public isResizing = false;

    //dialog modal
    public confirmedResult: boolean;

    activity: Activity[] = [];

    //variable
    message: any;
    activities: any;

    constructor(
        private route: ActivatedRoute,
        private modalService: ModalService,
        private gvrAlertService: GvrAlertService,
        private sidebarService: SidebarService,
        private activityDetailsService: ActivityDetailsService
      ) { }

      ngOnInit() {
        this.getActivities();
      }

      toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
      }

      getActivities() {
        this.activityDetailsService.getAnalysisCrm()
        .subscribe((res: any) => {
          console.log(res);
          this.activities = res;
        },
        error => {
          this.message = error;
        });
      }

}
