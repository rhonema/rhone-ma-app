import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';
import { Subject } from 'rxjs';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { GvrAlertService } from '../../../../_services/gvrAlert.service';
import { ActivityDetailsService } from '../../activities/activityDetail/activityDetails.service';

import { Activity } from '../../../../_models/activity';


@Component({
    selector: 'app-analysis-activities',
    templateUrl: './analysis-activities.component.html',
    styleUrls: ['./analysis-activities.component.css']
})
export class AnalysisActivitiesComponent implements OnInit {
    //template
    public sidebarVisible = true;
    public isResizing = false;

    //dialog modal
    public confirmedResult: boolean;

    activity: Activity[] = [];

    //variable
    message: any;
    activities: any;

    constructor(
        private route: ActivatedRoute,
        private modalService: ModalService,
        private gvrAlertService: GvrAlertService,
        private sidebarService: SidebarService,
        private activityDetailsService: ActivityDetailsService
      ) { }

      ngOnInit() {
        this.getActivities();
      }

      toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
      }

      getActivities() {
        this.activityDetailsService.getAnalysisActivity()
        .subscribe((res: any) => {
          this.activities = res;
        },
        error => {
          this.message = error;
        });
      }

}
