import { Component, OnInit } from '@angular/core';

import { SidebarService } from '../../_services/sidebar.service';

@Component({
  selector: 'app-leaves',
  templateUrl: './leaves.component.html',
  styleUrls: ['./leaves.component.css']
})
export class LeavesComponent implements OnInit {

  //template
  public sidebarVisible = true;
  public isResizing = false;

  constructor(
    private sidebarService: SidebarService,
  ) { }

  ngOnInit() {
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

}
