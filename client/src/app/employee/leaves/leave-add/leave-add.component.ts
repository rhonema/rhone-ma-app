import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../_services/sidebar.service';
import { EmployeeAlertService } from '../../../_services/employeeAlert.service';

@Component({
  selector: 'app-leave-add',
  templateUrl: './leave-add.component.html',
  styleUrls: ['./leave-add.component.css']
})
export class LeaveAddComponent implements OnInit {

    //template
    public sidebarVisible = true;
    public isResizing = false;

    //variable
    id: any;
    message: any;
    submitted = false;

    constructor(
        private sidebarService: SidebarService,
        private employeeAlertService: EmployeeAlertService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
    }

    }
