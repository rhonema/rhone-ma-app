import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import * as moment from 'moment';

import { SidebarService } from '../../_services/sidebar.service';
import { ModalService } from '../../_services/modal.service';
import { AlertService } from '../../_services/alert.service';
import { Calendar } from '../../_models/calendar';
import { EventsService } from '../../admin/admin/setup/events/events.service';

@Component({
  selector: 'app-calendars',
  templateUrl: './calendars.component.html',
  styleUrls: ['./calendars.component.css']
})
export class CalendarsComponent implements OnInit {

  //template
  public sidebarVisible = true;
  public isResizing = false;

  @ViewChild('calendar', { static: true }) calendarComponent: FullCalendarComponent;

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [];


  //variable
  calendar: Calendar[] = [];
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private modalService: ModalService,
    private alertService: AlertService,
    public eventsService: EventsService
  ) { }


  ngOnInit() {
    this.loadAllEvents();
  }

  loadAllEvents() {
    this.eventsService.getAllEventsByYear(moment().format('YYYY'))
    .subscribe((res: any) => {
        const eventData = [];
        res.forEach(event => {
          const title = event.title;
          const start = new Date(event.year, event.month-1, event.day);
          const data = {
            title,
            start
          };
          eventData.push(data);
        });
        this.calendarEvents = eventData;
    },
    error => {
      this.message = error;
    });
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    const calendarApi = this.calendarComponent.getApi();
  }

}
