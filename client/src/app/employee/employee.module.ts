import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FullCalendarModule } from '@fullcalendar/angular';

import { routing } from './employee.routing';
import { LayoutModule } from '../_layout/layout.module';
import { PagesModule } from '../pages/pages.module';
import { EmployeeComponent } from './employee/employee.component';
import { IndexComponent } from '../employee/index/index.component';
import { EmployeeAlertComponent } from '../_components/employeeAlert.component';

import { CalendarsComponent } from './calendars/calendars.component';

import { LeavesComponent } from './leaves/leaves.component';
import { LeaveAddComponent } from './leaves/leave-add/leave-add.component';




//General Setup

@NgModule({
  imports: [
    CommonModule,
    routing,
    LayoutModule,
    RouterModule,
    PagesModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AngularMultiSelectModule,
    FullCalendarModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    EmployeeComponent,
    IndexComponent,
    EmployeeAlertComponent,
    CalendarsComponent,
    LeavesComponent,
    LeaveAddComponent
  ]
})
export class EmployeeModule { }
