import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { IndexComponent } from './index/index.component';

import { AuthGuard } from '../_helpers/auth.guard';

import { CalendarsComponent } from './calendars/calendars.component';
import { LeavesComponent } from './leaves/leaves.component';
import { LeaveAddComponent } from './leaves/leave-add/leave-add.component';

//import { LeavesComponent } from './leaves/leaves.component';
//import { LeaveAddComponent } from './leaves/leave-add/leave-add.component';

//
//Employee
//

const routes: Routes = [
    {
        path: '',
        component: EmployeeComponent,
        children: [
            { path: '', redirectTo: 'dashboard'},
            {
              path: 'dashboard',
              children: [
                  {
                    path: '',
                    redirectTo: 'index',
                    pathMatch: 'full'
                  },
                  {
                    path: 'index',
                    component: IndexComponent,
                    canActivate: [AuthGuard],
                    data: {
                      title: 'Employee :: Dashboard'
                    }
                  }
              ]
            },
            //
            //Calendar
            //
            {
              path: 'calendars',
              component: CalendarsComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'Employee :: Calendar'
              }
            },
            //
            //Leave
            //
            {
              path: 'leaves',
              component: LeavesComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'Employee :: Leave'
              }
            },
            {
              path: 'leaves/add',
              component: LeaveAddComponent,
              canActivate: [AuthGuard],
              data: {
                title: 'Employee :: Add Leave'
              }
            }
        ]
    },
];

export const routing = RouterModule.forChild(routes);
