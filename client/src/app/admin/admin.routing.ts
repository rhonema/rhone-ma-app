import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { IndexComponent } from './admin/index/index.component';

import { AuthGuard } from '../_helpers/auth.guard';

import { Role } from '../_models/role';

//
//Admin
//
//General Setup
import { CompaniesComponent } from './admin/setup/companies/companies.component';
import { CompanyAddComponent } from './admin/setup/companies/company-add/company-add.component';
import { CompanyEditComponent } from './admin/setup/companies/company-edit/company-edit.component';

import { IsosComponent } from './admin/setup/iso/isos.component';
import { IsoAddComponent } from './admin/setup/iso/iso-add/iso-add.component';
import { IsoEditComponent } from './admin/setup/iso/iso-edit/iso-edit.component';

import { EventsComponent } from './admin/setup/events/events.component';
import { EventAddComponent } from './admin/setup/events/event-add/event-add.component';
import { EventEditComponent } from './admin/setup/events/event-edit/event-edit.component';

//Employee Setup
import { UsersComponent } from './admin/employee/users/users.component';
import { UserAddComponent } from './admin/employee/users/user-add/user-add.component';
import { UserEditComponent } from './admin/employee/users/user-edit/user-edit.component';

import { GradesComponent } from './admin/employee/grades/grades.component';
import { GradeAddComponent } from './admin/employee/grades/grade-add/grade-add.component';
import { GradeEditComponent } from './admin/employee/grades/grade-edit/grade-edit.component';

import { DepartmentsComponent } from './admin/employee/departments/departments.component';
import { DepartmentAddComponent } from './admin/employee/departments/department-add/department-add.component';
import { DepartmentEditComponent } from './admin/employee/departments/department-edit/department-edit.component';

import { PositionsComponent } from './admin/employee/positions/positions.component';
import { PositionAddComponent } from './admin/employee/positions/position-add/position-add.component';
import { PositionEditComponent } from './admin/employee/positions/position-edit/position-edit.component';

import { SalespersonCodesComponent } from './admin/employee/salespersonCodes/salespersonCodes.component';
import { SalespersonCodeAddComponent } from './admin/employee/salespersonCodes/salespersonCode-add/salespersonCode-add.component';
import { SalespersonCodeEditComponent } from './admin/employee/salespersonCodes/salespersonCode-edit/salespersonCode-edit.component';

//Product Setup
import { CategoriesComponent } from './admin/product/categories/categories.component';
import { CategoryAddComponent } from './admin/product/categories/category-add/category-add.component';
import { CategoryEditComponent } from './admin/product/categories/category-edit/category-edit.component';

import { SubcategoriesComponent } from './admin/product/subcategories/subcategories.component';
import { SubcategoryAddComponent } from './admin/product/subcategories/subcategory-add/subcategory-add.component';
import { SubcategoryEditComponent } from './admin/product/subcategories/subcategory-edit/subcategory-edit.component';

import { ProductsComponent } from './admin/product/products/products.component';
import { ProductAddComponent } from './admin/product/products/product-add/product-add.component';
import { ProductEditComponent } from './admin/product/products/product-edit/product-edit.component';
import { GenerateYearComponent } from './admin/leave/entitlement/generateYear/generateYear.component';


const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            { path: '', redirectTo: 'dashboard'},
            {
              path: 'dashboard',
              children: [
                  {
                    path: '',
                    redirectTo: 'index',
                    pathMatch: 'full'
                  },
                  {
                    path: 'index',
                    component: IndexComponent,
                    canActivate: [AuthGuard],
                    data: {
                      title: 'RHONE MA :: Dashboard',
                      roles: [Role.Admin]
                    }
                  }
              ]
            },
            //
            //Admin
            //
            //General Setup
            {
              path: 'setup',
              children: [
                {
                  path: 'companies',
                  component: CompaniesComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Companies'
                  }
                },
                {
                  path: 'companies/add',
                  component: CompanyAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Company'
                  }
                },
                {
                  path: 'companies/edit/:id',
                  component: CompanyEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Company'
                  }
                },
                {
                  path: 'isos',
                  component: IsosComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Iso Document'
                  }
                },
                {
                  path: 'isos/add',
                  component: IsoAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Iso Document'
                  }
                },
                {
                  path: 'isos/edit/:id',
                  component: IsoEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Iso Document'
                  }
                },
                {
                  path: 'events',
                  component: EventsComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Events'
                  }
                },
                {
                  path: 'events/add',
                  component: EventAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Events'
                  }
                },
                {
                  path: 'events/edit/:id',
                  component: EventEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Events'
                  }
                }
              ]
            },
            //Employee Setup
            {
              path: 'employee',
              children: [
                {
                  path: 'grades',
                  component: GradesComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Grades'
                  }
                },
                {
                  path: 'grades/add',
                  component: GradeAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Grade'
                  }
                },
                {
                  path: 'grades/edit/:id',
                  component: GradeEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Grade'
                  }
                },
                {
                  path: 'departments',
                  component: DepartmentsComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Departments'
                  }
                },
                {
                  path: 'departments/add',
                  component: DepartmentAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Department'
                  }
                },
                {
                  path: 'departments/edit/:id',
                  component: DepartmentEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Department'
                  }
                },
                {
                  path: 'positions',
                  component: PositionsComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Positions'
                  }
                },
                {
                  path: 'positions/add',
                  component: PositionAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Position'
                  }
                },
                {
                  path: 'positions/edit/:id',
                  component: PositionEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Position'
                  }
                },
                {
                  path: 'salespersonCodes',
                  component: SalespersonCodesComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Sales Person Codes'
                  }
                },
                {
                  path: 'salespersonCodes/add',
                  component: SalespersonCodeAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Sales Person Code'
                  }
                },
                {
                  path: 'salespersonCodes/edit/:id',
                  component: SalespersonCodeEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Sales Person Code'
                  }
                },
                {
                  path: 'users',
                  component: UsersComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Employees'
                  }
                },
                {
                  path: 'users/add',
                  component: UserAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Employee'
                  }
                },
                {
                  path: 'users/edit/:id',
                  component: UserEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Employee'
                  }
                }
              ]
            },
            //Leave Setup
            {
              path: 'leave/entitlements',
              children: [
                {
                  path: 'generateYear',
                  component: GenerateYearComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Generate Leave Year'
                  }
                }
              ]
            },
            //Product Setup
            {
              path: 'product',
              children: [
                {
                  path: 'categories',
                  component: CategoriesComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Categories'
                  }
                },
                {
                  path: 'categories/add',
                  component: CategoryAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Category'
                  }
                },
                {
                  path: 'categories/edit/:id',
                  component: CategoryEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Category'
                  }
                },
                {
                  path: 'subcategories',
                  component: SubcategoriesComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Sub Categories'
                  }
                },
                {
                  path: 'subcategories/add',
                  component: SubcategoryAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Sub Category'
                  }
                },
                {
                  path: 'subcategories/edit/:id',
                  component: SubcategoryEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Sub Category'
                  }
                },
                {
                  path: 'products',
                  component: ProductsComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Products'
                  }
                },
                {
                  path: 'products/add',
                  component: ProductAddComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Add Product'
                  }
                },
                {
                  path: 'products/edit/:id',
                  component: ProductEditComponent,
                  canActivate: [AuthGuard],
                  data: {
                    title: 'RHONE MA :: Edit Product'
                  }
                }
              ]
            }
        ]
    },

    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forChild(routes);
