import { Component, OnInit, OnDestroy } from '@angular/core';

import { SidebarService } from '../../../_services/sidebar.service';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {

    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;


    constructor(
      private sidebarService: SidebarService
    ) {

    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    toggleFullWidth() {
      this.isResizing = true;
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
  }



}
