import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { LeaveEntitlement } from '../../../../_models/leaveEntitlement';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntitlementsService {

    constructor(
        private http: HttpClient
    ) { }

    generateYear(year: any) {
      return this.http.get(`${environment.apiUrl}/leave/entitlements/generateYear/${year}`)
      .pipe(
          tap(_ => this.log(`Generate Leave Year`))
      );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }

}
