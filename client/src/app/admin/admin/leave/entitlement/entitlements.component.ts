import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { SidebarService } from '../../../../_services/sidebar.service';
import { EntitlementsService } from './entitlements.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';


@Component({
  selector: 'app-entitlements',
  templateUrl: './entitlements.component.html',
  styleUrls: ['./entitlements.component.css']
})
export class EntitlementsComponent implements OnInit {

  //template
  public sidebarVisible = true;
  public isResizing = false;

  //dialog modal
  public confirmedResult: boolean;

  //variable
  message: any;

  constructor(
    private router: Router,
    private sidebarService: SidebarService,
    private entitlementsService: EntitlementsService,
    private modalService: ModalService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

}
