import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { EntitlementsService } from './../entitlements.service';
import { ModalService } from '../../../../../_services/modal.service';
import { AlertService } from '../../../../../_services/alert.service';


@Component({
  selector: 'app-generateYear',
  templateUrl: './generateYear.component.html',
  styleUrls: ['./generateYear.component.css']
})
export class GenerateYearComponent implements OnInit {

  //template
  public sidebarVisible = true;
  public isResizing = false;

  //dialog modal
  public confirmedResult: boolean;

  //variable
  message: any;
  nextYear: any;
  submitted = false;

  constructor(
    private router: Router,
    private sidebarService: SidebarService,
    private entitlementsService: EntitlementsService,
    private modalService: ModalService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.nextYear = moment().add(1, 'years').format('YYYY');
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.entitlementsService.generateYear(this.nextYear)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Next Year Leave Generated.', true);
          //this.router.navigate(['/admin/leave/entitlement']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
