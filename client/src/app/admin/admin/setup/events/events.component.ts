import { Component, OnInit } from '@angular/core';
import { take , first} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';
import { EventsService } from './events.service';
import { Event } from '../../../../_models/event';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  //template
  public sidebarVisible = true;
  public isResizing = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  event: Event[] = [];
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private modalService: ModalService,
    private alertService: AlertService,
    private eventsService: EventsService
  ) { }


  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllEvents(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.eventsService.deleteEvent(id)
            .pipe(first())
            .subscribe(() => this.loadAllEvents(1)
            );
            this.alertService.success('Event Deleted.', true);
        }
      });
  }

  private loadAllEvents(page) {
    this.eventsService.getEvents(page)
    .subscribe((res: any) => {
      console.log(res);
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
