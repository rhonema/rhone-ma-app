import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { NgbDateStruct, NgbCalendar, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { EventsService } from '../events.service';
import { Event } from '../../../../../_models/event';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.css']
})
export class EventEditComponent implements OnInit {


  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //datepicker
  model: NgbDateStruct;
  date: { year: number, month: number };
  @ViewChild('dp', {static: true}) dp: NgbDatepicker;

  //drop down
  public holidayList: Array<any>;
  public selectedHoliday: Array<any>;
  public holidaySettings: any;


  //variable
  id = '';
  message: any;
  event: Event;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private eventsService: EventsService,
    private router: Router,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private calendar: NgbCalendar
    ) {
      this.holidayList = [
        {'holiday': 'YES'},
        {'holiday': 'NO'}
      ];

      this.holidaySettings = {
        singleSelection: true,
        idField: 'holiday',
        textField: 'holiday',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };
    }

    ngOnInit() {
      this.event = new Event();
      this.getEvent(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getEvent(id: any) {
      this.eventsService.getEvent(id).subscribe((data: any) => {
        this.event = data;
        this.model = { year: data.year, month: data.month, day: data.day };
        this.selectedHoliday = data.holiday;
      }, error => console.log(error));
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();

        this.eventsService.updateEvent(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Event Updated.', true);
            this.router.navigate(['/admin/setup/events']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
