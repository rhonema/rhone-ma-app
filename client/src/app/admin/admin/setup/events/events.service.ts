import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Event } from '../../../../_models/event';


@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(
    private http: HttpClient
  ) { }

    getEvents(page: any): Observable<Event[]> {
        return this.http.get<Event[]>(`${environment.apiUrl}/events/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Events')),
            catchError(this.handleError('getEvents', []))
        );
    }

    getAllEvents(): Observable<Event[]> {
        return this.http.get<Event[]>(`${environment.apiUrl}/events/all`)
        .pipe(
            tap(_ => this.log('Fetched Events')),
            catchError(this.handleError('getAllEvents', []))
        );
    }

    getAllEventsByYear(id: any): Observable<Event[]> {
        return this.http.get<Event[]>(`${environment.apiUrl}/events/all/${id}`)
        .pipe(
            tap(_ => this.log('Fetched Events')),
            catchError(this.handleError('getAllEventsByYear', []))
        );
    }

    getEvent(id: any): Observable<Event> {
        return this.http.get<Event>(`${environment.apiUrl}/events/${id}`)
        .pipe(
            tap(_ => this.log(`Fetched Event id: ${id}`))
        );
    }

    addEvent(iso: Event) {
        return this.http.post(`${environment.apiUrl}/events/create`, iso);
    }

    updateEvent(id: any, iso: Event) {
        return this.http.put(`${environment.apiUrl}/events/${id}`, iso)
        .pipe(
            tap(_ => this.log(`Updated Event id=${id}`))
        );
    }

    deleteEvent(id: any): Observable<Event> {
        return this.http.delete<Event>(`${environment.apiUrl}/events/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted Event id=${id}`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}