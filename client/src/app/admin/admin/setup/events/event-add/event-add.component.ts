import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgbDateStruct, NgbCalendar, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { EventsService } from '../events.service';
import { Event } from '../../../../../_models/event';

@Component({
  selector: 'app-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.css']
})
export class EventAddComponent implements OnInit {

  //template
  public sidebarVisible = true;
  public isResizing = false;

  //datepicker
  model: NgbDateStruct;
  date: { year: number, month: number };
  @ViewChild('dp', {static: true}) dp: NgbDatepicker;

  //variable
  message: any;
  submitted = false;
  event: Event;

  //drop down
  public holidayList: Array<any>;
  public selectedHoliday: Array<any>;
  public holidaySettings: any;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private eventsService: EventsService,
    private router: Router,
    private calendar: NgbCalendar
  ) {
    this.holidayList = [
      {'holiday': 'YES'},
      {'holiday': 'NO'}
    ];

    this.holidaySettings = {
      singleSelection: true,
      idField: 'holiday',
      textField: 'holiday',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
  }

  ngOnInit() {
    this.event = new Event();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  setCurrent() {
    //Current Date
    this.dp.navigateTo();
  }

  navigateEvent(event) {
    this.date = event.next;
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.eventsService.addEvent(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Event Added.', true);
          this.router.navigate(['/admin/setup/events']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
