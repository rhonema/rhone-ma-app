import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { CompaniesService } from '../companies.service';
import { Company } from '../../../../../_models/company';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.css']
})
export class CompanyEditComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  company: Company;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private companiesService: CompaniesService,
    private router: Router,
    private route: ActivatedRoute
    ) {}

    ngOnInit() {
      this.company = new Company();
      this.getCompany(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getCompany(id: any) {
      this.companiesService.getCompany(id).subscribe((data: any) => {
        this.company = data;
      }, error => console.log(error));
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();

        this.companiesService.updateCompany(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Company Updated.', true);
            this.router.navigate(['/admin/setup/companies']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
