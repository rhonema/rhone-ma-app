import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Company } from '../../../../_models/company';


@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  constructor(
    private http: HttpClient
  ) { }

    getCompanies(page: any): Observable<Company[]> {
        return this.http.get<Company[]>(`${environment.apiUrl}/companies/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Companies')),
            catchError(this.handleError('getCompanies', []))
        );
    }

    getAllCompanies(): Observable<Company[]> {
        return this.http.get<Company[]>(`${environment.apiUrl}/companies/all`)
        .pipe(
            tap(_ => this.log('Fetched Companies')),
            catchError(this.handleError('getAllCompanies', []))
        );
    }

    getCompany(id: any): Observable<Company> {
        return this.http.get<Company>(`${environment.apiUrl}/companies/${id}`)
        .pipe(
            tap(_ => this.log(`Fetched Company id: ${id}`))
        );
    }

    addCompany(company: Company) {
        return this.http.post(`${environment.apiUrl}/companies/create`, company);
    }

    updateCompany(id: any, company: Company) {
        return this.http.put(`${environment.apiUrl}/companies/${id}`, company)
        .pipe(
            tap(_ => this.log(`Updated company id=${id}`))
        );
    }

    deleteCompany(id: any): Observable<Company> {
        return this.http.delete<Company>(`${environment.apiUrl}/companies/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted company id=${id}`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}