import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { CompaniesService } from '../companies.service';
import { Company } from '../../../../../_models/company';

@Component({
  selector: 'app-company-add',
  templateUrl: './company-add.component.html',
  styleUrls: ['./company-add.component.css']
})
export class CompanyAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  submitted = false;
  company: Company;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private companiesService: CompaniesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.company = new Company();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.companiesService.addCompany(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Company Added.', true);
          this.router.navigate(['/admin/setup/companies']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
