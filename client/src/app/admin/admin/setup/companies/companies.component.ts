import { Component, OnInit } from '@angular/core';
import { take , first} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';
import { CompaniesService } from './companies.service';
import { Company } from '../../../../_models/company';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  company: Company[] = [];
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private modalService: ModalService,
    private alertService: AlertService,
    private companiesService: CompaniesService
  ) { }


  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllCompanies(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.companiesService.deleteCompany(id)
            .pipe(first())
            .subscribe(() => this.loadAllCompanies(1)
            );
            this.alertService.success('Company Deleted.', true);
        }
      });
  }

  private loadAllCompanies(page) {
    this.companiesService.getCompanies(page)
    .subscribe((res: any) => {
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
