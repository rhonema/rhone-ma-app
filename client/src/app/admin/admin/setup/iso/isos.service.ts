import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Iso } from '../../../../_models/iso';


@Injectable({
  providedIn: 'root'
})
export class IsosService {

  constructor(
    private http: HttpClient
  ) { }

    getIsos(page: any): Observable<Iso[]> {
        return this.http.get<Iso[]>(`${environment.apiUrl}/isos/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Isos')),
            catchError(this.handleError('getIsos', []))
        );
    }

    getAllIsos(): Observable<Iso[]> {
        return this.http.get<Iso[]>(`${environment.apiUrl}/isos/all`)
        .pipe(
            tap(_ => this.log('Fetched Isos')),
            catchError(this.handleError('getAllIsos', []))
        );
    }

    getIso(id: any): Observable<Iso> {
        return this.http.get<Iso>(`${environment.apiUrl}/isos/${id}`)
        .pipe(
            tap(_ => this.log(`Fetched Iso id: ${id}`))
        );
    }

    addIso(iso: Iso) {
        return this.http.post(`${environment.apiUrl}/isos/create`, iso);
    }

    updateIso(id: any, iso: Iso) {
        return this.http.put(`${environment.apiUrl}/isos/${id}`, iso)
        .pipe(
            tap(_ => this.log(`Updated Iso id=${id}`))
        );
    }

    deleteIso(id: any): Observable<Iso> {
        return this.http.delete<Iso>(`${environment.apiUrl}/isos/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted Iso id=${id}`))
        );
    }

    deleteFile(id: any): Observable<Iso> {
        return this.http.delete<Iso>(`${environment.apiUrl}/isos/file/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted Iso Document id=${id}`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}