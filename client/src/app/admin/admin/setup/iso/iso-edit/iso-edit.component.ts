import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { UploadService } from '../../../../../_services/upload.service';
import { IsosService } from '../isos.service';
import { Iso } from '../../../../../_models/iso';

@Component({
  selector: 'app-iso-edit',
  templateUrl: './iso-edit.component.html',
  styleUrls: ['./iso-edit.component.css']
})
export class IsoEditComponent implements OnInit {

  @Input()
  file: string;
  url: SafeResourceUrl;

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //drop down
  public fileTypeList: Array<any>;
  public selectedFileType: Array<any>;
  public fileTypeSettings: any;

  //upload
  fileObj: File;
  filename: string;


  //variable
  id = '';
  message: any;
  iso: Iso;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private uploadService: UploadService,
    private isosService: IsosService,
    private router: Router,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer
    ) {
      this.fileTypeList = [
        {'fileType': 'PROCEDURES'},
        {'fileType': 'WORK INSTRUCTIONS'}
      ];

      this.fileTypeSettings = {
        singleSelection: true,
        idField: 'fileType',
        textField: 'fileType',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };
    }

    ngOnInit() {
      this.iso = new Iso();
      this.getIso(this.route.snapshot.params.id);
    }

    onFilePicked(event: Event): void {
      const FILE = (event.target as HTMLInputElement).files[0];
      this.fileObj = FILE;
    }

    onFileUpload() {
      const fileForm = new FormData();
      fileForm.append('file', this.fileObj);
      this.uploadService.fileUpload(fileForm)
      .subscribe(res => {
        this.filename = res['file'];
      });
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getIso(id: any) {
      this.isosService.getIso(id).subscribe((data: any) => {
        this.iso = data;
        this.filename = data.filename;
        this.file = 'https://elasticbeanstalk-ap-southeast-1-633013030562.s3-ap-southeast-1.amazonaws.com/iso/' + this.filename;
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.file);
        this.selectedFileType = data.fileType;
      }, error => console.log(error));
    }

    deleteFile(id: any) {
      this.isosService.deleteFile(id).subscribe((data: any) => {
         this.filename = '';
      }, error => console.log(error));
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();
        if (this.filename) {
          form.value.filename = this.filename;
        }

        this.isosService.updateIso(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Iso Document Updated.', true);
            this.router.navigate(['/admin/setup/isos']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
