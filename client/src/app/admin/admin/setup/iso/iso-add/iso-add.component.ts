import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { UploadService } from '../../../../../_services/upload.service';
import { IsosService } from '../isos.service';
import { Iso } from '../../../../../_models/iso';

@Component({
  selector: 'app-iso-add',
  templateUrl: './iso-add.component.html',
  styleUrls: ['./iso-add.component.css']
})
export class IsoAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  submitted = false;
  iso: Iso;

  //upload
  fileObj: File;
  filename: string;

  //drop down
  public fileTypeList: Array<any>;
  public selectedFileType: Array<any>;
  public fileTypeSettings: any;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private uploadService: UploadService,
    private isosService: IsosService,
    private router: Router
  ) {
    this.fileTypeList = [
      {'fileType': 'PROCEDURES'},
      {'fileType': 'WORK INSTRUCTIONS'}
    ];

    this.fileTypeSettings = {
      singleSelection: true,
      idField: 'fileType',
      textField: 'fileType',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
  }

  ngOnInit() {
    this.iso = new Iso();
  }

  onFilePicked(event: Event): void {
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
  }

  onFileUpload() {
    const fileForm = new FormData();
    fileForm.append('file', this.fileObj);
    this.uploadService.fileUpload(fileForm)
    .subscribe(res => {
      this.filename = res['file'];
    });
  }


  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      form.value.filename = this.filename;

      this.isosService.addIso(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Iso Document Added.', true);
          this.router.navigate(['/admin/setup/isos']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
