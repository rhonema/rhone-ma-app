import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { DepartmentsService } from '../departments.service';
import { Department } from '../../../../../_models/department';

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})
export class DepartmentEditComponent implements OnInit {
  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  department: Department;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private departmentsService: DepartmentsService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

    ngOnInit() {
      this.department = new Department();
      this.getDepartment(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getDepartment(id: any) {
      this.departmentsService.getDepartment(id).subscribe((data: any) => {
        this.department = data;
      }, error => console.log(error));
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        this.departmentsService.updateDepartment(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Department Updated.', true);
            this.router.navigate(['/admin/employee/departments']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }
    }

}
