import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { DepartmentsService } from '../departments.service';
import { Department } from '../../../../../_models/department';

@Component({
  selector: 'app-department-add',
  templateUrl: './department-add.component.html',
  styleUrls: ['./department-add.component.css']
})
export class DepartmentAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  department: Department;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private departmentsService: DepartmentsService,
    private router: Router
    ) { }

    ngOnInit() {
      this.department = new Department();
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        this.departmentsService.addDepartment(form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('New Position Added.', true);
            this.router.navigate(['/admin/employee/positions']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }
    }


}
