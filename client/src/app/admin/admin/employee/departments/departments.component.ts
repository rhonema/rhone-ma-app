import { Component, OnInit } from '@angular/core';
import { take , first} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';
import { DepartmentsService } from './departments.service';
import { Department } from '../../../../_models/department';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  departments: Department[] = [];
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private modalService: ModalService,
    private alertService: AlertService,
    private departmentsService: DepartmentsService
  ) { }


  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllDepartments(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.departmentsService.deleteDepartment(id)
            .pipe(first())
            .subscribe(() => this.loadAllDepartments(1)
            );
            this.alertService.success('Position Deleted.', true);
        }
      });
  }

  private loadAllDepartments(page) {
    this.departmentsService.getDepartments(page)
    .subscribe((res: any) => {
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
