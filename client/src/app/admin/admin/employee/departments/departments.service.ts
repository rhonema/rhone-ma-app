import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Department } from '../../../../_models/department';


@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {

  constructor(
    private http: HttpClient
  ) { }

  getDepartments(page: any): Observable<Department[]> {
    return this.http.get<Department[]>(`${environment.apiUrl}/departments/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetch departments')),
      catchError(this.handleError('getDepartments', []))
    );
  }

  getAllDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(`${environment.apiUrl}/departments/all`)
    .pipe(
      tap(_ => this.log('Fetch departments')),
      catchError(this.handleError('getAllDepartments', []))
    );
  }

  getDepartment(id: any): Observable<Department> {
    return this.http.get<Department>(`${environment.apiUrl}/departments/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch department by ID=${id}`)),
      catchError(this.handleError<Department>(`getDepartment id=${id}`))
    );
  }

  addDepartment(department: Department) {
    return this.http.post(`${environment.apiUrl}/departments/create`, department);
  }

  updateDepartment(id: any, department: Department): Observable<any> {
    return this.http.put(`${environment.apiUrl}/departments/${id}`, department)
    .pipe(
      tap(_ => this.log(`Updated department id=${id}`))
    );
  }

  deleteDepartment(id: any): Observable<Department> {
    return this.http.delete<Department>(`${environment.apiUrl}/departments/${id}`)
    .pipe(
      tap(_ => this.log(`Deleted department id=${id}`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
