import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { PositionsService } from '../positions.service';
import { Position } from '../../../../../_models/position';

@Component({
  selector: 'app-position-edit',
  templateUrl: './position-edit.component.html',
  styleUrls: ['./position-edit.component.css']
})
export class PositionEditComponent implements OnInit {
  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  position: Position;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private positionsService: PositionsService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

    ngOnInit() {
      this.position = new Position();
      this.getPosition(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getPosition(id: any) {
      this.positionsService.getPosition(id).subscribe((data: any) => {
        this.position = data;
      }, error => console.log(error));
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        this.positionsService.updatePosition(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Position Updated.', true);
            this.router.navigate(['/admin/employee/positions']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }
    }

}
