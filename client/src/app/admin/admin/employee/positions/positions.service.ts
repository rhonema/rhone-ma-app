import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Position } from '../../../../_models/position';


@Injectable({
  providedIn: 'root'
})
export class PositionsService {

  constructor(
    private http: HttpClient
  ) { }

  getPositions(page: any): Observable<Position[]> {
    return this.http.get<Position[]>(`${environment.apiUrl}/positions/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetch Positions')),
      catchError(this.handleError('getPositions', []))
    );
  }

  getAllPositions(): Observable<Position[]> {
    return this.http.get<Position[]>(`${environment.apiUrl}/positions/all`)
    .pipe(
      tap(_ => this.log('Fetch Positions')),
      catchError(this.handleError('getPositions', []))
    );
  }

  getPosition(id: any): Observable<Position> {
    return this.http.get<Position>(`${environment.apiUrl}/positions/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch position by ID=${id}`)),
      catchError(this.handleError<Position>(`getPosition id=${id}`))
    );
  }

  addPosition(position: Position) {
    return this.http.post(`${environment.apiUrl}/positions/create`, position);
  }

  updatePosition(id: any, position: Position): Observable<any> {
    return this.http.put(`${environment.apiUrl}/positions/${id}`, position)
    .pipe(
      tap(_ => this.log(`Updated position id=${id}`))
    );
  }

  deletePosition(id: any): Observable<Position> {
    return this.http.delete<Position>(`${environment.apiUrl}/positions/${id}`)
    .pipe(
      tap(_ => this.log(`Deleted position id=${id}`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
