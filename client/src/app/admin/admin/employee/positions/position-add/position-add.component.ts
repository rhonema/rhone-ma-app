import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { PositionsService } from '../positions.service';
import { Position } from '../../../../../_models/position';

@Component({
  selector: 'app-position-add',
  templateUrl: './position-add.component.html',
  styleUrls: ['./position-add.component.css']
})
export class PositionAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  position: Position;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private positionsService: PositionsService,
    private router: Router
    ) { }

    ngOnInit() {
      this.position = new Position();
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        this.positionsService.addPosition(form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('New Position Added.', true);
            this.router.navigate(['/admin/employee/positions']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }
    }


}
