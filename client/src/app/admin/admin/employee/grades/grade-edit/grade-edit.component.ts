import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { GradesService } from '../grades.service';
import { Grade } from '../../../../../_models/grade';

@Component({
  selector: 'app-grade-edit',
  templateUrl: './grade-edit.component.html',
  styleUrls: ['./grade-edit.component.css']
})
export class GradeEditComponent implements OnInit {
  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  grade: Grade;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private gradesService: GradesService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

    ngOnInit() {
      this.grade = new Grade();
      this.getDepartment(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getDepartment(id: any) {
      this.gradesService.getGrade(id).subscribe((data: any) => {
        this.grade = data;
      }, error => console.log(error));
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        this.gradesService.updateGrade(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Grade Updated.', true);
            this.router.navigate(['/admin/employee/grades']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }
    }

}
