import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Grade } from '../../../../_models/grade';


@Injectable({
  providedIn: 'root'
})
export class GradesService {

  constructor(
    private http: HttpClient
  ) { }

  getGrades(page: any): Observable<Grade[]> {
    return this.http.get<Grade[]>(`${environment.apiUrl}/grades/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetch grades')),
      catchError(this.handleError('getGrades', []))
    );
  }

  getAllGrades(): Observable<Grade[]> {
    return this.http.get<Grade[]>(`${environment.apiUrl}/grades/all`)
    .pipe(
      tap(_ => this.log('Fetch grades')),
      catchError(this.handleError('getAllGrades', []))
    );
  }

  getGrade(id: any): Observable<Grade> {
    return this.http.get<Grade>(`${environment.apiUrl}/grades/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch grade by ID=${id}`)),
      catchError(this.handleError<Grade>(`getGrade id=${id}`))
    );
  }

  addGrade(grade: Grade) {
    return this.http.post(`${environment.apiUrl}/grades/create`, grade);
  }

  updateGrade(id: any, grade: Grade): Observable<any> {
    return this.http.put(`${environment.apiUrl}/grades/${id}`, grade)
    .pipe(
      tap(_ => this.log(`Updated grade id=${id}`))
    );
  }

  deleteGrade(id: any): Observable<Grade> {
    return this.http.delete<Grade>(`${environment.apiUrl}/grades/${id}`)
    .pipe(
      tap(_ => this.log(`Deleted grade id=${id}`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
