import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { GradesService } from '../grades.service';
import { Grade } from '../../../../../_models/grade';

@Component({
  selector: 'app-grade-add',
  templateUrl: './grade-add.component.html',
  styleUrls: ['./grade-add.component.css']
})
export class GradeAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  grade: Grade;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private gradesService: GradesService,
    private router: Router
    ) { }

    ngOnInit() {
      this.grade = new Grade();
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        this.gradesService.addGrade(form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('New Grade Added.', true);
            this.router.navigate(['/admin/employee/grades']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }
    }


}
