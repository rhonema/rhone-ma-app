import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { SalespersonCode } from '../../../../_models/salespersonCode';


@Injectable({
  providedIn: 'root'
})
export class SalespersonCodesService {

  constructor(
    private http: HttpClient
  ) { }

    getSalespersonCodes(page: any): Observable<SalespersonCode[]> {
        return this.http.get<SalespersonCode[]>(`${environment.apiUrl}/salespersonCodes/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Salesperson Code')),
            catchError(this.handleError('getSalespersonCodes', []))
        );
    }

    getAllSalespersonCodes(): Observable<SalespersonCode[]> {
        return this.http.get<SalespersonCode[]>(`${environment.apiUrl}/salespersonCodes/all`)
        .pipe(
            tap(_ => this.log('Fetched Salesperson Code')),
            catchError(this.handleError('getAllSalespersonCodes', []))
        );
    }

    getSalespersonCode(id: any): Observable<SalespersonCode> {
        return this.http.get<SalespersonCode>(`${environment.apiUrl}/salespersonCodes/${id}`)
        .pipe(
            tap(_ => this.log(`Fetched Salesperson Code id: ${id}`))
        );
    }

    addSalespersonCode(salespersonCode: SalespersonCode) {
        return this.http.post(`${environment.apiUrl}/salespersonCodes/create`, salespersonCode);
    }

    updateSalespersonCode(id: any, salespersonCode: SalespersonCode) {
        return this.http.put(`${environment.apiUrl}/salespersonCodes/${id}`, salespersonCode)
        .pipe(
            tap(_ => this.log(`Updated Salesperson Code id=${id}`))
        );
    }

    deleteSalespersonCode(id: any): Observable<SalespersonCode> {
        return this.http.delete<SalespersonCode>(`${environment.apiUrl}/salespersonCodes/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted Salesperson Code id=${id}`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}