import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { SalespersonCodesService } from '../salespersonCodes.service';
import { SalespersonCode } from '../../../../../_models/salespersonCode';

@Component({
  selector: 'app-salespersonCode-edit',
  templateUrl: './salespersonCode-edit.component.html',
  styleUrls: ['./salespersonCode-edit.component.css']
})
export class SalespersonCodeEditComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  code: SalespersonCode;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private salespersonCodesService: SalespersonCodesService,
    private router: Router,
    private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.code = new SalespersonCode();
        this.getSalespersonCode(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getSalespersonCode(id: any) {
      this.salespersonCodesService.getSalespersonCode(id).subscribe((data: any) => {
        this.code = data;
        console.log(this.code);
      }, error => console.log(error));
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();

        this.salespersonCodesService.updateSalespersonCode(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Salesperson Code Updated.', true);
            this.router.navigate(['/admin/employee/salespersonCodes']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
