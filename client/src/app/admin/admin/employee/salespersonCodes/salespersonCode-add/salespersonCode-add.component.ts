import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { SalespersonCodesService } from '../salespersonCodes.service';
import { SalespersonCode } from '../../../../../_models/salespersonCode';

@Component({
  selector: 'app-salespersonCode-add',
  templateUrl: './salespersonCode-add.component.html',
  styleUrls: ['./salespersonCode-add.component.css']
})
export class SalespersonCodeAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  submitted = false;
  code: SalespersonCode;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private salespersonCodesService: SalespersonCodesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.code = new SalespersonCode();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.salespersonCodesService.addSalespersonCode(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Salesperson Code Added.', true);
          this.router.navigate(['/admin/employee/salespersonCodes']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
