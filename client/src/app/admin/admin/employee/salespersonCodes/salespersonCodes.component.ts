import { Component, OnInit } from '@angular/core';
import { take , first} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';
import { SalespersonCodesService } from './salespersonCodes.service';
import { SalespersonCode } from '../../../../_models/salespersonCode';

@Component({
  selector: 'app-salespersonCodes',
  templateUrl: './salespersonCodes.component.html',
  styleUrls: ['./salespersonCodes.component.css']
})
export class SalespersonCodesComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  salespersonCode: SalespersonCode[] = [];
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private modalService: ModalService,
    private alertService: AlertService,
    private salespersonCodesService: SalespersonCodesService
  ) { }


  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllSalespersonCodes(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.salespersonCodesService.deleteSalespersonCode(id)
            .pipe(first())
            .subscribe(() => this.loadAllSalespersonCodes(1)
            );
            this.alertService.success('Salesperson Code Deleted.', true);
        }
      });
  }

  private loadAllSalespersonCodes(page) {
    this.salespersonCodesService.getSalespersonCodes(page)
    .subscribe((res: any) => {
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
