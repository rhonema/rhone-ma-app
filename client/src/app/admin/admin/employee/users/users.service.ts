import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { User } from '../../../../_models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ) { }

  getUsers(page: any): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/users/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetch Users')),
      catchError(this.handleError('getUsers', []))
    );
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/users/all`)
    .pipe(
      tap(_ => this.log('Fetch Users')),
      catchError(this.handleError('getAllUsers', []))
    );
  }

  getGvrUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/users/gvr`)
    .pipe(
      tap(_ => this.log('Fetch GVR Users')),
      catchError(this.handleError('getGvrUsers', []))
    );
  }

  getUser(id: any): Observable<User> {
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch User by ID=${id}`))
    );
  }

  addUser(user: User) {
    return this.http.post(`${environment.apiUrl}/users/register`, user);
  }

  updateUser(id: any, user: User): Observable<any> {
    return this.http.put(`${environment.apiUrl}/users/${id}`, user)
    .pipe(
      tap(_ => this.log(`Updated user id=${id}`))
    );
  }

  deleteUser(id: any): Observable<User> {
    return this.http.delete<User>(`${environment.apiUrl}/users/${id}`)
    .pipe(
      tap(_ => this.log(`Deleted user id=${id}`)),
      catchError(this.handleError<User>(`deleteUser`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
