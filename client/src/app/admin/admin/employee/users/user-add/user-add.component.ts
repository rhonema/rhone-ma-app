import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { NgbDateStruct, NgbCalendar, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { UsersService } from '../users.service';
import { PositionsService } from '../../positions/positions.service';
import { GradesService } from '../../grades/grades.service';
import { DepartmentsService } from '../../departments/departments.service';
import { IsosService } from '../../../setup/iso/isos.service';

import { Employee } from '../../../../../_models/employee';
import { Position } from '../../../../../_models/position';
import { Grade } from '../../../../../_models/grade';
import { Department } from '../../../../../_models/department';
import { Role } from '../../../../../_models/role';
import { Iso } from '../../../../../_models/iso';



@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
  //template
  public sidebarVisible = true;
  public isResizing = false;

  //datepicker
  model: NgbDateStruct;
  date: { year: number, month: number };
  @ViewChild('dp', {static: true}) dp: NgbDatepicker;

  //dropdown
  public positionList: Array<any>;
  public selectedPosition: Array<any>;
  public dropdownPositionSettings: any;

  public gradeList: Array<any>;
  public selectedGrade: Array<any>;
  public dropdownGradeSettings: any;

  public departmentList: Array<any>;
  public selectedDepartment: Array<any>;
  public dropdownDepartmentSettings: any;

  public otherDepartmentList: Array<any>;
  public selectedOtherDepartment: Array<any>;
  public dropdownOtherDepartmentSettings: any;

  public probationList: Array<any>;
  public selectedProbation: Array<any>;
  public probationSettings: any;

  public employmentList: Array<any>;
  public selectedEmployment: Array<any>;
  public employmentSettings: any;

  public hodList: Array<any>;
  public selectedHod: Array<any>;
  public hodSettings: any;

  public roleList: Array<any>;
  public selectedRole: Array<any>;
  public roleSettings: any;

  public dentalList: Array<any>;
  public selectedDental: Array<any>;
  public dentalSettings: any;

  public flightList: Array<any>;
  public selectedFlight: Array<any>;
  public flightSettings: any;

  public seminarList: Array<any>;
  public selectedSeminar: Array<any>;
  public seminarSettings: any;

  public equipmentList: Array<any>;
  public selectedEquipment: Array<any>;
  public equipmentSettings: any;

  public purchaseList: Array<any>;
  public selectedPurchase: Array<any>;
  public purchaseSettings: any;

  public gvrList: Array<any>;
  public selectedGvr: Array<any>;
  public gvrSettings: any;

  public leaveList: Array<any>;
  public selectedLeave: Array<any>;
  public leaveSettings: any;

  public jobReplaceList: Array<any>;
  public selectedJobReplace: Array<any>;
  public jobReplaceSettings: any;

  public gvrNotificationList: Array<any>;
  public selectedGvrNotification: Array<any>;
  public gvrNotificationSettings: any;

  public isoList: Array<any>;
  public selectedIso: Array<any>;
  public isoSettings: any;

  public pdpViewList: Array<any>;
  public selectedPdpView: Array<any>;
  public pdpViewSettings: any;

  public trainingViewList: Array<any>;
  public selectedTrainingView: Array<any>;
  public trainingViewSettings: any;

  public actionViewList: Array<any>;
  public selectedActionView: Array<any>;
  public actionViewSettings: any;

  public postActionViewList: Array<any>;
  public selectedPostActionView: Array<any>;
  public postActionViewSettings: any;

  //variable
  message: any;
  id = '';
  firstName = '';
  lastName = '';
  email = '';
  employee: Employee;
  positions: Position[] = [];
  grades: Grade[] = [];
  departments: Department[] = [];
  roles: Role[] = [];
  isos: Iso[] = [];
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private usersService: UsersService,
    private positionsService: PositionsService,
    private gradesService: GradesService,
    private departmentsService: DepartmentsService,
    private isosService: IsosService,
    private router: Router,
    private route: ActivatedRoute,
    private calendar: NgbCalendar
    ) {
      //position
      this.dropdownPositionSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'positionName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //grade
      this.dropdownGradeSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'gradeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //department
      this.dropdownDepartmentSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'departmentName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //other department
      this.dropdownOtherDepartmentSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'departmentName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //probation
      this.probationList = [
        {'probation': 'YES'},
        {'probation': 'NO'}
      ];

      this.probationSettings = {
        singleSelection: true,
        idField: 'probation',
        textField: 'probation',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //employment
      this.employmentList = [
        {'employment': 'YES'},
        {'employment': 'NO'}
      ];

      this.employmentSettings = {
        singleSelection: true,
        idField: 'employment',
        textField: 'employment',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //hod
      this.hodSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //role
      this.roleSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'role',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //dental
      this.dentalList = [
        {'dentalAccess': 'YES'},
        {'dentalAccess': 'NO'}
      ];

      this.dentalSettings = {
        singleSelection: true,
        idField: 'dentalAccess',
        textField: 'dentalAccess',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //flight
      this.flightList = [
        {'flightAccess': 'YES'},
        {'flightAccess': 'NO'}
      ];

      this.flightSettings = {
        singleSelection: true,
        idField: 'flightAccess',
        textField: 'flightAccess',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //seminar
      this.seminarList = [
        {'seminarAccess': 'YES'},
        {'seminarAccess': 'NO'}
      ];

      this.seminarSettings = {
        singleSelection: true,
        idField: 'seminarAccess',
        textField: 'seminarAccess',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //equipment
      this.equipmentList = [
        {'equipmentAccess': 'YES'},
        {'equipmentAccess': 'NO'}
      ];

      this.equipmentSettings = {
        singleSelection: true,
        idField: 'equipmentAccess',
        textField: 'equipmentAccess',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //purchase
      this.purchaseList = [
        {'purchaseAccess': 'YES'},
        {'purchaseAccess': 'NO'}
      ];

      this.purchaseSettings = {
        singleSelection: true,
        idField: 'purchaseAccess',
        textField: 'purchaseAccess',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //gvr
      this.gvrList = [
        {'gvrAccess': 'YES'},
        {'gvrAccess': 'NO'}
      ];

      this.gvrSettings = {
        singleSelection: true,
        idField: 'gvrAccess',
        textField: 'gvrAccess',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //Leave
      this.leaveSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //Job Replacement
      this.jobReplaceSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //gvr notification
      this.gvrNotificationSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //Iso
      this.isoSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'fileDescription',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //PDP view
      this.pdpViewSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //training view
      this.trainingViewSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //action view
      this.actionViewSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      //post action view
      this.postActionViewSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'employeeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

     }

    ngOnInit() {
      this.employee = new Employee();
      this.loadAllPositions();
      this.loadAllGrades();
      this.loadAllDepartments();
      this.loadAllHods();
      this.loadAllLeaves();
      this.loadAllJobReplace();
      this.loadAllGvr();
      this.loadAllIso();
      this.loadAllPdpView();
      this.loadAllTrainingView();
      this.loadAllActionView();
      this.loadAllPostActionView();
      const allRoles = [];
      for (var role in Role) {
        allRoles.push(role);
      }

      this.roleList = allRoles;

    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    loadAllPositions() {
      this.positionsService.getAllPositions()
      .subscribe((res: any) => {
        this.positionList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllGrades() {
      this.gradesService.getAllGrades()
      .subscribe((res: any) => {
        this.gradeList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllDepartments() {
      this.departmentsService.getAllDepartments()
      .subscribe((res: any) => {
        this.departmentList = res;
        this.otherDepartmentList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllHods() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.hodList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllLeaves() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.leaveList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllJobReplace() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.jobReplaceList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllGvr() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.gvrNotificationList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllIso() {
      this.isosService.getAllIsos()
      .subscribe((res: any) => {
        this.isoList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllPdpView() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.pdpViewList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllTrainingView() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.trainingViewList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllActionView() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.actionViewList = res;
      },
      error => {
        this.message = error;
      });
    }

    loadAllPostActionView() {
      this.usersService.getAllUsers()
      .subscribe((res: any) => {
        this.postActionViewList = res;
      },
      error => {
        this.message = error;
      });
    }

    selectToday() {
      this.model = this.calendar.getToday();
    }

    setCurrent() {
      //Current Date
      this.dp.navigateTo();
    }

    navigateEvent(event) {
      this.date = event.next;
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        this.usersService.addUser(form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Empployee Added.', true);
            this.router.navigate(['/admin/employee/users']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }
    }


}
