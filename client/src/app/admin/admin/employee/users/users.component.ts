import { Component, OnInit } from '@angular/core';
import { take , first} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';
import { UsersService } from './users.service';
import { User } from '../../../../_models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  users: User[] = [];
  message: any;
  positions: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private modalService: ModalService,
    private usersService: UsersService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllUsers(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.usersService.deleteUser(id)
            .pipe(first())
            .subscribe(x => this.loadAllUsers(1));
            this.alertService.success('User Deleted.', true);
        }
      });
  }

  private loadAllUsers(page) {
    this.usersService.getUsers(page)
    .subscribe((res: any ) => {
      console.log(res);
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    });
  }

}