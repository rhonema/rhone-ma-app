import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { CategoriesService } from '../categories.service';
import { SubcategoriesService } from '../../subcategories/subcategories.service';

import { Category } from '../../../../../_models/category';
import { Subcategory } from '../../../../../_models/subcategory';


@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dropdown
  public dropdownList: Array<any>;
	public selectedItems: Array<any>;
  public dropdownSettings: any;

  //variable
  category: Category;
  subcategories: Subcategory[] = [];
  message: any;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private categoriesService: CategoriesService,
    private subcategoriesService: SubcategoriesService,
    private router: Router
  ) {
    this.selectedItems = [];

    this.dropdownSettings = {
      singleSelection: false,
      idField: '_id',
      textField: 'subcategoryName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
   }

  ngOnInit() {
    this.category = new Category();
    this.loadAllSubcategories();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  loadAllSubcategories() {
    this.subcategoriesService.getAllSubcategories()
    .subscribe((res: any) => {
      this.dropdownList = res;
    },
    error => {
      this.message = error;
    });
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.categoriesService.addCategory(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Category Added.', true);
          this.router.navigate(['/admin/product/categories']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
