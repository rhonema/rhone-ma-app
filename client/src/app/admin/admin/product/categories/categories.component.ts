import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';

import { SidebarService } from '../../../../_services/sidebar.service';
import { CategoriesService } from './categories.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private categoriesService: CategoriesService,
    private modalService: ModalService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllCategories(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.categoriesService.deleteCategory(id)
            .pipe(first())
            .subscribe(() => this.loadAllCategories(1)
            );
            this.alertService.success('Category Deleted.', true);
        }
      });
  }


  private loadAllCategories(page) {
    this.categoriesService.getCategories(page)
    .subscribe((res: any) => {
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
