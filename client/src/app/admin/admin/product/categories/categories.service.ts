import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { Category } from '../../../../_models/category';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(
    private http: HttpClient
  ) { }

  getCategories(page: any): Observable<Category[]> {
    return this.http.get<Category[]>(`${environment.apiUrl}/categories/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetched categories')),
      catchError(this.handleError('getAll', []))
    );
  }

  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(`${environment.apiUrl}/categories/all`)
    .pipe(
      tap(_ => this.log('Fetch categories')),
      catchError(this.handleError('getAllCategories', []))
    );
  }

  getCategory(id: any): Observable<Category> {
    return this.http.get<Category>(`${environment.apiUrl}/categories/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch category id: ${id}`))
    );
  }

  addCategory(category: Category) {
    return this.http.post(`${environment.apiUrl}/categories/create`, category);
  }

  updateCategory(id: any, category: Category): Observable<any> {
    return this.http.put(`${environment.apiUrl}/categories/${id}`, category)
    .pipe(
      tap(_ => this.log(`Updated category id: ${id}`))
    );
  }

  deleteCategory(id: any): Observable<Category> {
    return this.http.delete<Category>(`${environment.apiUrl}/categories/${id}`)
    .pipe(
      tap(_ => this.log('Category deleted'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
