import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { CategoriesService } from '../categories.service';
import { SubcategoriesService } from '../../subcategories/subcategories.service';

import { Category } from '../../../../../_models/category';
import { Subcategory } from '../../../../../_models/subcategory';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dropdown
  public dropdownList: Array<any>;
	public selectedItems: Array<any>;
  public dropdownSettings: any;

  //variable
  id = '';
  message: any;
  category: Category;
  subcategories: Subcategory[] = [];
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private categoriesService: CategoriesService,
    private subcategoriesService: SubcategoriesService,
    private router: Router,
    private route: ActivatedRoute
    ) {
      this.dropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'subcategoryName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

     }

    ngOnInit() {
      this.category = new Category();
      this.getCategory(this.route.snapshot.params.id);
      this.loadAllSubcategories();
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getCategory(id: any) {
      this.categoriesService.getCategory(id).subscribe((data: any) => {
        this.category = data;
        this.selectedItems = data.subcategories;
      }, error => console.log(error));
    }

    loadAllSubcategories() {
      this.subcategoriesService.getAllSubcategories()
      .subscribe((res: any) => {
        this.dropdownList = res;
      },
      error => {
        this.message = error;
      });
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();

        this.categoriesService.updateCategory(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Category Updated.', true);
            this.router.navigate(['/admin/product/categories']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
