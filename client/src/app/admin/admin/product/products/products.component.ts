import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take , first} from 'rxjs/operators';
import { Subject } from 'rxjs';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ProductsService } from './products.service';
import { Product } from '../../../../_models/product';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //pagination
  products: Product[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  //variable
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private productsService: ProductsService,
    private modalService: ModalService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
    };

    this.loadAllProducts();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.productsService.deleteProduct(id)
            .pipe(first())
            .subscribe(() => this.loadAllProducts()
            );
            this.alertService.success('Product Deleted.', true);
        }
      });
  }

  private loadAllProducts() {
    return this.productsService.getAllProducts()
    .subscribe((res: any) => {
        this.products = res;
        this.dtTrigger.next();
    },
    error => {
      this.message = error;
    });
  }

}
