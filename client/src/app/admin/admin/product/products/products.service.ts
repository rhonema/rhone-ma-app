import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Product } from '../../../../_models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private http: HttpClient
  ) { }

  getProducts(page: any): Observable<Product[]> {
    return this.http.get<Product[]>(`${environment.apiUrl}/products/?page=${page}`)
    .pipe(
      tap(_ => this.log('Fetch Products')),
      catchError(this.handleError('getProducts', []))
    );
  }

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${environment.apiUrl}/products/all`)
    .pipe(
      tap(_ => this.log('Fetch Products')),
      catchError(this.handleError('getProducts', []))
    );
  }

  getProduct(id: any): Observable<Product> {
    return this.http.get<Product>(`${environment.apiUrl}/products/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch product by ID=${id}`)),
      catchError(this.handleError<Product>(`getProduct id=${id}`))
    );
  }

  getProductBySubcategory(catid: any, id: any): Observable<Product> {
    return this.http.get<Product>(`${environment.apiUrl}/products/category/${catid}/subcategory/${id}`)
    .pipe(
      tap(_ => this.log(`Fetch product by subcategory=${id}`)),
      catchError(this.handleError<Product>(`getProductBySubcategory id=${id}`))
    );
  }

  addProduct(product: Product) {
    return this.http.post(`${environment.apiUrl}/products/create`, product);
  }

  updateProduct(id: any, product: Product): Observable<any> {
    return this.http.put(`${environment.apiUrl}/products/${id}`, product)
    .pipe(
      tap(_ => this.log(`Updated product id=${id}`))
    );
  }

  deleteProduct(id: any): Observable<Product> {
    return this.http.delete<Product>(`${environment.apiUrl}/products/${id}`)
    .pipe(
      tap(_ => this.log(`Deleted product id=${id}`)),
      catchError(this.handleError<Product>(`deleteProduct`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
