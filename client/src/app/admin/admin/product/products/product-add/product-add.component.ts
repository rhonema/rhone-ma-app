import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { UploadService } from '../../../../../_services/upload.service';
import { ProductsService } from '../products.service';
import { CategoriesService } from '../../categories/categories.service';
import { SubcategoriesService } from '../../subcategories/subcategories.service';
import { CompaniesService } from '../../../setup/companies/companies.service';
import { GvrCategoriesService } from '../../../../../gvr/gvr/gvrCategories/gvrCategories.service';
import { GvrSectionsService } from '../../../../../gvr/gvr/gvrSections/gvrSections.service';
import { Category } from '../../../../../_models/category';
import { Product } from '../../../../../_models/product';
import { Subcategory } from '../../../../../_models/subcategory';
import { GvrSection } from '../../../../../_models/gvrSection';
import { GvrCategory } from '../../../../../_models/gvrCategory';



@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //upload
  imageObj: File;
  imageUrl: string;

  //dropdown
  public companyList: Array<any>;
	public selectedCompany: Array<any>;
  public companySettings: any;

  public categoryList: Array<any>;
	public selectedCategory: Array<any>;
  public categorySettings: any;
  
  public subcategoryList: Array<any>;
	public selectedSubcategory: Array<any>;
  public subcategorySettings: any;
  
  public gvrSectionList: Array<any>;
	public selectedGvrSection: Array<any>;
  public gvrSectionSettings: any;
  
  public gvrCategoryList: Array<any>;
	public selectedGvrCategory: Array<any>;
	public gvrCategorySettings: any;

  //variable
  message: any;
  submitted = false;
  product: Product;
  categories: Category[] = [];
  subcategories: Subcategory[] = [];
  gvrSection: GvrSection[] = [];
  gvrCategory: GvrCategory[] = [];

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private uploadService: UploadService,
    private productsService: ProductsService,
    private categoriesService: CategoriesService,
    private subcategoriesService: SubcategoriesService,
    private gvrSectionsService: GvrSectionsService,
    private gvrcategoriesService: GvrCategoriesService,
    private companiesService: CompaniesService,
    private router: Router
    ) {
      this.selectedCompany = [];
      this.companySettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'companyName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      this.selectedCategory = [];
      this.categorySettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'categoryName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      this.selectedSubcategory = [];
      this.subcategorySettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'subcategoryName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      this.selectedGvrSection = [];
      this.gvrSectionSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'gvrSectionName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };

      this.selectedGvrCategory = [];
      this.gvrCategorySettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'gvrCategoryName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true
      };
     }

    ngOnInit() {
      this.product = new Product();
      this.loadAllCompanies();
      this.loadAllCategories();
      this.loadAllSubcategories();
      this.loadAllGvrSections();
      this.loadAllGvrCategories();
    }

    onImagePicked(event: Event): void {
      const FILE = (event.target as HTMLInputElement).files[0];
      this.imageObj = FILE;
    }

    onImageUpload() {
      const imageForm = new FormData();
      imageForm.append('image', this.imageObj);
      this.uploadService.imageUpload(imageForm)
      .subscribe(res => {
        this.imageUrl = res['image'];
      });
    }

    onItemDeSelect() {
      this.selectedSubcategory = [];
      this.subcategoryList = [];
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    loadAllCompanies() {
      this.companiesService.getAllCompanies()
      .subscribe((res: any) => {
        this.companyList = res;
      },
      error => {
        this.alertService.error(error);
      });
    }

    loadAllCategories() {
      this.categoriesService.getAllCategories()
      .subscribe((res: any) => {
        this.categoryList = res;
      },
      error => {
        this.alertService.error(error);
      });
    }

    loadAllSubcategories() {
      this.subcategoriesService.getAllSubcategories()
      .subscribe((res: any) => {
        this.subcategoryList = res;
      },
      error => {
        this.alertService.error(error);
      });
    }

    loadAllGvrSections() {
      this.gvrSectionsService.getAllGvrSections()
      .subscribe((res: any) => {
        this.gvrSectionList = res;
      },
      error => {
        this.alertService.error(error);
      });
    }

    loadAllGvrCategories() {
      this.gvrcategoriesService.getAllGvrCategories()
      .subscribe((res: any) => {
        this.gvrCategoryList = res;
      },
      error => {
        this.alertService.error(error);
      });
    }

    onSubmit(isValid: boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        //reset alerts on submit
        this.alertService.clear();

        form.value.imgUrls = this.imageUrl;

        this.productsService.addProduct(form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Product Added.', true);
            this.router.navigate(['/admin/product/products']);
          },
          error => {
            this.alertService.error(error);
          }
        );
      } else {
        return;
      }

    }
}
