import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { SubcategoriesService } from '../subcategories.service';
import { Subcategory } from '../../../../../_models/subcategory';

@Component({
  selector: 'app-subcategory-edit',
  templateUrl: './subcategory-edit.component.html',
  styleUrls: ['./subcategory-edit.component.css']
})
export class SubcategoryEditComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  id = '';
  message: any;
  subcategory: Subcategory;
  submitted = false;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private subcategoriesService: SubcategoriesService,
    private router: Router,
    private route: ActivatedRoute
    ) {}

    ngOnInit() {
      this.subcategory = new Subcategory();
      this.getSubcategory(this.route.snapshot.params.id);
    }

    toggleFullWidth() {
      this.sidebarService.toggle();
      this.sidebarVisible = this.sidebarService.getStatus();
    }

    getSubcategory(id: any) {
      this.subcategoriesService.getSubcategory(id).subscribe((data: any) => {
        this.subcategory = data;
      }, error => console.log(error));
    }

    onSubmit(isValid: Boolean, form: NgForm) {
      if (isValid) {
        this.submitted = true;

        this.alertService.clear();

        this.subcategoriesService.updateSubcategory(form.value.id, form.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Subcategory Updated.', true);
            this.router.navigate(['/admin/product/subcategories']);
          },
          error => {
            this.alertService.error(error);
          }
        );

      } else {
        return;
      }
    }


}
