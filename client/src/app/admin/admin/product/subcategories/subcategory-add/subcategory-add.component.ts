import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../../../../../_services/sidebar.service';
import { AlertService } from '../../../../../_services/alert.service';
import { SubcategoriesService } from '../subcategories.service';
import { Subcategory } from '../../../../../_models/subcategory';

@Component({
  selector: 'app-subcategory-add',
  templateUrl: './subcategory-add.component.html',
  styleUrls: ['./subcategory-add.component.css']
})
export class SubcategoryAddComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //variable
  message: any;
  submitted = false;
  subcategory: Subcategory;

  constructor(
    private sidebarService: SidebarService,
    private alertService: AlertService,
    private subcategoriesService: SubcategoriesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.subcategory = new Subcategory();
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  onSubmit(isValid: Boolean, form: NgForm) {
    if (isValid) {
      this.submitted = true;

      this.alertService.clear();

      this.subcategoriesService.addSubcategory(form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Subcategory Added.', true);
          this.router.navigate(['/admin/product/subcategories']);
        },
        error => {
          this.alertService.error(error);
        }
      );

    } else {
      return;
    }
  }

}
