import { Component, OnInit } from '@angular/core';
import { take , first} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../../_services/sidebar.service';
import { ModalService } from '../../../../_services/modal.service';
import { AlertService } from '../../../../_services/alert.service';
import { SubcategoriesService } from './subcategories.service';
import { Subcategory } from '../../../../_models/subcategory';

@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.component.html',
  styleUrls: ['./subcategories.component.css']
})
export class SubcategoriesComponent implements OnInit {

  //template
  public sidebarVisible: boolean = true;
  public isResizing: boolean = false;

  //dialog modal
  public confirmedResult: boolean;

  //pagination
  pager = {};
  pages = [];
  pageOfItems = [];
  startIndex = 0;

  //variable
  subcategories: Subcategory[] = [];
  message: any;

  constructor(
    private route: ActivatedRoute,
    private sidebarService: SidebarService,
    private modalService: ModalService,
    private alertService: AlertService,
    private subcategoriesService: SubcategoriesService
  ) { }


  ngOnInit() {
    this.route.queryParams.subscribe(x => this.loadAllSubcategories(x.page || 1));
  }

  toggleFullWidth() {
    this.isResizing = true;
    this.sidebarService.toggle();
    this.sidebarVisible = this.sidebarService.getStatus();
  }

  openConfirm(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1) // take() manages unsubscription for us
    ).subscribe(result => {
        if (result) {
            this.subcategoriesService.deleteSubcategory(id)
            .pipe(first())
            .subscribe(() => this.loadAllSubcategories(1)
            );
            this.alertService.success('Subcategory Deleted.', true);
        }
      });
  }

  private loadAllSubcategories(page) {
    this.subcategoriesService.getSubcategories(page)
    .subscribe((res: any) => {
      console.log(res);
      this.pager = res.pager;
      this.pages = res.pager.pages;
      this.pageOfItems = res.pageOfItems;
      this.startIndex = res.pager.startIndex;
    },
    error => {
      this.message = error;
    });
  }

}
