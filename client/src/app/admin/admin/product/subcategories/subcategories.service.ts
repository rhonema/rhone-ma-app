import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import { Subcategory } from '../../../../_models/subcategory';


@Injectable({
  providedIn: 'root'
})
export class SubcategoriesService {

  constructor(
    private http: HttpClient
  ) { }

    getSubcategories(page: any): Observable<Subcategory[]> {
        return this.http.get<Subcategory[]>(`${environment.apiUrl}/subcategories/?page=${page}`)
        .pipe(
            tap(_ => this.log('Fetched Subcategories')),
            catchError(this.handleError('getSubcategories', []))
        );
    }

    getAllSubcategories(): Observable<Subcategory[]> {
        return this.http.get<Subcategory[]>(`${environment.apiUrl}/subcategories/all`)
        .pipe(
            tap(_ => this.log('Fetched Subcategories')),
            catchError(this.handleError('getSubcategories', []))
        );
    }

    getSubcategory(id: any): Observable<Subcategory> {
        console.log(id);
        return this.http.get<Subcategory>(`${environment.apiUrl}/subcategories/${id}`)
        .pipe(
            tap(_ => this.log(`Fetched Subcategory id: ${id}`))
        );
    }

    addSubcategory(subcategory: Subcategory) {
        return this.http.post(`${environment.apiUrl}/subcategories/create`, subcategory);
    }

    updateSubcategory(id: any, subcategory: Subcategory) {
        return this.http.put(`${environment.apiUrl}/subcategories/${id}`, subcategory)
        .pipe(
            tap(_ => this.log(`Updated subcategory id=${id}`))
        );
    }

    deleteSubcategory(id: any): Observable<Subcategory> {
        return this.http.delete<Subcategory>(`${environment.apiUrl}/subcategories/${id}`)
        .pipe(
            tap(_ => this.log(`Deleted  subcategory id=${id}`))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}