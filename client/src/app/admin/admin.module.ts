import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FullCalendarModule } from '@fullcalendar/angular';

import { SafePipe } from '../_helpers/safe.pipe';

import { routing } from './admin.routing';
import { LayoutModule } from '../_layout/layout.module';
import { PagesModule } from '../pages/pages.module';
import { AdminComponent } from './admin/admin.component';
import { IndexComponent } from './admin/index/index.component';
import { AlertComponent } from '../_components/alert.component';

//General Setup
import { CompaniesComponent } from './admin/setup/companies/companies.component';
import { CompanyAddComponent } from './admin/setup/companies/company-add/company-add.component';
import { CompanyEditComponent } from './admin/setup/companies/company-edit/company-edit.component';

import { IsosComponent } from './admin/setup/iso/isos.component';
import { IsoAddComponent } from './admin/setup/iso/iso-add/iso-add.component';
import { IsoEditComponent } from './admin/setup/iso/iso-edit/iso-edit.component';

import { EventsComponent } from './admin/setup/events/events.component';
import { EventAddComponent } from './admin/setup/events/event-add/event-add.component';
import { EventEditComponent } from './admin/setup/events/event-edit/event-edit.component';

//Employee Setup
import { UsersComponent } from './admin/employee/users/users.component';
import { UserAddComponent } from './admin/employee/users/user-add/user-add.component';
import { UserEditComponent } from './admin/employee/users/user-edit/user-edit.component';

import { GradesComponent } from './admin/employee/grades/grades.component';
import { GradeAddComponent } from './admin/employee/grades/grade-add/grade-add.component';
import { GradeEditComponent } from './admin/employee/grades/grade-edit/grade-edit.component';

import { DepartmentsComponent } from './admin/employee/departments/departments.component';
import { DepartmentAddComponent } from './admin/employee/departments/department-add/department-add.component';
import { DepartmentEditComponent } from './admin/employee/departments/department-edit/department-edit.component';

import { PositionsComponent } from './admin/employee/positions/positions.component';
import { PositionAddComponent } from './admin/employee/positions/position-add/position-add.component';
import { PositionEditComponent } from './admin/employee/positions/position-edit/position-edit.component';

import { SalespersonCodesComponent } from './admin/employee/salespersonCodes/salespersonCodes.component';
import { SalespersonCodeAddComponent } from './admin/employee/salespersonCodes/salespersonCode-add/salespersonCode-add.component';
import { SalespersonCodeEditComponent } from './admin/employee/salespersonCodes/salespersonCode-edit/salespersonCode-edit.component';

//Leave Setup
//import { EntitlementsComponent } from './admin/leave/entitlement/entitlements.component';
//import { GenerateYearComponent } from './admin/leave/entitlement/generateYear/generateYear.component';

//Product Setup
import { ProductsComponent } from './admin/product/products/products.component';
import { ProductAddComponent } from './admin/product/products/product-add/product-add.component';
import { ProductEditComponent } from './admin/product/products/product-edit/product-edit.component';

import { CategoriesComponent } from './admin/product/categories/categories.component';
import { CategoryAddComponent } from './admin/product/categories/category-add/category-add.component';
import { CategoryEditComponent } from './admin/product/categories/category-edit/category-edit.component';

import { SubcategoriesComponent } from './admin/product/subcategories/subcategories.component';
import { SubcategoryAddComponent } from './admin/product/subcategories/subcategory-add/subcategory-add.component';
import { SubcategoryEditComponent } from './admin/product/subcategories/subcategory-edit/subcategory-edit.component';

import { GenerateYearComponent } from './admin/leave/entitlement/generateYear/generateYear.component';
import { EntitlementsComponent } from './admin/leave/entitlement/entitlements.component';



@NgModule({
  imports: [
    CommonModule,
    routing,
    LayoutModule,
    RouterModule,
    PagesModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DataTablesModule,
    AngularMultiSelectModule,
    FullCalendarModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    SafePipe,
    AdminComponent,
    IndexComponent,
    AlertComponent,
    CompaniesComponent,
    CompanyAddComponent,
    CompanyEditComponent,
    IsosComponent,
    IsoAddComponent,
    IsoEditComponent,
    EventsComponent,
    EventAddComponent,
    EventEditComponent,
    UsersComponent,
    UserAddComponent,
    UserEditComponent,
    GradesComponent,
    GradeAddComponent,
    GradeEditComponent,
    DepartmentsComponent,
    DepartmentAddComponent,
    DepartmentEditComponent,
    PositionsComponent,
    PositionAddComponent,
    PositionEditComponent,
    SalespersonCodesComponent,
    SalespersonCodeAddComponent,
    SalespersonCodeEditComponent,
    EntitlementsComponent,
    GenerateYearComponent,
    ProductsComponent,
    ProductAddComponent,
    ProductEditComponent,
    CategoriesComponent,
    CategoryAddComponent,
    CategoryEditComponent,
    SubcategoriesComponent,
    SubcategoryAddComponent,
    SubcategoryEditComponent
  ]
})
export class AdminModule { }
