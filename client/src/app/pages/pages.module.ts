import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PageProfileComponent } from './page-profile/page-profile.component';

@NgModule({
  declarations: [
    PageProfileComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    RouterModule
  ],
  exports: []
})
export class PagesModule { }
