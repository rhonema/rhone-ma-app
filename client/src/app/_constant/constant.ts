export const OBJECTIVE = [
    {'purpose': 'BUSINESS FOLLOW UP'},
    {'purpose': 'COURTESY VISIT'},
    {'purpose': 'CUSTOMER SURVEY'},
    {'purpose': 'CUSTOMER COMPLAINT'},
    {'purpose': 'EQUIPMENT SERVICE'},
    {'purpose': 'ON LEAVE'},
    {'purpose': 'OPEN NEW ACCOUNT'},
    {'purpose': 'PAYMENT COLLECTION'},
    {'purpose': 'PRODUCT DELIVERY'},
    {'purpose': 'PROMOTE PRODUCT/SERVICE'},
    {'purpose': 'PSO COLLECTION'},
    {'purpose': 'SALES MEETING/MEETING/DISCUSSION'},
    {'purpose': 'SEMINAR/EXHIBITION/TRAINING'},
    {'purpose': 'SEND/COLLECT SAMPLE'},
    {'purpose': 'STATION IN OFFICE'},
    {'purpose': 'TECHNICAL SERVICE'},
    {'purpose': 'OTHERS'},
];

export const TITLE = [
    {'title': 'MR.'},
    {'title': 'MRS.'},
    {'title': 'MS.'},
    {'title': 'DR.'}
];

export const CPTITLE = [
    {'contactPersonTitle': 'MR.'},
    {'contactPersonTitle': 'MRS.'},
    {'contactPersonTitle': 'MS.'},
    {'contactPersonTitle': 'DR.'}
];

export const POPULATION = [
    {'population': 'BIRD POPULATION'},
    {'population': 'SOW POPULATION'},
    {'population': 'EGG POPULATION'},
    {'population': 'FEED POPULATION'}
];

export const TIMELINE = [
    {'timeline': 'JAN'},
    {'timeline': 'FEB'},
    {'timeline': 'MAR'},
    {'timeline': 'APR'},
    {'timeline': 'MAY'},
    {'timeline': 'JUN'},
    {'timeline': 'JUL'},
    {'timeline': 'AUG'},
    {'timeline': 'SEP'},
    {'timeline': 'OCT'},
    {'timeline': 'NOV'},
    {'timeline': 'DEC'}
];

export const STATUS = [
    {'status': 'PROSPECTING'},
    {'status': 'APPROACHING'},
    {'status': 'ENGAGING'},
    {'status': 'NEGOTIATING'},
    {'status': 'CLOSED WON'},
    {'status': 'CLOSED LOST'},
    {'status': 'FOLLOW UP'}
];

