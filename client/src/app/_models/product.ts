import { Category } from './category';
import { Subcategory } from './subcategory';
import { Company } from './company';
import { GvrSection } from './gvrSection';
import { GvrCategory } from './gvrCategory';

export class Product {
    id: number;
    productName: string;
    description: string;
    indication: string;
    composition: string;
    dosage: string;
    withdrawal: string;
    storage: string;
    precaution: string;
    packing: string;
    company: Company[];
    categories: Category[];
    subcategories: Subcategory[];
    gvrSections: GvrSection[];
    gvrCategories: GvrCategory[];
    imgUrls: [];
}
