import { Moment } from 'moment';

export class Calendar {
    id: number;
    event: string;
    eventStart: Moment;
    eventEnd: Moment;
}