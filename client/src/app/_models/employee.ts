import { Moment } from 'moment';

import { Position } from './position';
import { Role } from './role';


export class Employee {
  id: number;
  username: string;
  password: string;
  roles: Role;
  employeeName: string;
  initial: string;
  shortname: string;
  email: string;
  grades: [];
  departments: [];
  otherDepartments: [];
  hod: Employee[];
  positions: Position[];
  staffNo: number;
  employment: [];
  probation: [];
  leaveNotification: Employee[];
  jobReplacement: Employee[];
  trainingView: Employee[];
  trainingEdit: boolean;
  pdpView: Employee[];
  pdpEdit: boolean;
  actionView: Employee[];
  actionEdit: boolean;
  postActionView: Employee[];
  postActionEdit: boolean;
  defaultReceipients: Employee[];
  gvrAccess: [];
  gvrNotification: [];
  dentalAccess: [];
  flightAccess: [];
  seminarAccess: [];
  equipmentAccess: [];
  purchaseAccess: [];
  isoDocuments: [];
  agreement: number;
  agreementDate: Moment;
  year: number;
  month: number;
  day: number;
  token: string;
}
