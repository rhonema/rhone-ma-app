export class LeaveEntitlement {
    id: number;
    employee: string;
    entitlement: number;
    carryForward: number;
    dateGenerated: string;
    leaveYear: number;
  }
