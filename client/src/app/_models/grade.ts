export class Grade {
    id: number;
    gradeName: string;
    lessFive: number;
    betweenFiveTen: number;
    aboveTen: number;
    dental: number;
  }
