export class Event {
    id: number;
    title: string;
    year: number;
    month: number;
    day: number;
    holiday: string;
  }
