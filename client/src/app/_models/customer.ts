import { Company } from './company';
import { SalespersonCode } from './salespersonCode';
import { ContactPerson } from './contactPerson';

export class Customer {
    id: number;
    company: Company[];
    customerCode: string;
    customerName: string;
    contactPerson: ContactPerson[];
}
