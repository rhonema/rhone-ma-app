export class Iso {
    id: number;
    filename: string;
    fileDescription: string;
    fileType: number;
  }
