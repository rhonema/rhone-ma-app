export class ActivityDetail {
    id: number;
    section: [];
    category: [];
    services: [];
    others: boolean;
    otherServices: string;
    concern: string;
    competitor: string;
    action: string;
    whom: [];
    timeline: [];
    status: [];
}