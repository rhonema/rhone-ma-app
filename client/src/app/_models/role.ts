export enum Role {
    Employee = 'Employee',
    Hod = 'Hod',
    Finance = 'Finance',
    Gvr = 'Gvr',
    Admin = 'Admin'
}
