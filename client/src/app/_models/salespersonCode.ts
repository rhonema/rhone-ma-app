export class SalespersonCode {
    id: number;
    salespersonCode: string;
    codeDescription: string;
}