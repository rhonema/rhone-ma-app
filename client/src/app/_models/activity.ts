import { Moment } from 'moment';

import { Company } from './company';
import { Customer } from './customer';
import { User } from './user';
import { ActivityDetail } from './activityDetail';


export class Activity {
    id: number;
    weekNo: number;
    year: number;
    activityDate: string;
    company: Company[];
    purpose: [];
    otherPurpose: string;
    customer: Customer[];
    contactPerson: Customer[];
    farmName: string;
    population: string;
    size: string;
    title: string;
    farmContactPerson: string;
    farmContactNumber: string;
    farmContactEmail: string;
    activityDetail: ActivityDetail[];
    otherInfo: string;
    createdBy: User[];
    submitted: number;
    dateSubmitted: Moment;
    lateSubmitted: number;
    submittedTo: [];
    claim: number;
}