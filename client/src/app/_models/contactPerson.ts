import { CustomerPosition } from './customerPosition';

export class ContactPerson {
    id: number;
    contactPersonTitle: [];
    contactPersonName: string;
    contactPersonPosition: CustomerPosition[];
    contactPersonPhone: string;
    contactPersonEmail: string;
}
