import { Position } from './position';

export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  email: string;
  positions: Position[];
  token: string;
}
