import { GvrCategory } from './gvrCategory';


export class GvrSection {
    id: number;
    gvrSectionName: string;
    gvrCategories: GvrCategory[];
}