const express = require('express');
const router = express.Router()
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('./_helpers/jwt');
const errorHandler = require('./_helpers/error-handler');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
// upload 
app.use('/upload', require('./scripts/upload/upload.controller'));

//
//Admin
//
//General Setup
app.use('/companies', require('./scripts/admin/setup/companies/companies.controller'));
app.use('/isos', require('./scripts/admin/setup/isos/isos.controller'));
app.use('/events', require('./scripts/admin/setup/events/events.controller'));

//Employee Setup
app.use('/users', require('./scripts/admin/setup/users/users.controller'));
app.use('/grades', require('./scripts/admin/setup/grades/grades.controller'));
app.use('/departments', require('./scripts/admin/setup/departments/departments.controller'));
app.use('/positions', require('./scripts/admin/setup/positions/positions.controller'));
app.use('/salespersonCodes', require('./scripts/admin/setup/salespersonCodes/salespersonCodes.controller'));

//Leave Setup
app.use('/leave/entitlements', require('./scripts/admin/leave/entitlements/entitlements.controller'));

//Product Setup
app.use('/categories', require('./scripts/admin/setup/categories/categories.controller'));
app.use('/subcategories', require('./scripts/admin/setup/subcategories/subcategories.controller'));
app.use('/products', require('./scripts/admin/setup/products/products.controller'));

//
//Gvr
//
//General Setup
app.use('/gvr/customers', require('./scripts/gvr/customers/customers.controller'));
app.use('/gvr/customers/contactPersons', require('./scripts/gvr/customers/contactPersons/contactPersons.controller'));
app.use('/gvr/customers/customerPositions', require('./scripts/gvr/customers/customerPositions/customerPositions.controller'));

app.use('/gvr/gvrSections', require('./scripts/gvr/gvrSections/gvrSections.controller'));

app.use('/gvr/gvrCategories', require('./scripts/gvr/gvrCategories/gvrCategories.controller'));

//Activity
app.use('/gvr/activities', require('./scripts/gvr/activities/activities.controller'));
app.use('/gvr/activities/activityDetails', require('./scripts/gvr/activities/activityDetails/activityDetails.controller'));

//
//mobile
//
app.use('/mobile/companies', require('./scripts/mobile/companies.controller'));
app.use('/mobile/products', require('./scripts/mobile/products.controller'));
app.use('/mobile/categories', require('./scripts/mobile/categories.controller'));
app.use('/mobile/subcategories', require('./scripts/mobile/subcategories.controller'));



// global error handler
app.use(errorHandler);

module.exports = app;