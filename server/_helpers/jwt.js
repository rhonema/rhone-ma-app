const expressJwt = require('express-jwt');
const userService = require('../scripts/admin/setup/users/user.service');
const env = require('dotenv');

env.config();

module.exports = jwt;

function jwt() {
    const secret = process.env.JWT_KEY;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/users/authenticate',
            '/users/register',
            '/gvr/customers/test',
            /^\/gvr\/activities\/report\/.*/,
            '/mobile/products',
            '/mobile/products/all',
            /^\/mobile\/products\/.*/,
            /^\/mobile\/products\/categories\/.*/,
            /^\/mobile\/products\/categories\/*\/subcategories\/.*/,
            '/mobile/categories',
            /^\/mobile\/categories\/.*/,
            '/mobile/subcategories',
            /^\/mobile\/subcategories\/.*/,
            '/mobile/companies',
            /^\/mobile\/companies\/.*/
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};