const mongoose = require('mongoose');
const env = require('dotenv');

env.config();

mongoose
.connect(process.env.MONGODB_URI || "mongodb+srv://rhonema:" + process.env.MONGO_ATLAS_PW + "@cluster0-evtie.mongodb.net/rhonema-cms?retryWrites=true&w=majority", {
    useUnifiedTopology: true,
    useNewUrlParser: true,
});

mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;

module.exports = {
    //
    //Admin
    //
    //General Setup
    Company: require('../scripts/admin/setup/companies/company.model'),
    Iso: require('../scripts/admin/setup/isos/iso.model'),
    Event: require('../scripts/admin/setup/events/event.model'),

    //Employee Setup
    Employee: require('../scripts/admin/setup/users/employee.model'),
    Grade: require('../scripts/admin/setup/grades/grade.model'),
    Department: require('../scripts/admin/setup/departments/department.model'),
    Position: require('../scripts/admin/setup/positions/position.model'),
    SalespersonCode: require('../scripts/admin/setup/salespersonCodes/salespersonCode.model'),

    //Product Setup
    Category: require('../scripts/admin/setup/categories/category.model'),
    Subcategory: require('../scripts/admin/setup/subcategories/subcategory.model'),
    Product: require('../scripts/admin/setup/products/product.model'),
    
    //
    //Gvr
    //
    //General Setup
    Customer: require('../scripts/gvr/customers/customer.model'),
    ContactPerson: require('../scripts/gvr/customers/contactPerson.model'),
    CustomerPosition: require('../scripts/gvr/customers/customerPositions/customerPosition.model'),

    GvrSection: require('../scripts/gvr/gvrSections/gvrSection.model'),

    GvrCategory: require('../scripts/gvr/gvrCategories/gvrCategory.model'),

    //Activity
    Activity: require('../scripts/gvr/activities/activity.model')
};