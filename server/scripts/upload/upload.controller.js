const express = require('express');
const router = express.Router();

const upload = require('../../_helpers/upload');
const uploadFile = require('../../_helpers/uploadFile');

router.post('/', upload.array('image', 1), (req, res) => {
    res.send({ image: req.file });
});

router.post('/file', uploadFile.array('file', 1), (req, res) => {
    res.send({ file: req.file });
});

module.exports = router;