const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../_helpers/config');
const productService = require('./product.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/categories/:id', getByCategoryId);
router.get('/categories/:cid/subcategories/:sid', getBySubcategoryId);
router.get('/:id', getById);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    productService.count()
    .then(total => {
        totalItems = total;

        productService.getAll(page, pageSize)
        .then(products => {
            const items = products;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    productService.getAll()
        .then(products => res.json(products))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentProduct = req.product;
    
    productService.getById(req.params.id)
        .then(product => product ? res.json(product) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByCategoryId(req, res, next) {
    productService.getByCategoryId(req.params.id)
        .then(products => res.json(products))
        .catch(err => next(err));
}

function getBySubcategoryId(req, res, next) {
    productService.getBySubcategoryId(req.params.cid, req.params.sid)
        .then(products => res.json(products))
        .catch(err => next(err));
}