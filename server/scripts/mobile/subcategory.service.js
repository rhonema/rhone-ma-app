const db = require('../../_helpers/db');
const Subcategory = db.Subcategory;

module.exports = {
    getAll,
    getById
};

async function getAll() {
    return await Subcategory
    .find()
    .select('-hash')
    .sort({subcategoryName:1});
}

async function getById(id) {
    return await Subcategory.findById(id).select('-hash');
}