const db = require('../../_helpers/db');
const Product = db.Product;

module.exports = {
    count,
    getAll,
    getByCategoryId,
    getBySubcategoryId,
    getById
};

async function count() {
    return Product.countDocuments();
}

async function getAll(page, pageSize) {
    return await Product
    .find()
    .populate('categories')
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({productName:1});
}

async function getByCategoryId(id) {
    return await Product
    .find()
    .where('categories').in([id])
    .populate('company')
    .populate('categories')
    .populate('subcategories')
    .select('-hash');
}

async function getBySubcategoryId(cid, sid) {
    return await Product
    .find()
    .where('categories').in([cid])
    .where('subcategories').in([sid])
    .populate('company')
    .populate('categories')
    .populate('subcategories')
    .select('-hash');
}

async function getById(id) {
    return await Product
    .findById(id)
    .populate('company')
    .populate('categories')
    .populate('subcategories')
    .select('-hash');
}