const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../_helpers/config');
const categoryService = require('./category.service');

// routes
router.get('/', getAll);
router.get('/:id', getById);

module.exports = router;


function getAll(req, res, next) {
    categoryService.getAll()
        .then(categories => res.json(categories))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentCategory = req.category;
    
    categoryService.getById(req.params.id)
        .then(category => category ? res.json(category) : res.sendStatus(404))
        .catch(err => next(err));
}