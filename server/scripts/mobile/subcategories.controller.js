const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../_helpers/config');
const subcategoryService = require('./subcategory.service');

// routes
router.get('/', getAll);
router.get('/:id', getById);

module.exports = router;


function getAll(req, res, next) {
    subcategoryService.getAll()
        .then(subcategories => res.json(subcategories))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentSubcategory = req.subcategory;

    subcategoryService.getById(req.params.id)
        .then(subcategory => subcategory ? res.json(subcategory) : res.sendStatus(404))
        .catch(err => next(err));
}