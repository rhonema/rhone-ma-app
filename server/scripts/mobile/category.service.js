const db = require('../../_helpers/db');
const Category = db.Category;

module.exports = {
    getAll,
    getById
};

async function getAll() {
    return await Category
    .find()
    .populate('subcategories')
    .select('-hash')
    .sort({categoryName:1});
}

async function getById(id) {
    return await Category
    .findById(id)
    .populate('subcategories')
    .select('-hash');
}