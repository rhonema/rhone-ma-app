const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../_helpers/config');
const companyService = require('./company.service');

// routes
router.get('/', getAll);
router.get('/:id', getById);

module.exports = router;


function getAll(req, res, next) {
    companyService.getAll()
        .then(companies => res.json(companies))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentCompany = req.company;

    companyService.getById(req.params.id)
        .then(company => company ? res.json(company) : res.sendStatus(404))
        .catch(err => next(err));
}