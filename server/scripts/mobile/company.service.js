const db = require('../../_helpers/db');
const Company = db.Company;

module.exports = {
    getAll,
    getById
};

async function getAll() {
    return await Company
    .find()
    .select('-hash')
    .sort({companyName:1});
}

async function getById(id) {
    return await Company.findById(id).select('-hash');
}