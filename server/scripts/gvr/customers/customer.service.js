var ObjectId = require('mongoose').Types.ObjectId; 

const db = require('../../../_helpers/db');
const Customer = db.Customer;
const CustomerPosition = db.CustomerPosition;

module.exports = {
    count,
    getAll,
    company,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Customer.countDocuments();
}

async function getAll(page, pageSize) {
    return await Customer
    .find()
    .populate('company')
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({customerCode:1});
}

/*
async function test() {
    Customer.find({}, (err, customers) => {
        customers.map(customer => {
            const test = new Customer(customer);
            test.company = ObjectId("5dcb680d769b5513eab6c5e4");
            test.save();
            //console.log(customer);
            //Customer.findByIdAndRemove(customer.id);
        })
    })
    

   //Customer.deleteMany({ company: { $in: ['5dcb680d769b5513eab6c5e4']}}, function(err) {})
}
*/

async function company(id) {
    return await Customer
    .find( {company: id} )
    .select('-hash')
    .sort({customerCode:1});
}

async function getById(id) {
    return await Customer
    .findById(id)
    .populate('company')
    .populate('contactPerson')
    .populate(
        {
            path: 'contactPerson',
            populate: {
                path: 'contactPersonPosition',
                model: 'CustomerPosition'
            }
        }
    )
}

async function create(customerParam) {

    // validate
    if (await Customer.findOne({ customerCode: customerParam.customerCode })) {
        throw 'Customer Code "' + customerParam.customerCode + '" is already taken';
    }

    const customer = new Customer(customerParam);

    await customer.save();
}

async function update(id, customerParam) {

    const customer = await Customer.findById(id);

    // validate
    if (!customer) throw 'Customer not found';
    
    // copy positionParam properties to position
    Object.assign(customer, customerParam);

    await customer.save();

    return id;
    
}

async function _delete(id) {
    const customer = Customer.findById(id);

    if(!customer) throw 'Customer not found!';

    await Customer.findByIdAndRemove(id);
}