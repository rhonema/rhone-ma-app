const express = require('express');
const router = express.Router();
const paginate = require('jw-paginate');

const config = require('../../../../_helpers/config');
const contactPersonService = require('./contactPerson.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.get('/:id/customers/:cid', getByCid);
router.post('/create', create);
router.put('/update', update);
router.delete('/:id/customers/:cid', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    customerService.count()
    .then(total => {
        totalItems = total;

        contactPersonService.getAll(page, pageSize)
        .then(customers => {
            const items = customers;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

            //let date = currentWeekNumber('2020-03-29T09:30:00');
            //console.log(date);

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    contactPersonService.getAll()
        .then(customers => res.json(customers))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentContactPerson = req.customer;
    
    contactPersonService.getById(req.params.id)
        .then(contactPerson => contactPerson ? res.json(contactPerson) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByCid(req, res, next) {
    contactPersonService.getByCid(req.params.id, req.params.cid)
        .then(customer => customer ? res.json(customer) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    contactPersonService.create(req.body)
        .then(customer => customer ? res.json(customer) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
  
    contactPersonService.update(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    contactPersonService.delete(req.params.id, req.params.cid)
        .then(() => res.json({}))
        .catch(err => next(err))
}
