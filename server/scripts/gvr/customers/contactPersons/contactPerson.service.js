const db = require('../../../../_helpers/db');
const Customer = db.Customer;
const ContactPerson = db.ContactPerson;

module.exports = {
    count,
    getAll,
    getById,
    getByCid,
    create,
    update,
    delete: _delete
};

async function count() {
    return Customer.countDocuments();
}

async function getAll(page, pageSize) {
    return await Customer
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({customerCode:1});
}

async function getById(id) {
    return await Customer
    .findById(id)
    .populate('company')
    .select('-hash');
}

async function getByCid(id, cid) {

    const contactPerson = await ContactPerson.findById(id).populate('contactPersonPosition');

    if (!contactPerson) throw 'Contact Person not found';
    
    return await contactPerson;

}

async function create(customerParam) {

    const contactPerson = await ContactPerson(customerParam.customer);
    contactPerson.contactPersonName = customerParam.customer.contactPersonName.toUpperCase();
    await contactPerson.save()

    const customer = await Customer.findById(customerParam.id);

    if (!customer) throw 'Customer not found';

    customer.contactPerson.push(contactPerson._id)
    await customer.save()

    /*
    customer.contactPerson.push({
        contactPersonName: customerParam.customer.contactPersonName.toUpperCase(), 
        contactPersonPosition: customerParam.customer.contactPersonPosition,
        contactPersonPhone: customerParam.customer.contactPersonPhone, 
        contactPersonEmail: customerParam.customer.contactPersonEmail
    });
    */

    //await customer.save();

    return await customerParam.id;
   
}

async function update(customerParam) {
    const contactPerson = await ContactPerson.findById(customerParam.id);

    // validate
    if (!contactPerson) throw 'Contact Person not found';

    Object.assign(contactPerson, customerParam.customer);
    contactPerson.contactPersonName = customerParam.customer.contactPersonName.toUpperCase();
    await contactPerson.save();
    
    // copy positionParam properties to position
    /*
    var contactPerson = customer.contactPerson.id(customerParam.id);
    contactPerson.contactPersonName = customerParam.customer.contactPersonName.toUpperCase();
    contactPerson.contactPersonPosition = customerParam.customer.contactPersonPosition;
    contactPerson.contactPersonPhone = customerParam.customer.contactPersonPhone;
    contactPerson.contactPersonEmail = customerParam.customer.contactPersonEmail;
    */

}

async function _delete(id, cid) {

    const customer = await Customer.findById(cid);

    if(!customer) throw 'Customer not found!';

    await customer.contactPerson.remove(id);
    await customer.save();
}