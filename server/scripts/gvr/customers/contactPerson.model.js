const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ContactPerson = new Schema({
    contactPersonTitle: [],
    contactPersonName: { type: String, required: true },
    contactPersonPosition: [
        { type: Schema.Types.ObjectId, ref: 'CustomerPosition', required: true }
    ],
    contactPersonPhone: { type: String, required: true },
    contactPersonEmail: { type: String }
});


ContactPerson.set('toJSON', { virtuals: true });

module.exports = mongoose.model('ContactPerson', ContactPerson);