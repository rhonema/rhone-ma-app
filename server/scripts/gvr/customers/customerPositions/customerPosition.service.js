const db = require('../../../../_helpers/db');
const CustomerPosition = db.CustomerPosition;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return CustomerPosition.countDocuments();
}

async function getAll(page, pageSize) {
    return await CustomerPosition
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({customerPositionName:1});
}

async function getById(id) {
    return await CustomerPosition.findById(id).select('-hash');
}

async function create(customerPositionParam) {

    const customerPosition = new CustomerPosition(customerPositionParam);

    // Uppercase
    customerPosition.customerPositionName = customerPositionParam.customerPositionName.toUpperCase();

    // save user
    await customerPosition.save();
}

async function update(id, customerPositionParam) {

    const customerPosition = await CustomerPosition.findById(id);

    // validate
    if (!customerPosition) throw 'Position not found';

    
    // copy positionParam properties to position
    Object.assign(customerPosition, customerPositionParam);

    customerPosition.customerPositionName = customerPositionParam.customerPositionName.toUpperCase();

    await customerPosition.save();
}

async function _delete(id) {
    const customerPosition = await CustomerPosition.findById(id);

    // validate
    if (!customerPosition) throw 'Position not found';
    
    await CustomerPosition.findByIdAndRemove(id);
}
