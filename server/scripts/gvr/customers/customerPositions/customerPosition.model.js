const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CustomerPosition = new Schema({
    customerPositionName: { type: String, required: true }
});



CustomerPosition.set('toJSON', { virtuals: true });

module.exports = mongoose.model('CustomerPosition', CustomerPosition);