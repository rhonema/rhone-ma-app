const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Customer = new Schema({
    company: [
        { type: Schema.Types.ObjectId, ref: 'Company', required: true }
    ],
    customerCode: { type: String, required: true },
    customerName: { type: String, required: true },
    contactPerson: [
        { type: Schema.Types.ObjectId, ref: 'ContactPerson', required: true }
    ]
});

Customer.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Customer', Customer);