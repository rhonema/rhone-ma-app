const express = require('express');
const router = express.Router();
const paginate = require('jw-paginate');

const config = require('../../../_helpers/config');
const customerService = require('./customer.service');

// routes
router.get('/', getAll);
router.get('/test', test);
router.get('/all', all);
router.get('/company/:id', company);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    customerService.count()
    .then(total => {
        totalItems = total;

        customerService.getAll(page, pageSize)
        .then(customers => {
            const items = customers;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function test(req, res, next) {
    customerService.test()
        .then(customers => console.log(customers))
        .catch(err => next(err));
}


function all(req, res, next) {
    customerService.getAll()
        .then(customers => res.json(customers))
        .catch(err => next(err));
}

function company(req, res, next) {
    customerService.company(req.params.id)
        .then(customers => res.json(customers))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentCustomer = req.customer;
    
    customerService.getById(req.params.id)
        .then(customer => customer ? res.json(customer) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    customerService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    customerService.update(req.params.id, req.body)
    .then(id => id ? res.json(id) : res.sendStatus(404))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    customerService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
