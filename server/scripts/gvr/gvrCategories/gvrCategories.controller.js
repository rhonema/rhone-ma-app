const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../_helpers/config');
const gvrCategoryService = require('./gvrCategory.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    gvrCategoryService.count()
    .then(total => {
        totalItems = total;

        gvrCategoryService.getAll(page, pageSize)
        .then(gvrCategories => {
            const items = gvrCategories;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    gvrCategoryService.getAll()
        .then(gvrCategory => res.json(gvrCategory))
        .catch(err => next(err));
}

function getById(req, res, next) {
    gvrCategoryService.getById(req.params.id)
        .then(gvrCategory => gvrCategory ? res.json(gvrCategory) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    gvrCategoryService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    gvrCategoryService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    gvrCategoryService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
