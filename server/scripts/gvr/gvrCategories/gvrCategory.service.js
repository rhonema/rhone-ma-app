const db = require('../../../_helpers/db');
const GvrCategory = db.GvrCategory;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return GvrCategory.countDocuments();
}

async function getAll(page, pageSize) {
    return await GvrCategory
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({subcategoryName:1});
}

async function getById(id) {
    return await GvrCategory.findById(id).select('-hash');
}

async function create(gvrCategoryParam) {
    const gvrCategory = new GvrCategory(gvrCategoryParam);

    //Uppercase
    gvrCategory.gvrCategoryName = gvrCategoryParam.gvrCategoryName.toUpperCase();

    await gvrCategory.save();
}

async function update(id, gvrCategoryParam) {

    const gvrCategory = await GvrCategory.findById(id);

    // validate
    if (!gvrCategory) throw 'gvrCategory not found';

    
    // copy positionParam properties to position
    Object.assign(gvrCategory, gvrCategoryParam);

    gvrCategory.gvrCategoryName = gvrCategoryParam.gvrCategoryName.toUpperCase();

    await gvrCategory.save();
}

async function _delete(id) {
    const gvrCategory = GvrCategory.findById(id);

    if(!gvrCategory) throw 'gvrCategory not found!';

    await GvrCategory.findByIdAndRemove(id);
}