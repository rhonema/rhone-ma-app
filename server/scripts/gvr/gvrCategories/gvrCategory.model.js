const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GvrCategory = new Schema({
    gvrCategoryName: { type: String, required: true }
});

GvrCategory.set('toJSON', { virtuals: true });

module.exports = mongoose.model('GvrCategory', GvrCategory);