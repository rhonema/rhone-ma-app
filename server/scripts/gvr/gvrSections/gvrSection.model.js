const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GvrSection = new Schema({
    gvrSectionName: { type: String, required: true },
    gvrCategories: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'GvrCategory'
         }
    ]
});

GvrSection.set('toJSON', { virtuals: true });

module.exports = mongoose.model('GvrSection', GvrSection);