const db = require('../../../_helpers/db');
const GvrSection = db.GvrSection;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return GvrSection.countDocuments();
}

async function getAll(page, pageSize) {
    return await GvrSection
    .find()
    .populate('gvrCategories')
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({subSectionName:1});
}

async function getById(id) {
    return await GvrSection
    .findById(id)
    .populate('gvrCategories')
    .select('-hash');
}

async function create(gvrSectionParam) {
    const gvrSection = new GvrSection(gvrSectionParam);

    //Uppercase
    gvrSection.gvrSectionName = gvrSectionParam.gvrSectionName.toUpperCase();

    await gvrSection.save();
}

async function update(id, gvrSectionParam) {

    const gvrSection = await GvrSection.findById(id);

    // validate
    if (!gvrSection) throw 'Gvr Section not found';

    
    // copy positionParam properties to position
    Object.assign(gvrSection, gvrSectionParam);

    gvrSection.gvrSectionName = gvrSectionParam.gvrSectionName.toUpperCase();

    await gvrSection.save();
}

async function _delete(id) {
    const gvrSection = GvrSection.findById(id);

    if(!gvrSection) throw 'gvrSection not found!';

    await GvrSection.findByIdAndRemove(id);
}