const express = require('express');
const router = express.Router();
const paginate = require('jw-paginate');

const config = require('../../../_helpers/config');
const activityService = require('./activity.service');

// routes
router.get('/years', years);
router.get('/list/:id/year/:year/uid/:uid', list);
router.get('/report/:id/year/:year/uid/:uid', report);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/check', check);
router.post('/submit', submit);
router.post('/submitClaim', submitClaim);
router.post('/unlock', unlock);


module.exports = router;

function years(req, res, next) {
    activityService.years()
        .then(years => years ? res.json(years) : res.sendStatus(404))
        .catch(err => next(err));
}

function list(req, res, next) {
    activityService.list(req.params.id, req.params.year, req.params.uid)
        .then(activities => activities ? res.json(activities) : res.sendStatus(404))
        .catch(err => next(err));
}

function report(req, res, next) {
    activityService.report(req.params.id, req.params.year, req.params.uid)
        .then(activities => activities ? res.json(activities) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    activityService.getById(req.params.id)
        .then(activity => activity ? res.json(activity) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    activityService.create(req.body)
        .then(activity => activity ? res.json(activity) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    activityService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    activityService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function check(req, res, next) {
    activityService.check(req.body)
        .then(activity => activity ? res.json(activity) : res.json())
        .catch(err => next(err));
}

function submit(req, res, next) {
    activityService.submit(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function submitClaim(req, res, next) {
    activityService.submitClaim(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function unlock(req, res, next) {
    activityService.unlock(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}