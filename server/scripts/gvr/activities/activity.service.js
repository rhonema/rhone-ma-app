var ObjectId = require('mongoose').Types.ObjectId; 
var moment = require('moment');
const nodemailer = require("nodemailer");
const db = require('../../../_helpers/db');
const env = require('dotenv');
const Activity = db.Activity;
const Customer = db.Customer;
const ContactPerson = db.ContactPerson;
const Employee = db.Employee;

env.config();

module.exports = {
    years,
    list,
    report,
    getById,
    create,
    update,
    delete: _delete,
    check,
    submit,
    submitClaim,
    unlock
};

async function years() {
    return await Activity
    .distinct('year')
}

async function list(id, year, uid) {
    return await Activity
    .find( {weekNo: id, year: year, createdBy: uid} )
    .populate('company')
    .populate('customer')
    .sort({activityDate:1});
}

async function report(id, year, uid) {
    return await Activity
    .find( {weekNo: id, year: year, createdBy: uid} )
    .populate('company')
    .populate('customer')
    .populate('contactPerson')
    .populate(
        {
            path: 'contactPerson',
            populate: {
                path: 'contactPersonPosition',
                model: 'CustomerPosition'
            }
        }
    )
    .populate(
        {
            path: 'createdBy',
            model: 'Employee'
          }
    )
    .populate(
        {
            path: 'activityDetail.section',
            model: 'GvrSection'
          }
    )
    .populate(
        {
            path: 'activityDetail.category',
            model: 'GvrCategory'
          }
    )
    .populate(
        {
            path: 'activityDetail.services',
            model: 'Product'
          }
    )
    .populate(
        {
            path: 'activityDetail.whom',
            model: 'Employee'
          }
    )
    .sort({activityDate:1});
}

async function getById(id) {
    return await Activity
    .findById(id)
    .populate('company')
    .populate('customer')
    .populate('contactPerson')
    .populate(
        {
            path: 'activityDetail.section',
            model: 'GvrSection'
          }
    )
    .populate(
        {
            path: 'activityDetail.category',
            model: 'GvrCategory'
          }
    )
    .populate(
        {
            path: 'activityDetail.services',
            model: 'Product'
          }
    )
    .select('-hash');
}

async function create(activityParam) {

    //save activity info
    const activity = new Activity(activityParam);
    let newActivity = await activity.save();

    //Save contact person info
    if (activityParam.contactPersonName) {
        const contactPerson = await ContactPerson(activityParam);
        contactPerson.contactPersonName = activityParam.contactPersonName.toUpperCase();
        await contactPerson.save();
    
        activity.contactPerson.push(contactPerson._id)
        await activity.save();
   
    
        //find customer and add the regarding contact person
        const customer = await Customer.findById(newActivity.customer);

        if (!customer) throw 'Customer not found';

        customer.contactPerson.push(contactPerson._id)
        await customer.save()
    }

    return newActivity._id;
}

async function update(id, activityParam) {

    const activity = await Activity.findById(id);

    // validate
    if (!activity) throw 'Customer not found';
    
    // copy positionParam properties to position
    Object.assign(activity, activityParam);

    await activity.save();
}

async function _delete(id) {
    const activity = Activity.findById(id);

    if(!activity) throw 'Activity not found!';

    await activity.findOneAndRemove(id);
}

async function check(activityParam) {
    const activity =  await Activity.findOne({ weekNo: activityParam.week, year: activityParam.year, createdBy: activityParam.uid });

    if (activity) {
        return activity;
    }
}

async function submit(activityParam) {

    const activity = await Activity.findOne({ weekNo: activityParam.week, year: activityParam.year, createdBy: activityParam.uid });

    if (activity) {

        var allEmail = [];
        for(let i=0; i<activityParam.receipients.receipients.length; i++) {
            const employee = await Employee.findById(activityParam.receipients.receipients[i]._id).select('email');
            allEmail.push(employee.email);
        }

        //Get sender detail
        const sender = await Employee.findById(activityParam.uid);

        //using office365
        const email = 'webmaster@rhonema.com';
        const password = process.env.EMAIL_PW;
        const smtpConnectionString = 
        {
        host: 'smtp.office365.com',
        port: '587',
        auth: { user: email, pass: password },
        secureConnection: false,
        tls: { ciphers: 'SSLv3' }
        };
    

        const mailTransport = nodemailer.createTransport(smtpConnectionString);

        
        mailTransport.sendMail({
            from: email,
            to: allEmail.join(","),
            cc: sender.email,
            subject: 'GVR Week ' + activityParam.week,
            html: "Dear All,<br /><br />" + sender.employeeName + " has submitted GVR week " + activityParam.week + ".<br /><br />Please click link below to view<br /><a href='https://app.rhonema.com/gvr/activities/report/"+activityParam.week+"/year/"+activityParam.year+"/uid/"+activityParam.uid+"'>GVR Report</a><br /><br />Thank you",
        });
        

        const start = moment(activityParam.year).day('Sunday').week(activityParam.week).format('YYYY/MM/DD');
        const today = moment().format('YYYY/MM/DD');

        
        const days = Math.abs(
            moment(start, 'YYYY-MM-DD')
            .startOf('day')
            .diff(moment(today, 'YYYY-MM-DD').startOf('day'), 'days')
        ) + 1

        if (days > 14) {
            var late = 1;
        } else {
            var late = 0
        }

        const filter = { weekNo: activityParam.week, year: activityParam.year, createdBy: activityParam.uid };
        const update = { submitted: 1, dateSubmitted: Date.now(), lateSubmitted: late };

        await Activity.updateMany(filter, update);
        
        return 1;
    } else {
        return 2;
    }
    
}

async function submitClaim(activityParam) {

    const activity = await Activity.findOne({ weekNo: activityParam.week, year: activityParam.year, createdBy: activityParam.uid });
    
    if (activity) {
        
        const filter = { weekNo: activityParam.week, year: activityParam.year, createdBy: activityParam.uid };
        const update = { claim: 1 };

        await Activity.updateMany(filter, update);
        
        return 1;
    } else {
        return 2;
    }

}

async function unlock(activityParam) {

    const activity = await Activity.findOne({ weekNo: activityParam.week, year: activityParam.year, createdBy: activityParam.uid });
    
    if (activity) {
        
        const filter = { weekNo: activityParam.week, year: activityParam.year, createdBy: activityParam.uid };
        const update = { submitted: 0 };

        await Activity.updateMany(filter, update);
        
        return 1;
    } else {
        return 2;
    }

}
