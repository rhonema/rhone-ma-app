const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ActivityDetail = new Schema({
    section: [
        { type: Schema.Types.ObjectId, ref: 'Category', required: true }
    ],
    category: [
        { type: Schema.Types.ObjectId, ref: 'Subcategory' }
    ],
    services: [
        { type: Schema.Types.ObjectId, ref: 'Product' }
    ],
    others: { type: Boolean },
    otherServices: { type: String },
    concern: { type: String, required: true },
    competitor: { type: String, required: true },
    action: { type: String, required: true },
    whom: [
        { type: Schema.Types.ObjectId, ref: 'Employee', required: true }
    ],
    timeline: [],
    status: []
});


const Activity = new Schema({
    weekNo: { type: Number, required: true },
    year: { type: Number, required: true },
    activityDate: [],
    company: [
        { type: Schema.Types.ObjectId, ref: 'Company', required: true }
    ],
    purpose: [],
    otherPurpose: { type: String },
    customer: [
        { type: Schema.Types.ObjectId, ref: 'Customer', required: true }
    ],
    contactPerson: [
        { type: Schema.Types.ObjectId, ref: 'ContactPerson', required: true }
    ],
    farmName: { type: String },
    population: [],
    size: { type: String },
    title: [],
    farmContactPerson: { type: String },
    farmContactNumber: { type: String },
    farmContactEmail: { type: String },
    activityDetail: [ActivityDetail],
    otherInfo: { type: String },
    createdBy: [
        { type: Schema.Types.ObjectId, ref: 'Employee', required: true }
    ],
    submitted: { type: Number, default: 0 },
    dateSubmitted: { type: Date },
    lateSubmitted: { type: Number, default: 0 },
    submittedTo: { type: String },
    claim: { type: Number, default: 0}
});

Activity.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Activity', Activity);