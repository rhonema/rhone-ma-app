const db = require('../../../../_helpers/db');
const Activity = db.Activity;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    analysis,
    crm
};

async function count() {
    return Activity.countDocuments();
}

async function getAll(page, pageSize) {
    return await Activity
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({activityDate:1});
}

async function getById(id, aid) {
    const activity = await Activity.findById(aid)
    .populate(
        {
            path: 'activityDetail.section',
            model: 'GvrSection'
          }
    )
    .populate(
        {
            path: 'activityDetail.category',
            model: 'GvrCategory'
          }
    )
    .populate(
        {
            path: 'activityDetail.services',
            model: 'Product'
          }
    )
    .populate(
        {
            path: 'activityDetail.whom',
            model: 'Employee'
          }
    )

    if (!activity) throw 'Activity not found';
    
    return await activity.activityDetail.id(id);
}

async function create(activityParam) {
    const activity = await Activity.findById(activityParam.id);

    if (!activity) throw 'Activity not found';

    activity.activityDetail.push({
        section: activityParam.activity.section, 
        category: activityParam.activity.category,
        services: activityParam.activity.services,
        others: activityParam.activity.others,
        otherServices: activityParam.activity.otherServices,
        concern: activityParam.activity.concern, 
        competitor: activityParam.activity.competitor,
        action: activityParam.activity.action, 
        whom: activityParam.activity.whom,
        timeline: activityParam.activity.timeline, 
        status: activityParam.activity.status
    });

    await activity.save();

    return await activityParam.id;
   
}

async function update(activityParam) {

    const activity = await Activity.findById(activityParam.aid);

    // validate
    if (!activity) throw 'Customer not found';
    
    // copy activityParam properties to activityDetail
    var detail = activity.activityDetail.id(activityParam.id);
    detail.section = activityParam.activity.section,
    detail.category = activityParam.activity.category,
    detail.services = activityParam.activity.services,
    detail.others = activityParam.activity.others,
    detail.otherServices = activityParam.activity.otherServices,
    detail.concern = activityParam.activity.concern, 
    detail.competitor = activityParam.activity.competitor,
    detail.action = activityParam.activity.action, 
    detail.whom = activityParam.activity.whom,
    detail.timeline = activityParam.activity.timeline, 
    detail.status = activityParam.activity.status

    await activity.save();
}

async function _delete(id, aid) {

    const activity = await Activity.findById(aid);

    if(!activity) throw 'Activity not found!';

    await activity.activityDetail.id(id).remove();
    await activity.save();
}

async function analysis() {
    return await Activity
    .find({activityDetail: { $exists: true, $not: {$size: 0} }})
    .populate(
        {
            path: 'activityDetail.section',
            model: 'GvrSection'
          }
    )
    .populate(
        {
            path: 'activityDetail.category',
            model: 'GvrCategory'
          }
    )
    .populate(
        {
            path: 'activityDetail.services',
            model: 'Product'
          }
    )
    .populate(
        {
            path: 'activityDetail.whom',
            model: 'Employee'
          }
    )
    .select('-hash')
    .sort({activityDate:1});
}

async function crm() {
    return await Activity
    .find({farmName: { $ne: '', $ne: null }})
    .populate('company')
    .populate('customer')
    .populate('contactPerson')
    .populate(
        {
            path: 'contactPerson',
            populate: {
                path: 'contactPersonPosition',
                model: 'CustomerPosition'
            }
        }
    )
    .select('-hash')
    .sort({activityDate:1});
}