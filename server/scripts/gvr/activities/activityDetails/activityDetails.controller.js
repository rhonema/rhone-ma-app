const express = require('express');
const router = express.Router();
const paginate = require('jw-paginate');

const config = require('../../../../_helpers/config');
const activityDetailService = require('./activityDetail.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id/activities/:aid', getById);
router.post('/create', create);
router.put('/update', update);
router.delete('/:id/activities/:aid', _delete);
router.get('/analysis', analysis);
router.get('/crm', crm);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    customerService.count()
    .then(total => {
        totalItems = total;

        activityDetailService.getAll(page, pageSize)
        .then(customers => {
            const items = customers;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

            //let date = currentWeekNumber('2020-03-29T09:30:00');
            //console.log(date);

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    activityDetailService.getAll()
        .then(customers => res.json(customers))
        .catch(err => next(err));
}

function getById(req, res, next) {
    activityDetailService.getById(req.params.id, req.params.aid)
        .then(activity => activity ? res.json(activity) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    activityDetailService.create(req.body)
        .then(activity => activity ? res.json(activity) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    activityDetailService.update(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    activityDetailService.delete(req.params.id, req.params.aid)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function analysis(req, res, next) {
    activityDetailService.analysis()
        .then(activities => res.json(activities))
        .catch(err => next(err));
}

function crm(req, res, next) {
    activityDetailService.crm()
        .then(crm => res.json(crm))
        .catch(err => next(err));
}

