const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const entitlementService = require('./entitlement.service');

// routes
router.get('/generateYear/:id', generateYear);


module.exports = router;

function generateYear(req, res, next) {
    entitlementService.generateYear(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}
