const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
    salespersonCode: { type: String, required: true },
    codeDescription: { type: String, required: true }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('SalespersonCode', schema);