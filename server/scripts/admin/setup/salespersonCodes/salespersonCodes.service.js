const db = require('../../../../_helpers/db');
const SalespersonCode = db.SalespersonCode;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return SalespersonCode.countDocuments();
}

async function getAll(page, pageSize) {
    return await SalespersonCode
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({SalespersonCode:1});
}

async function getById(id) {
    return await SalespersonCode.findById(id).select('-hash');
}

async function create(salespersonCodeParam) {
    const salespersonCode = new SalespersonCode(salespersonCodeParam);

    //Uppercase
    salespersonCode.salespersonCode = salespersonCodeParam.salespersonCode.toUpperCase();
    salespersonCode.codeDescription = salespersonCodeParam.codeDescription.toUpperCase();


    await salespersonCode.save();
}

async function update(id, salespersonCodeParam) {

    const salespersonCode = await SalespersonCode.findById(id);

    // validate
    if (!salespersonCode) throw 'Salesperson Code not found';

    
    // copy positionParam properties to position
    Object.assign(salespersonCode, salespersonCodeParam);

    salespersonCode.salespersonCode = salespersonCodeParam.salespersonCode.toUpperCase();
    salespersonCode.codeDescription = salespersonCodeParam.codeDescription.toUpperCase();

    await salespersonCode.save();
}

async function _delete(id) {
    const salespersonCode = SalespersonCode.findById(id);

    if(!salespersonCode) throw 'Salesperson Code not found!';

    await SalespersonCode.findByIdAndRemove(id);
}