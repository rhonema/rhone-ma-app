const db = require('../../../../_helpers/db');
const Department = db.Department;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Department.countDocuments();
}

async function getAll(page, pageSize) {
    return await Department
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({departmentName:1});
}

async function getById(id) {
    return await Department.findById(id).select('-hash');
}

async function create(departmentParam) {

    const department = new Department(departmentParam);

    // save user
    await department.save();
}

async function update(id, departmentParam) {

    const department = await Department.findById(id);

    // validate
    if (!department) throw 'Department not found';

    // copy positionParam properties to department
    Object.assign(department, departmentParam);

    await department.save();
}

async function _delete(id) {
    const department = await Department.findById(id);

    // validate
    if (!department) throw 'Department not found';
    
    await Department.findByIdAndRemove(id);
}
