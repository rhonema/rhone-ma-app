const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Department = new Schema({
    departmentName: { type: String, unique: true, required: true }
});

Department.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Department', Department);
