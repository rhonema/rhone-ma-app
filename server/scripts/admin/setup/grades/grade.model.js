const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Grade = new Schema({
    gradeName: { type: String, unique: true, required: true },
    lessFive: { type: Number, required: true },
    betweenFiveTen: { type: Number, required: true },
    aboveTen: { type: Number, required: true },
    dental: { type: Number, required: true }
});

Grade.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Grade', Grade);
