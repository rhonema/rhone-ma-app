const db = require('../../../../_helpers/db');
const Grade = db.Grade;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Grade.countDocuments();
}

async function getAll(page, pageSize) {
    return await Grade
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({gradeName:1});
}

async function getById(id) {
    return await Grade.findById(id).select('-hash');
}

async function create(gradeParam) {

    const grade = new Grade(gradeParam);

    // Uppercase
    grade.gradeName = gradeParam.gradeName.toUpperCase();

    // save user
    await grade.save();
}

async function update(id, gradeParam) {

    const grade = await Grade.findById(id);

    // validate
    if (!grade) throw 'Grade not found';

    
    // copy gradeParam properties to grade
    Object.assign(grade, gradeParam);

    grade.gradeName = gradeParam.gradeName.toUpperCase();

    await grade.save();
}

async function _delete(id) {
    const grade = await Grade.findById(id);

    // validate
    if (!grade) throw 'grade not found';
    
    await grade.findByIdAndRemove(id);
}
