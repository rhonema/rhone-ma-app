const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const gradeService = require('../grades/grade.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    gradeService.count()
    .then(total => {
        totalItems = total;

        gradeService.getAll(page, pageSize)
        .then(grades => {
            const items = grades;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });
        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    gradeService.getAll()
        .then(grades => res.json(grades))
        .catch(err => next(err));
}

function getById(req, res, next) {
    gradeService.getById(req.params.id)
        .then(grade => grade ? res.json(grade) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    gradeService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    gradeService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    gradeService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
