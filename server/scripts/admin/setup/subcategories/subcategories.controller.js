const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const subcategoryService = require('../subcategories/subcategory.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    subcategoryService.count()
    .then(total => {
        totalItems = total;

        subcategoryService.getAll(page, pageSize)
        .then(subcategories => {
            const items = subcategories;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    subcategoryService.getAll()
        .then(subcategories => res.json(subcategories))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentSubcategory = req.subcategory;

    subcategoryService.getById(req.params.id)
        .then(subcategory => subcategory ? res.json(subcategory) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    subcategoryService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    subcategoryService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    subcategoryService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
