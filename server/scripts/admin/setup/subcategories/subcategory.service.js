const db = require('../../../../_helpers/db');
const Subcategory = db.Subcategory;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Subcategory.countDocuments();
}

async function getAll(page, pageSize) {
    return await Subcategory
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({subcategoryName:1});
}

async function getById(id) {
    return await Subcategory.findById(id).select('-hash');
}

async function create(subcategoryParam) {
    const subcategory = new Subcategory(subcategoryParam);

    //Uppercase
    subcategory.subcategoryName = subcategoryParam.subcategoryName.toUpperCase();

    await subcategory.save();
}

async function update(id, subcategoryParam) {

    const subcategory = await Subcategory.findById(id);

    // validate
    if (!subcategory) throw 'Subcategory not found';

    
    // copy positionParam properties to position
    Object.assign(subcategory, subcategoryParam);

    subcategory.subcategoryName = subcategoryParam.subcategoryName.toUpperCase();

    await subcategory.save();
}

async function _delete(id) {
    const subcategory = Subcategory.findById(id);

    if(!subcategory) throw 'Subcategory not found!';

    await Subcategory.findByIdAndRemove(id);
}