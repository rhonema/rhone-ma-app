const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
    subcategoryName: { type: String, required: true }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Subcategory', schema);