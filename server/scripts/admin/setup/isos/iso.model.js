const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Iso = new Schema({
    filename: { type: String },
    fileDescription: { type: String, required: true },
    fileType: []
});

Iso.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Iso', Iso);