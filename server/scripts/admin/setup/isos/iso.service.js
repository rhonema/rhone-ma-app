const db = require('../../../../_helpers/db');
const Iso = db.Iso;
const aws = require('aws-sdk');
const env = require('dotenv');

env.config();

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    deleteFile: _deleteFile
};

async function count() {
    return Iso.countDocuments();
}

async function getAll(page, pageSize) {
    return await Iso
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({filename:1});
}

async function getById(id) {
    return await Iso
    .findById(id)
    .select('-hash');
}


async function create(isoParam) {
    const iso = new Iso(isoParam);

    await iso.save();
}

async function update(id, isoParam) {

    const iso = await Iso.findById(id);

    // validate
    if (!iso) throw 'Iso not found';

    Object.assign(iso, isoParam);

    await iso.save();
}

async function _delete(id) {
    const iso = Iso.findById(id);

    if(!iso) throw 'Iso not found!';

    //delete file from AWS
    aws.config.update({
        secretAccessKey: process.env.SECRET_ACCESS_KEY,
        accessKeyId: process.env.ACCESS_KEY_ID,
        region: process.env.REGION
    });

    const s3 = new aws.S3();

    const params = {
            Bucket: process.env.FILEBUCKET,
            Key: iso.filename
    }

    try {
        await s3.headObject(params).promise()
        console.log("File Found in S3")
        try {
            await s3.deleteObject(params).promise()
            console.log("file deleted Successfully")
        }
        catch (err) {
            console.log("ERROR in file Deleting : " + JSON.stringify(err))
        }
    } catch (err) {
            console.log("File not Found ERROR : " + err.code)
    }

    await Iso.findByIdAndRemove(id);
}

async function _deleteFile(id) {
    
    const iso = await Iso.findById(id);
    // validate
    if (!iso) throw 'Iso not found';

    //delete file from AWS
    aws.config.update({
        secretAccessKey: process.env.SECRET_ACCESS_KEY,
        accessKeyId: process.env.ACCESS_KEY_ID,
        region: process.env.REGION
    });

    const s3 = new aws.S3();

    const params = {
            Bucket: process.env.FILEBUCKET,
            Key: iso.filename
    }

    try {
        await s3.headObject(params).promise()
        console.log("File Found in S3")
        try {
            await s3.deleteObject(params).promise()
            console.log("file deleted Successfully")
        }
        catch (err) {
            console.log("ERROR in file Deleting : " + JSON.stringify(err))
        }
    } catch (err) {
            console.log("File not Found ERROR : " + err.code)
    }
    
    Iso.find({filename: iso.filename}, (err, res) => {
        res.map(isos => {
            const detail = new Iso(isos);
            console.log(isos);
            console.log(detail);
            detail.filename = '';
            detail.save();
        })
    })
}