const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const isoService = require('../isos/iso.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);
router.delete('/file/:id', _deleteFile);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    isoService.count()
    .then(total => {
        totalItems = total;

        isoService.getAll(page, pageSize)
        .then(isos => {
            const items = isos;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    isoService.getAll()
        .then(isos => res.json(isos))
        .catch(err => next(err));
}

function getById(req, res, next) {
    isoService.getById(req.params.id)
        .then(iso => iso ? res.json(iso) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    isoService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {

    isoService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    isoService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function _deleteFile(req, res, next) {
    isoService.deleteFile(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
