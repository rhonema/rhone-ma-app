const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const positionService = require('../positions/position.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    positionService.count()
    .then(total => {
        totalItems = total;

        positionService.getAll(page, pageSize)
        .then(positions => {
            const items = positions;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });
        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    positionService.getAll()
        .then(positions => res.json(positions))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentPosition = req.positionName;
    
    positionService.getById(req.params.id)
        .then(position => position ? res.json(position) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    positionService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    positionService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    positionService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
