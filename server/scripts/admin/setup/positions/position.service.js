const db = require('../../../../_helpers/db');
const Position = db.Position;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Position.countDocuments();
}

async function getAll(page, pageSize) {
    return await Position
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({positionName:1});
}

async function getById(id) {
    return await Position.findById(id).select('-hash');
}

async function create(positionParam) {

    const position = new Position(positionParam);

    // Uppercase
    position.positionName = positionParam.positionName.toUpperCase();

    // save user
    await position.save();
}

async function update(id, positionParam) {

    const position = await Position.findById(id);

    // validate
    if (!position) throw 'Position not found';

    
    // copy positionParam properties to position
    Object.assign(position, positionParam);

    position.positionName = positionParam.positionName.toUpperCase();

    await position.save();
}

async function _delete(id) {
    const position = await Position.findById(id);

    // validate
    if (!position) throw 'Position not found';
    
    await Position.findByIdAndRemove(id);
}
