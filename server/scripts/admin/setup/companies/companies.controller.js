const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const companyService = require('../companies/company.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    companyService.count()
    .then(total => {
        totalItems = total;

        companyService.getAll(page, pageSize)
        .then(companies => {
            const items = companies;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    companyService.getAll()
        .then(companies => res.json(companies))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentCompany = req.company;

    companyService.getById(req.params.id)
        .then(company => company ? res.json(company) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    companyService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    companyService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    companyService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
