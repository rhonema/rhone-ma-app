const db = require('../../../../_helpers/db');
const Company = db.Company;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Company.countDocuments();
}

async function getAll(page, pageSize) {
    return await Company
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({companyName:1});
}

async function getById(id) {
    return await Company.findById(id).select('-hash');
}

async function create(companyParam) {
    const company = new Company(companyParam);

    //Uppercase
    company.companyName = companyParam.companyName.toUpperCase();

    await company.save();
}

async function update(id, companyParam) {

    const company = await Company.findById(id);

    // validate
    if (!company) throw 'Company not found';

    
    // copy positionParam properties to position
    Object.assign(company, companyParam);

    company.companyName = companyParam.companyName.toUpperCase();

    await company.save();
}

async function _delete(id) {
    const company = Company.findById(id);

    if(!company) throw 'Company not found!';

    await Company.findByIdAndRemove(id);
}