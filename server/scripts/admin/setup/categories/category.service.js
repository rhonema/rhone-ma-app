const db = require('../../../../_helpers/db');
const Category = db.Category;

module.exports = {
    count,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Category.countDocuments();
}

async function getAll(page, pageSize) {
    return await Category
    .find()
    .populate('subcategories')
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({categoryName:1});
}

async function getById(id) {
    return await Category
    .findById(id)
    .populate('subcategories')
    .select('-hash');
}

async function create(categoryParam) {
    const category = new Category(categoryParam);

    //Uppercase
    category.categoryName = categoryParam.categoryName.toUpperCase();

    await category.save();
}

async function update(id, categoryParam) {

    const category = await Category.findById(id);

    // validate
    if (!category) throw 'Category not found';

    
    // copy categoryParam properties to category
    Object.assign(category, categoryParam);

    category.categoryName = categoryParam.categoryName.toUpperCase();

    await category.save();
}

async function _delete(id) {
    const category = Category.findById(id);

    if(!category) throw 'Category not found!';

    await Category.findByIdAndRemove(id);
}