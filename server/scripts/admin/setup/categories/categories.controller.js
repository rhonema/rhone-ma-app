const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const categoryService = require('../categories/category.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    categoryService.count()
    .then(total => {
        totalItems = total;

        categoryService.getAll(page, pageSize)
        .then(categories => {
            const items = categories;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    categoryService.getAll()
        .then(categories => res.json(categories))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentCategory = req.category;
    
    categoryService.getById(req.params.id)
        .then(category => category ? res.json(category) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    categoryService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    categoryService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    categoryService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
