const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    categoryName: { type: String, required: true },
    subcategories: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Subcategory'
         }
    ]
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Category', schema);