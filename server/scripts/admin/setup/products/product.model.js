const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
    productName: { type: String, required: true },
    description: { type: String, required: true },
    indication: { type: String, required: true },
    composition: { type: String, required: true },
    dosage: { type: String, required: true },
    withdrawal: { type: String, required: true },
    storage: { type: String, required: true },
    precaution: { type: String, required: true },
    packing: { type: String, required: true },
    company: [
        { type: Schema.Types.ObjectId, ref: 'Company', required: true }
    ],
    categories: [
        { type: Schema.Types.ObjectId, ref: 'Category' }
    ],
    subcategories: [
        { type: Schema.Types.ObjectId, ref: 'Subcategory' }
    ],
    gvrSections: [
        { type: Schema.Types.ObjectId, ref: 'GvrSection' }
    ],
    gvrCategories: [
        { type: Schema.Types.ObjectId, ref: 'GvrCategory' }
    ],
    imgUrls: [
        { type: String }
    ],
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Product', schema);