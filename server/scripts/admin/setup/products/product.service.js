const db = require('../../../../_helpers/db');
const Product = db.Product;

module.exports = {
    count,
    getAll,
    getById,
    getBySubcategory,
    create,
    update,
    delete: _delete
};

async function count() {
    return Product.countDocuments();
}

async function getAll(page, pageSize) {
    return await Product
    .find()
    .populate('categories')
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({productName:1});
}

async function getById(id) {
    return await Product
    .findById(id)
    .populate('company')
    .populate('categories')
    .populate('subcategories')
    .populate('gvrSections')
    .populate('gvrCategories')
    .select('-hash');
}

async function getBySubcategory(cid, id) {
    return await Product
    .find()
    .where('gvrSections').in(cid)
    .where('gvrCategories').in(id)
    .populate('company')
    .populate('gvrSections')
    .populate('gvrCategories')
    .select('-hash');
}

async function create(productParam) {
    const product = new Product(productParam);

    //Uppercase
    product.productName = productParam.productName.toUpperCase();

    await product.save();
}

async function update(id, productParam) {

    const product = await Product.findById(id);

    // validate
    if (!product) throw 'Product not found';

    Object.assign(product, productParam);

    product.productName = productParam.productName.toUpperCase();

    await product.save();
}

async function _delete(id) {
    const product = Product.findById(id);

    if(!product) throw 'Product not found!';

    await Product.findByIdAndRemove(id);
}