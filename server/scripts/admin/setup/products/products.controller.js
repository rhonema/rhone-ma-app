const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const productService = require('../products/product.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/:id', getById);
router.get('/category/:cid/subcategory/:id', getBySubcategory);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    productService.count()
    .then(total => {
        totalItems = total;

        productService.getAll(page, pageSize)
        .then(products => {
            const items = products;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    productService.getAll()
        .then(products => res.json(products))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentProduct = req.product;
    
    productService.getById(req.params.id)
        .then(product => product ? res.json(product) : res.sendStatus(404))
        .catch(err => next(err));
}

function getBySubcategory(req, res, next) {
    productService.getBySubcategory(req.params.cid, req.params.id)
        .then(product => product ? res.json(product) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    productService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {

    productService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    productService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
