const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();

const config = require('../../../../_helpers/config');
const eventService = require('../events/event.service');

// routes
router.get('/', getAll);
router.get('/all', all);
router.get('/all/:id', getByYear);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    eventService.count()
    .then(total => {
        totalItems = total;

        eventService.getAll(page, pageSize)
        .then(events => {
            const items = events;

            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });
        })
        .catch(err => next(err));
    }); 
}

function all(req, res, next) {
    eventService.getAll()
        .then(events => res.json(events))
        .catch(err => next(err));
}

function getByYear(req, res, next) {
    eventService.getByYear(req.params.id)
        .then(events => res.json(events))
        .catch(err => next(err));
}

function getById(req, res, next) {
    eventService.getById(req.params.id)
        .then(event => event ? res.json(event) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    eventService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    eventService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    eventService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}
