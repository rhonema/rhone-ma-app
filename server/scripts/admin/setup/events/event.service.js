const db = require('../../../../_helpers/db');
const Event = db.Event;

module.exports = {
    count,
    getAll,
    getByYear,
    getById,
    create,
    update,
    delete: _delete
};

async function count() {
    return Event.countDocuments();
}

async function getAll(page, pageSize) {
    return await Event
    .find()
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({start:1});
}

async function getByYear(id) {
    return await Event
    .find({year: id})
    .select('-hash');
}


async function getById(id) {
    return await Event.findById(id).select('-hash');
}

async function create(eventParam) {

    const event = new Event(eventParam);

    event.year = eventParam.start.year;
    event.month = eventParam.start.month;
    event.day = eventParam.start.day;

    // save user
    await event.save();
}

async function update(id, eventParam) {

    const event = await Event.findById(id);

    // validate
    if (!event) throw 'Event not found';

    // copy positionParam properties to department
    Object.assign(event, eventParam);

    event.year = eventParam.start.year;
    event.month = eventParam.start.month;
    event.day = eventParam.start.day;
    
    await event.save();
}

async function _delete(id) {
    const event = await Event.findById(id);

    // validate
    if (!event) throw 'Event not found';
    
    await Event.findByIdAndRemove(id);
}
