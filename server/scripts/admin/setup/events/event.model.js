const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Event = new Schema({
    title: { type: String, required: true },
    year: { type: Number, required: true },
    month: { type: Number, required: true },
    day: { type: Number, required: true },
    holiday: []
});

Event.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Event', Event);
