const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true},
    positions: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Position'
         }
    ],
    dateJoined: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);
