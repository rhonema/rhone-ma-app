﻿const express = require('express');
const paginate = require('jw-paginate');
const router = express.Router();
const authorize = require('../../../../_helpers/authorize');

const config = require('../../../../_helpers/config');
const userService = require('../users/user.service');
const Role = require('../../../../_helpers/role');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/all', all);
router.get('/gvr', gvr);
router.post('/create', create);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', authorize(Role.Admin), _delete);

module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Authentication Failed!' }))
        .catch(err => next(err));
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

/*
function test(req, res, next) {
    userService.test()
        .then(user => user ? console.log(user) : res.sendStatus(404))
        .catch(err => next(err));
}
*/

function getAll(req, res, next) {
    //get page from query params or default to first page
    const page = parseInt(req.query.page) || 1;
    const pageSize = config.pagination.pageSize;
    let totalItems;
    userService.count()
    .then(total => {
        totalItems = total;

        userService.getAll(page, pageSize)
        .then(users => {
            const items = users;
            const pager = paginate(totalItems, page, pageSize);

            pageOfItems = items;

            res.json({ pager, pageOfItems });

        })
        .catch(err => next(err));
    });
}

function all(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function gvr(req, res, next) {
    userService.getGvr()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(users => res.json(users))
        .catch(err => next(err));
}


