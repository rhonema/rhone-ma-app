﻿const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../../../../_helpers/db');
const env = require('dotenv');

//const User = db.User;
const Employee = db.Employee;

env.config();

module.exports = {
    authenticate,
    count,
    getAll,
    getGvr,
    getById,
    create,
    update,
    delete: _delete
};

async function authenticate({ username, password }) {
    console.log(bcrypt.hashSync('Rhonema123', 10));
    const user = await Employee.findOne({ username });
    if (user && bcrypt.compareSync(password, user.password)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id, role: user.roles }, process.env.JWT_KEY);
        //const token = jwt.sign({ sub: user.id, role: user.role }, "RhoneMa20!!", { expiresIn: "1h" });
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function count() {
    return Employee.countDocuments();
}

async function getAll(page, pageSize) {
    return await Employee.find()
    .populate('positions')
    .skip((page - 1) * pageSize)
    .limit(pageSize)
    .select('-hash')
    .sort({employeeName:1});
}

async function getGvr() {
    return await Employee.find()
    .sort({firstName:1});
}

async function getById(id) {
    return await Employee.findById(id)
    .populate('positions')
    .populate('departments')
    .populate('grades')
    .populate(
        {
            path: 'hod',
            model: 'Employee'
        }
    )
    .populate(
        {
            path: 'otherDepartments',
            model: 'Department'
        }
    )
    .populate(
        {
            path: 'leaveNotification',
            model: 'Employee'
        }
    )
    .populate(
        {
            path: 'jobReplacement',
            model: 'Employee'
        }
    )
    .populate(
        {
            path: 'isoDocuments',
            model: 'Iso'
        }
    )
    .select('-hash');
}

async function create(userParam) {

    // validate
    if (await Employee.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new Employee(userParam);

    // hash password
    if (userParam.password) {
        user.password = bcrypt.hashSync(userParam.password, 10);
    }

    // Uppercase
    user.employeeName = userParam.employeeName.toUpperCase();

    user.year = userParam.dateJoined.year;
    user.month = userParam.dateJoined.month;
    user.day = userParam.dateJoined.day;

    // save user
    await user.save();
}

/*
async function test() {
    Employee.find({}, (err, users) => {
        users.map(user => {
            const employee = new Employee(user);
            employee.password = bcrypt.hashSync(user.password, 10);
            employee.save();
        })
    })
    
}
*/

async function update(id, userParam) {
    const user = await Employee.findById(id);

    // validate
    if (!user) throw 'User not found';
    
    // copy userParam properties to user
    Object.assign(user, userParam);

    // Uppercase
    user.employeeName = userParam.employeeName.toUpperCase();

    user.year = userParam.dateJoined.year;
    user.month = userParam.dateJoined.month;
    user.day = userParam.dateJoined.day;

    await user.save(function(error) {
        console.log(error);
    });
}

async function _delete(id) {
    const user = await Employee.findById(id);

    // validate
    if (!user) throw 'User not found';
    
    await User.findByIdAndRemove(id);
}
