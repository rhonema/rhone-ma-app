const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    roles: [],
    employeeName: { type: String, required: true },
    initial: { type: String, required: true },
    shortname: { type: String },
    email: { type: String, required: true },
    grades: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Grade'
         }
    ],
    departments: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Department'
         }
    ],
    otherDepartments: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Department'
         }
    ],
    hod: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
         }
    ],
    positions: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Position'
         }
    ],
    staffNo: { type: Number },
    employment: [],
    probation: [],
    leaveNotification: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
         }
    ],
    jobReplacement: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
         }
    ],
    trainingView: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
         }
    ],
    trainingEdit: { type: Boolean },
    pdpView: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
         }
    ],
    pdpEdit: { type: Boolean },
    actionView: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
         }
    ],
    actionEdit: { type: Boolean },
    postActionView: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
         }
    ],
    postActionEdit: { type: Boolean },
    defaultReceipients: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
         }
    ],
    gvrAccess: [],
    gvrNotification: [],
    dentalAccess: [],
    flightAccess: [],
    seminarAccess: [],
    equipmentAccess: [],
    purchaseAccess: [],
    isoDocuments: [
        { 
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Iso'
         }
    ],
    agreement: { type: Number },
    agreementDate: { type: Date, default: Date.now }
    
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Employee', schema);
